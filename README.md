# FoodCravings


## Members

1.  Aaron Dewitt 
    * EID: aad777
    * GitlabId: arkd 
2.  Kevin Gao 
    * EID: kg29745
    * GitlabId: kevingao232
3.  Rishabh Thakkar 
    * EID: rt25543
    * GitlabId: ribsthakkar
4.  Alexis Torres 
    * EID: aet2348
    * GitlabId: Alexistower
5.  Robert Guan
    * EID: ryg99
    * GitlabId: rguan

## Git SHA
731ba7bf0442f5fa1e7b19c4b317167c8fdef47d

## Pipeline
https://gitlab.com/Alexistower/foodcravings/pipelines

## Website Link:
https://foodcravings.net/


## Postman tests path: /app/Postman.json
## Python tests: /app/test
## Frontend tests: /frontend/src/pages/{pagename}/{pagename}.test.tsx

## Phase 1
## Estimated Completion times
1.  Aaron: Estimated: 4 , Actual: 6 
2.  Kevin:  Estimated: 2, Actual: 3
3.  Rishabh: Estimated: 5, Actual: 7 
4.  Robert: Estimated: 6, Actual: 8 
5.  Alexis: Estimated: 5, Actual: 9


## Phase 2
## Estimated Completion times
1.  Aaron: Estimated: 12 , Actual: 18 
2.  Kevin:  Estimated: 13, Actual: 14
3.  Rishabh: Estimated: 16, Actual: 19 
4.  Robert: Estimated: 15, Actual: 15 
5.  Alexis: Estimated: 12, Actual: 16


## Phase 3
## Estimated Completion times
1.  Aaron: Estimated: 12 , Actual: 11 
2.  Kevin:  Estimated: 13, Actual: 15
3.  Rishabh: Estimated: 10, Actual: 9 
4.  Robert: Estimated: 15, Actual: 13 
5.  Alexis: Estimated: 14, Actual: 17

## Phase 4
## Estimated Completion times
1.  Aaron: Estimated: 5, Actual: 2 
2.  Kevin:  Estimated: 5, Actual: 4
3.  Rishabh: Estimated: 4, Actual: 4 
4.  Robert: Estimated: 4, Actual: 3 
5.  Alexis: Estimated: 5, Actual: 3

