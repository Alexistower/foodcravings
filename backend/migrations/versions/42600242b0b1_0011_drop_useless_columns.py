"""0011 drop useless columns

Revision ID: 42600242b0b1
Revises: 1e19cf59477d
Create Date: 2019-11-04 15:07:12.797861

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "42600242b0b1"
down_revision = "1e19cf59477d"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("nutrition_label", "protein_unit")
    op.drop_column("nutrition_label", "transFat_unit")
    op.drop_column("nutrition_label", "sugars_unit")
    op.drop_column("nutrition_label", "iron_unit")
    op.drop_column("nutrition_label", "fiber_unit")
    op.drop_column("nutrition_label", "cholesterol_unit")
    op.drop_column("nutrition_label", "carbohydrates_unit")
    op.drop_column("nutrition_label", "sodium_unit")
    op.drop_column("nutrition_label", "saturatedFat_unit")
    op.drop_column("nutrition_label", "calcium_unit")
    op.drop_column("nutrition_label", "fat_unit")
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "nutrition_label",
        sa.Column("fat_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("calcium_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column(
            "saturatedFat_unit", sa.VARCHAR(), autoincrement=False, nullable=True
        ),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("sodium_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column(
            "carbohydrates_unit", sa.VARCHAR(), autoincrement=False, nullable=True
        ),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("cholesterol_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("fiber_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("iron_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("sugars_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("transFat_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    op.add_column(
        "nutrition_label",
        sa.Column("protein_unit", sa.VARCHAR(), autoincrement=False, nullable=True),
    )
    # ### end Alembic commands ###
