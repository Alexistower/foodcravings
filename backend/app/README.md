#install postgres
figure it out, probably:
```bash
sudo apt-get install postgres-client -y
```
or something
##create postgres user and db for dev
```bash
sudo -u postgres createdb food
sudo -u postgres createuser food -P
```
use password as password for dev 

then assign database food to user food
```bash
sudo -u postgres psql food
```
```sql
ALTER DATABASE food OWNER TO food;
```
\q to quit

# AWS
to launch on a new aws instance be sure to export the DATABASE_URL 
the database password is a secret so never store it in our repo