import os
import unittest
from datetime import datetime

from sqlalchemy import func
from .. import app
from .. import models
import decimal


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = (
            os.environ.get("DATABASE_URL")
            or "postgres://food:password@localhost:5432/food"
        )
        self.app = app.test_client()

    def test_recipe_construct(self):
        r = models.Recipe(name="Corn Salsa", health_labels=["Vegetarian"], calories=250)
        assert r.name == "Corn Salsa"
        assert r.health_labels == ["Vegetarian"]
        assert r.calories == 250
        assert type(r.name) == str
        assert type(r.health_labels) == list
        assert type(r.calories) == int

    def test_recipe_label_construct(self):
        r = models.Recipe(name="Corn Salsa", health_labels=["Vegetarian"], calories=250)
        l = models.RecipeLabel(calories=1000000, fat=10000, protein=10)
        r.recipe_label = l
        assert r.recipe_label is l
        assert l.recipe is r
        assert l.calories == 1000000
        assert l.fat == 10000
        assert l.protein == 10

    def test_ingredient_construct(self):
        r = models.Recipe(name="Corn Salsa", health_labels=["Vegetarian"], calories=250)
        i = models.Ingredient(name="Corn", quantity=200, measure="Cobs")
        r.ingredients.append(i)
        assert r.ingredients == [i]
        assert i.recipe is r
        assert i.name == "Corn"
        assert i.quantity == 200
        assert i.measure == "Cobs"

    def test_db_recipe(self):
        row = models.Recipe.query.get(352)
        fields = {
            "id": int,
            "name": str,
            "image_url": str,
            "health_labels": list,
            "diet_labels": list,
            "instructions": str,
            "servings": int,
            "calories": decimal.Decimal,
            "ingredients": decimal.Decimal,
            "menu_items": list,
            "recipe_label": models.RecipeLabel,
            "created_at": datetime,
            "updated_at": datetime,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)

    def test_db_recipe_label(self):
        row = models.RecipeLabel.query.get(352)
        fields = {
            "id": int,
            "calories": decimal.Decimal,
            "fat": decimal.Decimal,
            "saturatedFat": decimal.Decimal,
            "transFat": decimal.Decimal,
            "cholesterol": decimal.Decimal,
            "sodium": decimal.Decimal,
            "carbohydrates": decimal.Decimal,
            "fiber": decimal.Decimal,
            "sugars": decimal.Decimal,
            "protein": decimal.Decimal,
            "calcium": decimal.Decimal,
            "iron": decimal.Decimal,
            "recipe_id": int,
            "recipe": models.Recipe,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)

    def test_db_ingredient(self):
        row = models.Ingredient.query.get(3169)
        fields = {
            "id": int,
            "name": str,
            "quantity": decimal.Decimal,
            "measure": str,
            "text": str,
            "recipe_id": int,
            "recipe": models.Recipe,
            "grocery_items": list,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)

    if __name__ == "__main__":
        unittest.main()
