import os
import unittest
from datetime import datetime

from sqlalchemy import func
from .. import app, models
import decimal


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = (
            os.environ.get("DATABASE_URL")
            or "postgres://food:password@localhost:5432/food"
        )
        self.app = app.test_client()

    def test_restaurant_construct(self):
        r = models.Restaurant(name="Torchy's", cuisines=["Mexican"], priceRange="$$")
        assert r.name == "Torchy's"
        assert r.cuisines == ["Mexican"]
        assert r.priceRange == "$$"
        assert type(r.name) == str
        assert type(r.cuisines) == list
        assert type(r.priceRange) == str

    def test_menu_item_construct(self):
        r = models.Restaurant(name="Torchy's", cuisines=["Mexican"], priceRange="$$")
        m = models.MenuItem(
            name="Chips and Salsa",
            description="Fresh chips with house salsa",
            price=3.35,
        )
        r.menu_items.append(m)

        assert r.menu_items == [m]
        assert m.restaurant is r
        assert m.name == "Chips and Salsa"
        assert m.description == "Fresh chips with house salsa"
        assert m.price == 3.35
        assert type(m.name) == str
        assert type(m.description) == str
        assert type(m.price) == float

    def test_db_restaurant(self):
        row = models.Restaurant.query.get(33)
        fields = {
            "id": int,
            "image_url": str,
            "name": str,
            "address": str,
            "cuisines": list,
            "priceRange": str,
            "phone": str,
            "created_at": datetime,
            "updated_at": datetime,
            "menu_items": list,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)

    def test_db_menu_item(self):
        row = models.MenuItem.query.get(352)
        fields = {
            "id": int,
            "name": str,
            "description": str,
            "price": decimal.Decimal,
            "price_string": str,
            "restaurant_id": int,
            "restaurant": models.Restaurant,
            "recipes": list,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)


if __name__ == "__main__":
    unittest.main()
