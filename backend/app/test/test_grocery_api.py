import os
import unittest
import decimal

from datetime import datetime
from sqlalchemy import func

from .. import app
from .. import models


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config["TESTING"] = True
        app.config["SQLALCHEMY_DATABASE_URI"] = (
            os.environ.get("DATABASE_URL")
            or "postgres://food:password@localhost:5432/food"
        )
        self.app = app.test_client()

    def test_grocery_construct(self):
        g = models.Grocery(name="Corn", price=10.0, brand="Whole Foods", serving_size=2)
        assert g.name == "Corn"
        assert g.price == 10.0
        assert g.brand == "Whole Foods"
        assert g.serving_size == 2
        assert type(g.name) == str
        assert type(g.price) == float

    def test_grocery_label_construct(self):
        g = models.Grocery(name="Corn", price=10.0, brand="Whole Foods", serving_size=2)
        l = models.NutritionLabel(calories=1000000, fat=10000)
        g.label = l
        assert g.label is l
        assert l.grocery is g
        assert l.calories == 1000000
        assert l.fat == 10000

    def test_db_grocery(self):
        rowId = models.Grocery.query.order_by(func.random()).first().id
        row = models.Grocery.query.get(336)
        fields = {
            "id": int,
            "name": str,
            "price": decimal.Decimal,
            "brand": str,
            "image_url": str,
            "category": str,
            "serving_size": decimal.Decimal,
            "serving_size_unit": str,
            "ingredients": list,
            "label": models.NutritionLabel,
            "created_at": datetime,
            "updated_at": datetime,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            if typ is not None and field is not None:
                assert issubclass(type(getattr(row, field)), typ)

    def test_db_grocery_label(self):
        row = models.NutritionLabel.query.get(452)
        fields = {
            "id": int,
            "calories": decimal.Decimal,
            "fat": decimal.Decimal,
            "saturatedFat": decimal.Decimal,
            "transFat": decimal.Decimal,
            "cholesterol": decimal.Decimal,
            "sodium": decimal.Decimal,
            "carbohydrates": decimal.Decimal,
            "fiber": decimal.Decimal,
            "sugars": decimal.Decimal,
            "protein": decimal.Decimal,
            "calcium": decimal.Decimal,
            "iron": decimal.Decimal,
            "grocery_id": int,
            "grocery": models.Grocery,
        }
        for field in row.__table__.columns._data.keys():
            assert field in fields
            typ = fields[field]
            assert issubclass(type(getattr(row, field)), typ)


if __name__ == "__main__":
    unittest.main()
