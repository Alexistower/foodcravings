from flask import Flask
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from .util import encoder

import os


class Config:
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("DATABASE_URL") or "postgres://food:password@localhost:5432/food"
    )
    SQLALCHEMY_TRACK_MODIFICATIONS = True


app = Flask(__name__, static_url_path="", static_folder="static")
app.json_encoder = encoder.JSONEncoder
app.config.from_object(Config)

db = SQLAlchemy(app)
db.init_app(app)
ma = Marshmallow(app)

manager = APIManager(app, flask_sqlalchemy_db=db)

migrate = Migrate(app, db)

CORS(app)


from . import routes
from . import api
