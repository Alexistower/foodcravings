from .. import manager, ma, models

manager.create_api(models.Restaurant, page_size=6)
manager.create_api(models.MenuItem)


class RestaurantSchema(ma.ModelSchema):
    class Meta:
        model = models.Restaurant


class MenuItemSchema(ma.ModelSchema):
    class Meta:
        model = models.MenuItem
