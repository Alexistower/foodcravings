from .. import manager, ma, models

manager.create_api(models.Recipe, page_size=50, max_page_size=100)
manager.create_api(models.Ingredient)
manager.create_api(models.RecipeLabel)


class RecipeSchema(ma.ModelSchema):
    class Meta:
        model = models.Recipe


class IngredientSchema(ma.ModelSchema):
    class Meta:
        model = models.Ingredient


class RecipeLabelSchema(ma.ModelSchema):
    class Meta:
        model = models.RecipeLabel
