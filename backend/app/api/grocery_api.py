from .. import manager, ma, models


manager.create_api(models.Grocery, page_size=50, max_page_size=100)
manager.create_api(models.NutritionLabel)


class BasicSchema(ma.Schema):
    class Meta:
        fields = ("name", "image_url", "id")


class GrocerySchema(ma.ModelSchema):
    class Meta:
        model = models.Grocery

    # ingredients = ma.Nested(BasicSchema)


class NutritionLabelSchema(ma.ModelSchema):
    class Meta:
        model = models.NutritionLabel
