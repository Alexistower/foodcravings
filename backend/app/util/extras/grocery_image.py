import requests
import json
import urllib.parse as u
from .. import grocery
from .. import db


for groc in grocery.Grocery.query.all():
    walmart_search_url = "https://www.walmart.com/search/?query=" + u.quote(groc.name)
    try:
        if groc.image_url == "":
            response = requests.request("GET", walmart_search_url)
            idx = response.text.find(
                '<script id="searchContent" type="application/json">'
            )
            text = response.text[
                idx + len('<script id="searchContent" type="application/json">') :
            ]
            text = text[: text.index("\n") - 9]
            item_url = json.loads(text)["searchContent"]["preso"]["items"][0][
                "productPageUrl"
            ]
            # print("got to page url")
            walmart_product_url = "https://www.walmart.com" + item_url
            response = requests.request("GET", walmart_product_url)
            # print("searching page url")
            idx = response.text.find('<meta property="og:image" content="')
            end_idx = response.text.find('"/><meta property="og:type"')
            text = response.text[
                idx + len('<meta property="og:image" content="') : end_idx
            ]
            groc.image_url = text
            db.session.commit()
    except:
        continue
