import requests
from .. import recipe
from .. import db

all_recipes = {}

for rec in recipe.Recipe.query.all():
    recipes_edamam_url = (
        "https://api.edamam.com/search?q="
        + rec.name
        + "&app_id=edd380fe&app_key=b3601d60b8bfba5d6b6e9658d10f4f5d"
    )
    # print(recipes_edamam_url)
    try:
        ed_response = requests.request("GET", recipes_edamam_url)
        if len(ed_response.json()["hits"]):
            resp_label = ed_response.json()["hits"][0]["recipe"]["totalNutrients"]
            label = recipe.RecipeLabel()
            rec.recipe_label = label
            try:
                label.calories = resp_label["ENERC_KCAL"]["quantity"]
            except:
                pass
            try:
                label.fat = resp_label["FAT"]["quantity"]
            except:
                pass
            try:
                label.saturatedFat = resp_label["FASAT"]["quantity"]
            except:
                pass
            try:
                label.carbohydrates = resp_label["CHOCDF"]["quantity"]
            except:
                pass
            try:
                label.cholesterol = resp_label["CHOLE"]["quantity"]
            except:
                pass
            try:
                label.protein = resp_label["PROCNT"]["quantity"]
            except:
                pass
            try:
                label.sugars = resp_label["SUGAR"]["quantity"]
            except:
                pass
            try:
                label.calcium = resp_label["CA"]["quantity"]
            except:
                pass
            try:
                label.fiber = resp_label["FIBTG"]["quantity"]
            except:
                pass
            try:
                label.sodium = resp_label["NA"]["quantity"]
            except:
                pass
            try:
                label.iron = resp_label["FE"]["quantity"]
            except:
                pass
    except:

        continue
    db.session.commit()
