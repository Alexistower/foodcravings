import requests
import json
import urllib.parse as u


def recipe_ingredients():
    with open("recipes_saved.json", "r") as rest:
        results = json.load(rest)
        for recipe_obj in results:
            for ingredient in recipe_obj["recipe"]["ingredients"]:
                yield ingredient["food"]


with open("groceries_saved.txt", "w") as output:
    output.write("[\n")
    for ingredient in recipe_ingredients():
        walmart_search_url = "https://www.walmart.com/search/?query=" + u.quote(
            ingredient
        )
        response = requests.request("GET", walmart_search_url)
        idx = response.text.find('<script id="searchContent" type="application/json">')
        text = response.text[
            idx + len('<script id="searchContent" type="application/json">') :
        ]
        text = text[: text.index("\n") - 9]
        item_url = json.loads(text)["searchContent"]["preso"]["items"][0][
            "productPageUrl"
        ]
        walmart_product_url = "https://www.walmart.com" + item_url

        response = requests.request("GET", walmart_product_url)
        idx = response.text.find('<script id="item" type="application/json">')
        text = response.text[idx + len('<script id="item" type="application/json">') :]
        text = text[: text.index("\n") - 9]
        item = json.loads(text)

        name = item["item"]["product"]["midasContext"]["query"]
        brand = item["item"]["product"]["midasContext"]["brand"]

        usda_body = {"generalSearchInput": name, "brandOwner": brand}
        usda_general_search = "https://15pZ1MkPiSNR6aIn72pWb62qhjKs1xfivnOcaMA3@api.nal.usda.gov/fdc/v1/search"
        response = requests.request("GET", usda_general_search)
        id = response.json()[0]["id"]  # some way of getting id of first item

        usda_item_search = (
            "https://15pZ1MkPiSNR6aIn72pWb62qhjKs1xfivnOcaMA3@api.nal.usda.gov/fdc/v1/"
            + str(id)
        )
        response = requests.request("GET", usda_general_search)
        item = {key: value for (key, value) in (item.items() + response.json().items())}
        output.write(json.dumps(item))
        output.write(",")
        break
    output.write("]")
