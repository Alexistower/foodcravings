from .. import recipe
from .. import db


all_ingr = {}
all_groceries = {}

count = 0
names = set()
names_bad = set()
maps = {}


def make_connections():
    global count
    global names
    global names_bad
    # for ingr in recipe.Ingredient.query.all():
    #     all_ingr[ingr.id] = ingr
    #
    # for groc in grocery.Grocery.query.all():
    #     ingr = all_ingr[groc.ingredient_id]
    #     ingr.groceries.append(groc)
    #     temp = list(set(ingr.groceries))
    #     ingr.groceries = temp
    #     db.session.add(ingr)
    # for ingr in recipe.Ingredient.query.all():
    #     if(len(ingr.groceries) == 0):
    #         count += 1
    #     # print(ingr.groceries)
    for rec in recipe.Recipe.query.all():
        for ingr in rec.ingredients:
            if len((ingr.grocery_items)) > 0:
                names.add(ingr.name.lower())
            if len(ingr.grocery_items) == 0:
                names_bad.add(ingr.name.lower())
    print(len(names))
    print(len(names_bad))
    for rec in recipe.Recipe.query.all():
        for ingr in rec.ingredients:
            if len(ingr.groceries) > 0:
                if ingr.name.lower() in maps and len(maps[ingr.name.lower()]) < len(
                    ingr.groceries
                ):
                    maps[ingr.name.lower()] = ingr.groceries
                elif ingr.name.lower() not in maps:
                    maps[ingr.name.lower()] = ingr.groceries
    for ingr in recipe.Ingredient.query.all():
        if ingr.name.lower() in maps:
            for rel in maps[ingr.name.lower()]:
                ingr.grocery_items.append(rel)
            db.session.add(ingr)
    db.session.commit()
    names = set()
    names_bad = set()
    for rec in recipe.Recipe.query.all():
        for ingr in rec.ingredients:
            if len((ingr.grocery_items)) > 0:
                names.add(ingr.name.lower())
            if len(ingr.grocery_items) == 0:
                names_bad.add(ingr.name.lower())
    print(len(names))
    print(len(names_bad))


make_connections()
