import requests
import json


with open("restaurants_saved.json", "w") as output:
    output.write("[\n")
    headers = {
        "X-RapidAPI-Host": "us-restaurant-menus.p.rapidapi.com",
        "X-RapidAPI-Key": "29013a07f8msh758d5b4d4eda58ep196d7djsn178ca82255a8",
    }
    for i in range(1, 10):
        restaurant_url = (
            "https://us-restaurant-menus.p.rapidapi.com/restaurants/search?lat=30.2862&lon=-97.7416&distance=10&q=&page="
            + str(i)
            + "&fullmenu"
        )
        response = requests.request("GET", restaurant_url, headers=headers)
        output.write(json.dumps(response.json()))
        output.write(",")
    output.write("]\n")
