import requests
import json
import urllib.parse as u
from .. import restaurant, recipe, grocery
from .. import db


all_recipes = {}
all_groceries = {}
all_restaurants = {}
# all_ingredients = set()

# print(recipe.Ingredient.query.all())
# apiKeys = {"ed6a641603fd447b85d06d61f05670e1": 150, "a60cbb1f8cd546bdaad0c21d3843a9bd": 0}
apiKeys = {
    "c404cc3ff0ac4865b5429b9f13dc72ff": 0,
    "f91924aa88154356a0fb43d9b5a4d303": 0,
    "24fa348e696a46b8897a9d8039138f67": 0,
    "bd423bcc6d7a499d9d6e5249408871b6": 0,
    "b97cd1e2d5d94172a9d236c40697af32": 0,
    "316775947edd479184869bcf44c4ac12": 0,
    "7a424618677f452f923e13375011d21e": 0,
    "33e0666ffd344390af575d4b7a9be5d1": 0,
    "307ac8faa7064c67a7ef1d5a7a21dfe8": 0,
    "0213294e606149f684ef63bb8446e94f": 0,
    "9092252def93422a9f4c4b0caa2928d3": 0,
    "f192306955ce4ba4a41c9058bb4e5357": 0,
    "70e515013c0b4be086c69f44685d622a": 0,
    "1710962d6cf8499f898f815bcd49d0d8": 0,
    "e4dbbe3a3b4647ae9c1805b9d9914bd8": 0,
    "bfb470a1a5464cd1bbb80bb275ffd5ca": 0,
    "aeec80258c9f4ff5981692112cf2b171": 0,
    "10e1656a43704baeb39d242238191470": 0,
    "d9329f4107e945f3951fc55664992725": 0,
    "cf3266da545b4702a8b42e250756314e": 0,
    "b1967624479a40c183d848f8ef4234a1": 0,
    "c77877e86517482fb522a91f3acc9f85": 0,
    "32abca44454c4ee3b3fd7e88b5544021": 0,
    "1d862990d971485c813aae9ae56ca258": 0,
    "c5cdcda2ba7847cca83f4d217721cd7d": 0,
    "7612346716b34b9495c8e21d77915e27": 0,
    "5f24a4c4118d424baca3502165f68ea7": 0,
    "13e4cbc8b24f4f1c8ae696f7deb8377e": 0,
    "1030894855514f2892e0ac6458e5e16f": 0,
    "5ccd3b8352ac4cc5be1891d0cd5ff343": 0,
    "5a7a8d54b12843529dbefbc0df05ec1b": 0,
    "0d6fd4eea2b64cc5810645f96caeb841": 0,
}
to_add = {}


def get_spoo_api_key():
    try:
        a = set(apiKeys.keys())
        i = a.pop()
        while apiKeys[i] >= 150:
            i = a.pop()
        apiKeys[i] += 1
        return i
    except:
        return None


def populate_ingredient(ingr):
    print(ingr.name.lower())
    if ingr.name.lower() in all_groceries:
        for g in all_groceries[ingr.name.lower()]:
            ingr.groceries.append(g)
        return
    walmart_search_url = "https://www.walmart.com/search/?query=" + u.quote(
        "fresh" + ingr.name
    )
    try:
        response = requests.request("GET", walmart_search_url)
    except:
        print("walmart failed")
        return
    idx = response.text.find('<script id="searchContent" type="application/json">')
    text = response.text[
        idx + len('<script id="searchContent" type="application/json">') :
    ]
    text = text[: text.index("\n") - 9]
    for i in range(3):
        try:
            item_url = json.loads(text)["searchContent"]["preso"]["items"][i][
                "productPageUrl"
            ]
        except:
            continue
        try:
            walmart_product_url = "https://www.walmart.com" + item_url
        except:
            continue

        response = requests.request("GET", walmart_product_url)
        idx = response.text.find('<script id="item" type="application/json">')
        text = response.text[idx + len('<script id="item" type="application/json">') :]
        text = text[: text.index("\n") - 9]
        try:
            item = json.loads(text)
        except:
            continue

        # if item["item"]["productId"] in all_groceries:
        #     ingr.groceries.append(all_groceries[item["item"]["productId"]])
        #     continue
        try:
            prod = item["item"]["product"]["midasContext"]
            if prod["price"] > 50:
                continue
            name = prod["query"]
            brand = prod["brand"]
            g = grocery.Grocery()
            g.name = name
            g.brand = brand
            g.price = prod["price"]
            g.category = prod["categoryPathName"]
            # print(g.name, g.brand)
            url = "https://api.nal.usda.gov/fdc/v1/search"

            querystring = {"api_key": "15pZ1MkPiSNR6aIn72pWb62qhjKs1xfivnOcaMA3"}

            payload = (
                '{\n\t"generalSearchInput": "'
                + name
                + '",\n\t"brandOwner": "'
                + brand
                + '"}'
            )
            headers = {
                "Content-Type": "application/json",
            }

            response = requests.request(
                "POST",
                url,
                data=payload.encode("utf-8"),
                headers=headers,
                params=querystring,
            )
            # print(response.json())
            id = response.json()["foods"][0][
                "fdcId"
            ]  # some way of getting id of first item
        except:
            continue
        usda_item_search = (
            "https://15pZ1MkPiSNR6aIn72pWb62qhjKs1xfivnOcaMA3@api.nal.usda.gov/fdc/v1/"
            + str(id)
        )
        usd_nut = requests.request(
            "GET", usda_item_search, headers=headers, params=querystring
        ).json()
        # print(usd_nut)
        try:
            g.serving_size = usd_nut["servingSize"]
        except:
            pass
        try:
            g.serving_size_unit = usd_nut["servingSizeUnit"]
        except:
            pass
        label = grocery.NutritionLabel()
        try:
            usd_nut = usd_nut["labelNutrients"]
        except:
            pass
        try:
            label.calories = usd_nut["calories"]["value"]
        except:
            pass
        try:
            label.fat = usd_nut["fat"]["value"]
        except:
            pass
        try:
            label.saturatedFat = usd_nut["saturatedFat"]["value"]
        except:
            pass
        try:
            label.transFat = usd_nut["transFat"]["value"]
        except:
            pass
        try:
            label.cholesterol = usd_nut["cholesterol"]["value"]
        except:
            pass
        try:
            label.sodium = usd_nut["sodium"]["value"]
        except:
            pass
        try:
            label.carbohydrates = usd_nut["carbohydrates"]["value"]
        except:
            pass
        try:
            label.fiber = usd_nut["fiber"]["value"]
        except:
            pass
        try:
            label.sugars = usd_nut["sugars"]["value"]
        except:
            pass
        try:
            label.protein = usd_nut["protein"]["value"]
        except:
            pass
        try:
            label.calcium = usd_nut["calcium"]["value"]
        except:
            pass
        try:
            label.iron = usd_nut["iron"]["value"]
        except:
            pass
        g.label = label
        # db.session.add(label)
        ingr.groceries.append(g)
        if ingr.name.lower not in all_groceries:
            all_groceries[ingr.name.lower()] = [g]
        else:
            all_groceries[ingr.name.lower()].append(g)
        # db.session.add(g)


def populate_menu_item(item):
    print(item.name)
    key = get_spoo_api_key()
    if key == None:
        raise Exception("Out of requests")
    recipes_spoonacular_url = (
        "https://api.spoonacular.com/recipes/findByIngredients?ingredients="
        + u.quote(item.description.replace(".", ""))
        + ","
        + u.quote(item.name.replace(".", ""))
        + "&number=10&apiKey="
        + key
    )
    # print(recipes_spoonacular_url)
    try:
        response = requests.request("GET", recipes_spoonacular_url, timeout=10)
    except:
        return
    if len(response.json()):
        try:
            title = u.quote(response.json()[0]["title"])
        except:
            populate_menu_item(item)
            return
        # print(title)
        recipes = 0
        while recipes < 2 and recipes < len(response.json()):
            title = u.quote(response.json()[recipes]["title"])
            recipes_edamam_url = (
                "https://api.edamam.com/search?q="
                + title
                + "&app_id=edd380fe&app_key=b3601d60b8bfba5d6b6e9658d10f4f5d"
            )
            # print(recipes_edamam_url)
            try:
                ed_response = requests.request("GET", recipes_edamam_url)
            except:
                recipes += 1
                continue
            if len(ed_response.json()["hits"]):
                count = 2
                i = 0
                while i < len(ed_response.json()["hits"]) and i < count:
                    rec_resp = ed_response.json()["hits"][i]["recipe"]
                    if rec_resp["label"].lower() in all_recipes:
                        item.recipes.append(all_recipes[rec_resp["label"].lower()])
                        i += 1
                        continue
                    rec_obj = recipe.Recipe()
                    rec_obj.name = rec_resp["label"]
                    rec_obj.health_labels = rec_resp["healthLabels"]
                    rec_obj.diet_labels = rec_resp["dietLabels"]
                    rec_obj.image_url = rec_resp["image"]
                    rec_obj.instructions = rec_resp["url"]
                    rec_obj.servings = rec_resp["yield"]
                    rec_obj.calories = rec_resp["calories"]
                    for ingr in rec_resp["ingredients"]:
                        ingr_obj = recipe.Ingredient()
                        ingr_obj.name = ingr["food"]
                        ingr_obj.text = ingr["text"]
                        ingr_obj.quantity = ingr["quantity"]
                        ingr_obj.measure = ingr["measure"]
                        populate_ingredient(ingr_obj)
                        rec_obj.ingredients.append(ingr_obj)
                        # db.session.add(ingr_obj)
                    i += 1
                    item.recipes.append(rec_obj)
                    all_recipes[rec_resp["label"].lower()] = rec_obj
                    # db.session.add(rec_obj)
            recipes += 1


def menu_items():
    with open("restaurants_abridged.json", "r") as rest:
        results = json.load(rest)
        for result_obj in results:
            for rest in result_obj["result"]["data"]:
                r_obj = restaurant.Restaurant()
                r_obj.name = rest["restaurant_name"]
                r_obj.address = rest["address"]["formatted"]
                if (r_obj.name, r_obj.address) in all_restaurants:
                    print(r_obj.name, " already finished")
                    continue
                r_obj.cuisines = rest["cuisines"]
                r_obj.phone = rest["restaurant_phone"]
                r_obj.priceRange = rest["price_range"]
                for menu in rest["menus"]:
                    for section in menu["menu_sections"]:
                        for item in section["menu_items"]:
                            menu_item = restaurant.MenuItem()
                            try:
                                menu_item.name = item["name"]
                                menu_item.description = item["description"]
                            except:
                                continue
                            try:
                                menu_item.price = item["pricing"][0]["price"]
                                menu_item.price_string = item["pricing"][0][
                                    "priceString"
                                ]
                            except:
                                pass

                            populate_menu_item(menu_item)
                            r_obj.menu_items.append(menu_item)
                            # db.session.add(menu_item)
                db.session.add(r_obj)
                print("finished ", r_obj.name)
                db.session.commit()
                # break
            # break


def prepare_cache():
    for ingr in recipe.Ingredient.query.all():
        all_groceries[ingr.name.lower()] = ingr.groceries
    for rec in recipe.Recipe.query.all():
        all_recipes[rec.name.lower()] = rec
    for rest in restaurant.Restaurant.query.all():
        all_restaurants[(rest.name, rest.address)] = rest
    # for ing in recipe.Ingredient.query.all():
    #     populate_ingredient(ing)
    #     # print(all_groceries)
    # db.session.commit()


prepare_cache()
menu_items()
