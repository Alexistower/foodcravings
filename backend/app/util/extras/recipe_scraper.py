import json
import requests
import urllib.parse as u


def menu_items():
    with open("restaurants_saved.json", "r") as rest:
        results = json.load(rest)
        for result_obj in results:
            for restaurant in result_obj["result"]["data"]:
                for menu in restaurant["menus"]:
                    for section in menu["menu_sections"]:
                        for item in section["menu_items"]:
                            try:
                                if len(item["description"]):
                                    yield item
                            except:
                                pass


# menu_items()
with open("recipes_saved.json", "a") as output:
    for menu_item in menu_items():
        count = 0
        count += 1
        try:
            recipes_spoonacular_url = (
                "https://api.spoonacular.com/recipes/findByIngredients?ingredients="
                + menu_item["description"]
                + ","
                + menu_item["name"]
                + "&number=2&apiKey=ed6a641603fd447b85d06d61f05670e1"
            )
            response = requests.request("GET", recipes_spoonacular_url)
            if len(response.json()):
                title = u.quote(response.json()[0]["title"])
                # print(title)
                recipes_edamam_url = (
                    "https://api.edamam.com/search?q="
                    + title
                    + "&app_id=edd380fe&app_key=b3601d60b8bfba5d6b6e9658d10f4f5d"
                )
                # print(recipes_edamam_url)
                response = requests.request("GET", recipes_edamam_url)
                if len(response.json()["hits"]):
                    output.write(json.dumps(response.json()["hits"][0]))
                    output.write(",")
        except Exception as e:
            print(count)
            print(type(e))
    output.write("]\n")
