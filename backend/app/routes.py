import requests
from flask import jsonify, json
import random
import html

from . import app
from . import db
from flask import jsonify
from .models import *
from sqlalchemy import func, desc
from .models import *


@app.route("/")
def hello_world():
    return "Welcome to Food Cravings"


# Make a route that will return JSON object with links from top 50 most frequent groceries
# graph =
# {"nodes": [{"id": number1, "name": "groceryItem1"}, {"id": number2, "name": "recipe1"}]},
#  "links": [{"source": number1, "target": number2}]}
@app.route("/api/networkgraph")
def frequentGroceries():
    return "DEFAULT"


@app.route("/api/menutrition")
def groc_graph():
    groceryLabelUnit = {
        "calcium": "mg",
        "carbohydrates": "g",
        "cholesterol": "mg",
        "fat": "g",
        "fiber": "g",
        "iron": "mg",
        "protein": "g",
        "saturatedFat": "g",
        "sodium": "mg",
        "sugars": "g",
        "transFat": "g",
    }
    output = dict()
    output["name"] = "Restaurants"
    output["children"] = []
    for res in Restaurant.query.all():
        rest_info = dict()
        rest_info["name"] = res.name
        rest_info["children"] = []
        output["children"].append(rest_info)
        for menu_item in res.menu_items:
            if len(rest_info["children"]) > 8:
                break
            if random.random() > 0.5:
                continue
            item_info = dict()
            item_info["name"] = menu_item.name
            totals = {
                "calcium": 0,
                "carbohydrates": 0,
                "cholesterol": 0,
                "fat": 0,
                "fiber": 0,
                "iron": 0,
                "protein": 0,
                "saturatedFat": 0,
                "sodium": 0,
                "sugars": 0,
                "transFat": 0,
            }
            counts = {
                "calcium": 0,
                "calories": 0,
                "carbohydrates": 0,
                "cholesterol": 0,
                "fat": 0,
                "fiber": 0,
                "iron": 0,
                "protein": 0,
                "saturatedFat": 0,
                "sodium": 0,
                "sugars": 0,
                "transFat": 0,
            }

            for recipe in menu_item.recipes:
                nut_label = recipe.recipe_label
                # cols = [r for r in nut_label.column_descriptions
                if nut_label is None:
                    continue
                for column, value in nut_label.__dict__.items():
                    if column in totals and value >= 0:
                        totals[column] += value / recipe.servings
                        counts[column] += 1
            item_info["children"] = []
            for key, value in totals.items():
                leaf = dict()
                if groceryLabelUnit[key] == "mg":
                    value = value / 1000
                if counts[key] == 0:
                    counts[key] += 1
                leaf["name"] = key + ":" + str(round(value / counts[key], 2))
                leaf["value"] = round(value / counts[key], 2)
                item_info["children"].append(leaf)
            rest_info["children"].append(item_info)
    return jsonify(output)


@app.route("/bargraph")
def cuisine_avg_price():
    output = dict()
    for res in Restaurant.query.all():
        parsed = html.unescape(res.cuisines)[1:-1].split(",")
        totalcost = 0
        numitems = 0
        for menuitem in res.menu_items:
            if menuitem.price > 0:
                totalcost += menuitem.price
                numitems += 1

        for cuisine in parsed:
            if cuisine in output:
                output[cuisine][0] += totalcost
                output[cuisine][1] += numitems
            else:
                output[cuisine] = [totalcost, numitems]

    for x in output.keys():
        if output[x][0] > 0:
            output[x] = output[x][0] / output[x][1]
        else:
            output[x] = 0

    result = []
    for x in output.keys():
        result.append({"Cuisine": str(x), "Average Price": output[x]})

    return jsonify(result)


@app.route("/api/empcities")
def city_vals():
    resp = requests.get(
        "http://api.ethicalemployers.club/v1/city/list/?size=150"
    ).json()["content"]
    output = []
    for c in resp:
        o = {}
        o["name"] = c["name"] + ":" + str(round(c["overallScore"], 2))
        o["location"] = json.loads(c["latlon"])
        output.append(o)

    return jsonify(output)
