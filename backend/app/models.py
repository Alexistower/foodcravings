from sqlalchemy.dialects.postgresql import ARRAY as Array
from . import db

association_table_M2R = db.Table(
    "association_table_M2R",
    db.Column("menu_item_id", db.Integer, db.ForeignKey("menu_item.id")),
    db.Column("recipe_id", db.Integer, db.ForeignKey("recipes.id")),
)


class Restaurant(db.Model):
    __tablename__ = "restaurant"
    id = db.Column(db.Integer, primary_key=True)
    image_url = db.Column(db.String, default="")
    name = db.Column(db.String, nullable=False)
    address = db.Column(db.String, default="")
    cuisines = db.Column(db.String, default="")
    priceRange = db.Column(db.String, default="")
    phone = db.Column(db.String, default="")
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(
        db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now()
    )
    menu_items = db.relationship("MenuItem", back_populates="restaurant")


class MenuItem(db.Model):
    __tablename__ = "menu_item"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    description = db.Column(db.String, default="")
    price = db.Column(db.Numeric(scale=2), default=-1.0)
    price_string = db.Column(db.String, default="")
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant.id"))
    restaurant = db.relationship("Restaurant", back_populates="menu_items")
    recipes = db.relationship(
        "Recipe", secondary=association_table_M2R, back_populates="menu_items"
    )


association_table_I2G = db.Table(
    "association_table_I2G",
    db.Column("ingredient_id", db.Integer, db.ForeignKey("ingredients.id")),
    db.Column("grocery_id", db.Integer, db.ForeignKey("groceries.id")),
)


class Recipe(db.Model):
    __tablename__ = "recipes"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, default="", nullable=False)
    image_url = db.Column(db.String, default="")
    health_labels = db.Column(Array(db.String))
    diet_labels = db.Column(Array(db.String))
    instructions = db.Column(db.String, default="")
    servings = db.Column(db.Integer, default=-1.0)
    calories = db.Column(db.Numeric(scale=2), default=-1.0)
    ingredients = db.relationship("Ingredient", back_populates="recipe")
    menu_items = db.relationship(
        "MenuItem", secondary=association_table_M2R, back_populates="recipes"
    )
    recipe_label = db.relationship(
        "RecipeLabel", uselist=False, back_populates="recipe"
    )
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(
        db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now()
    )


class Ingredient(db.Model):
    __tablename__ = "ingredients"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    quantity = db.Column(db.Numeric(scale=2), default=-1.0)
    measure = db.Column(db.String, default="")
    text = db.Column(db.String, default="")
    recipe_id = db.Column(db.Integer, db.ForeignKey("recipes.id"), nullable=False)
    recipe = db.relationship("Recipe", back_populates="ingredients")
    # recipes = db.relationship("Recipe", secondary=association_table_R2I, back_populates="ingredients")
    grocery_items = db.relationship(
        "Grocery", secondary=association_table_I2G, back_populates="ingredients"
    )


class RecipeLabel(db.Model):
    __table__name = "recipe_label"
    id = db.Column(db.Integer, primary_key=True)
    calories = db.Column(db.Numeric(scale=2), default=-1.0)
    fat = db.Column(db.Numeric(scale=2), default=-1.0)
    saturatedFat = db.Column(db.Numeric(scale=2), default=-1.0)
    cholesterol = db.Column(db.Numeric(scale=2), default=-1.0)
    sodium = db.Column(db.Numeric(scale=2), default=-1.0)
    carbohydrates = db.Column(db.Numeric(scale=2), default=-1.0)
    fiber = db.Column(db.Numeric(scale=2), default=-1.0)
    sugars = db.Column(db.Numeric(scale=2), default=-1.0)
    protein = db.Column(db.Numeric(scale=2), default=-1.0)
    calcium = db.Column(db.Numeric(scale=2), default=-1.0)
    iron = db.Column(db.Numeric(scale=2), default=-1.0)
    recipe_id = db.Column(db.Integer, db.ForeignKey("recipes.id"), nullable=False)
    recipe = db.relationship("Recipe", back_populates="recipe_label")


class Grocery(db.Model):
    __tablename__ = "groceries"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    price = db.Column(db.Numeric(precision=2), default=-1.0)
    brand = db.Column(db.String, default="")
    image_url = db.Column(db.String, default="")
    category = db.Column(db.String, default="")
    serving_size = db.Column(db.Numeric(scale=2))
    serving_size_unit = db.Column(db.String, default="")
    ingredients = db.relationship(
        "Ingredient", secondary=association_table_I2G, back_populates="grocery_items"
    )
    label = db.relationship("NutritionLabel", uselist=False, back_populates="grocery")
    created_at = db.Column(db.DateTime, server_default=db.func.now())
    updated_at = db.Column(
        db.DateTime, server_default=db.func.now(), server_onupdate=db.func.now()
    )


class NutritionLabel(db.Model):
    __table__name = "nutrition_label"
    id = db.Column(db.Integer, primary_key=True)
    calories = db.Column(db.Numeric(scale=2), default=-1.0)
    fat = db.Column(db.Numeric(scale=2), default=-1.0)
    saturatedFat = db.Column(db.Numeric(scale=2), default=-1.0)
    transFat = db.Column(db.Numeric(scale=2), default=-1.0)
    cholesterol = db.Column(db.Numeric(scale=2), default=-1.0)
    sodium = db.Column(db.Numeric(scale=2), default=-1.0)
    carbohydrates = db.Column(db.Numeric(scale=2), default=-1.0)
    fiber = db.Column(db.Numeric(scale=2), default=-1.0)
    sugars = db.Column(db.Numeric(scale=2), default=-1.0)
    protein = db.Column(db.Numeric(scale=2), default=-1.0)
    calcium = db.Column(db.Numeric(scale=2), default=-1.0)
    iron = db.Column(db.Numeric(scale=2), default=-1.0)
    grocery_id = db.Column(db.Integer, db.ForeignKey("groceries.id"), nullable=False)
    grocery = db.relationship("Grocery", back_populates="label")
