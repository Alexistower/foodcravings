#! /usr/bin/env bash
docker stop flask-api
docker rm flask-api
docker build --build-arg db=${DATABASE_URL} -t flask-api .
docker run -d --name flask-api -p 80:80 -p 5432:5432 flask-api
