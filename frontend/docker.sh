#! /usr/bin/env bash
docker stop react
docker rm react
docker build -t react .
docker run -d --name react -p 80:80 react
