import unittest
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


class TestGUI(unittest.TestCase):
    url = "http://localhost:3000/"
    url1 = "https://foodcravings.net/"

    def setUp(self):
        ops = webdriver.ChromeOptions()
        ops.add_argument("--no-sandbox")
        ops.add_argument("--headless")
        # self.driver = webdriver.Chrome('./chromedriver')
        self.driver = webdriver.Chrome(options=ops)
        """self.driver = webdriver.Chrome()"""
        self.url = TestGUI.url1

    def test_splash_page_load(self):
        self.driver.get(self.url)
        self.assertIn("Food Cravings", self.driver.title)

    def test_get_to_recipe_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes")

    def test_get_to_restaurant_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(link.text, "Restaurants")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant")

    def test_get_to_groceries_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Groceries")
        self.assertEqual(link.text, "Groceries")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "groceries")

    def test_get_to_about_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("About")
        self.assertEqual(link.text, "About")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "about")

    def test_get_from_recipes_page_to_restaurant_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        next_link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(next_link.text, "Restaurants")
        next_link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant")

    def test_get_from_recipes_page_to_grocery_page(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        next_link = self.driver.find_element_by_link_text("Groceries")
        self.assertEqual(next_link.text, "Groceries")
        next_link.click()
        self.assertEqual(self.driver.current_url, self.url + "groceries")

    def test_get_to_recipe_instance_page_1(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        instance_link = self.driver.find_element_by_partial_link_text("Mango")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes/25")

    def test_get_to_recipe_instance_page_2(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        instance_link = self.driver.find_element_by_partial_link_text("gyoza")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes/27")

    def test_get_to_recipe_instance_page_3(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        instance_link = self.driver.find_element_by_partial_link_text("Wings")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes/28")

    def test_get_to_recipe_instance_page_4(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Recipes")
        self.assertEqual(link.text, "Recipes")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "recipes")
        instance_link = self.driver.find_element_by_partial_link_text("Sausage")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes/29")

    def test_get_to_restaurant_instance_page_1(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(link.text, "Restaurants")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "restaurant")
        instance_link = self.driver.find_element_by_partial_link_text("Potbelly")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant/11")

    def test_get_to_restaurant_instance_page_2(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(link.text, "Restaurants")
        link.click()
        time.sleep(4)
        self.assertEqual(self.driver.current_url, self.url + "restaurant")
        instance_link = self.driver.find_element_by_partial_link_text("Kismet")
        instance_link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant/14")

    def test_get_to_receipe_instance_from_restaurant_instance_page_1(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(link.text, "Restaurants")
        link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "restaurant")
        instance_link = self.driver.find_element_by_partial_link_text("Kismet")
        instance_link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "restaurant/14")
        instance_link = self.driver.find_element_by_partial_link_text(
            "Peanut Butter Cup Brownie Pizza"
        )
        instance_link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "recipes/504")

    def test_get_to_grocery_instance_from_restaurant_instance_page_1(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Restaurants")
        self.assertEqual(link.text, "Restaurants")
        link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "restaurant")
        instance_link = self.driver.find_element_by_partial_link_text("Chipotle")
        instance_link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "restaurant/10")
        instance_link = self.driver.find_element_by_partial_link_text(
            "Low-Fat Cultured Buttermilk"
        )
        instance_link.click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, self.url + "groceries/11964707")

    def test_search_1(self):
        self.driver.get(self.url)
        search = self.driver.find_element_by_class_name("form-control")
        search.send_keys("bell")
        search.send_keys(Keys.ENTER)
        time.sleep(3)
        link = self.driver.find_element_by_partial_link_text("Potbelly")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant/11")

    def test_search_2(self):
        self.driver.get(self.url)
        search = self.driver.find_element_by_class_name("form-control")
        search.send_keys("bell")
        search.send_keys(Keys.ENTER)
        time.sleep(3)
        link = self.driver.find_element_by_partial_link_text("Bellissimo")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "recipes/1328")

    def test_search_3(self):
        self.driver.get(self.url)
        search = self.driver.find_element_by_class_name("form-control")
        search.send_keys("bell")
        search.send_keys(Keys.ENTER)
        time.sleep(3)
        link = self.driver.find_element_by_partial_link_text("Fresh Bellies")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "groceries/55")

    def test_search_pagination(self):
        self.driver.get(self.url)
        search = self.driver.find_element_by_class_name("form-control")
        search.send_keys("f")
        search.send_keys(Keys.ENTER)
        time.sleep(3)
        paginate = self.driver.find_elements_by_class_name("page-link")
        paginate[2].click()
        time.sleep(3)
        link = self.driver.find_element_by_partial_link_text("Fricano")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "restaurant/20")

    def test_advanced_search_1(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Groceries")
        self.assertEqual(link.text, "Groceries")
        link.click()
        time.sleep(2)
        button = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/form/div[2]/button'
        )
        button.click()
        search = self.driver.find_elements_by_class_name("form-control")
        search[1].send_keys("oil")
        select = Select(self.driver.find_elements_by_name("name")[1])
        select.select_by_visible_text("price")
        select = Select(self.driver.find_element_by_name("op"))
        select.select_by_visible_text("greater than")
        search = self.driver.find_element_by_name("val")
        search.send_keys(5)
        search.send_keys(Keys.ENTER)
        time.sleep(2)
        link = self.driver.find_element_by_partial_link_text("Angostura")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "groceries/134")

    def test_advanced_search_2(self):
        self.driver.get(self.url)
        link = self.driver.find_element_by_link_text("Groceries")
        self.assertEqual(link.text, "Groceries")
        link.click()
        time.sleep(2)
        button = self.driver.find_element_by_xpath(
            '//*[@id="root"]/div/div/form/div[2]/button'
        )
        button.click()
        search = self.driver.find_elements_by_class_name("form-control")
        search[1].send_keys("oil")
        select = Select(self.driver.find_elements_by_name("name")[0])
        select.select_by_visible_text("price")
        select = Select(self.driver.find_elements_by_name("name")[1])
        select.select_by_visible_text("price")
        select = Select(self.driver.find_element_by_name("op"))
        select.select_by_visible_text("less than")
        search = self.driver.find_element_by_name("val")
        search.send_keys(10)
        search.send_keys(Keys.ENTER)
        time.sleep(2)
        link = self.driver.find_element_by_partial_link_text("eBook")
        link.click()
        self.assertEqual(self.driver.current_url, self.url + "groceries/290")

    def tearDown(self):
        self.driver.close()
        pass


if __name__ == "__main__":
    unittest.main()
