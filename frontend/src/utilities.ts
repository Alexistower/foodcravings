export class Utilities {
    static arrayStringEval(str: string, obj: object): any {
        return str.split('.').reduce((o: any, i: any) => o[i], obj);
    }
}
