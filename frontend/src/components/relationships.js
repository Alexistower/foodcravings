import React, {Component} from 'react';
import {Card, CardDeck} from 'react-bootstrap';
import "./RelationshipBox.css"


class RelationshipList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            relatedItems: undefined
        };
    }

    render() {
        let relatedItems = this.props.relatedItems;

        return (

            <div class="relDiv">
                <CardDeck className="carDeck">
                    {relatedItems &&
                    relatedItems.length > 0 &&
                    relatedItems.map(item => {
                        return (
                            <Card className="carCard">
                                <Card.Img var="top" class="carImg" src={item.image_url}/>
                                <Card.Link href={item.url} class="carLink">{item.name} </Card.Link>
                            </Card>
                        );
                    })}
                </CardDeck>
            </div>
        );
    }
}

export default RelationshipList;