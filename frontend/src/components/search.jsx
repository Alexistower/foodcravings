import React, {Component} from "react";
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Form from 'react-bootstrap/Form'
import Button from "react-bootstrap/Button";

// props: fields: string[], simple: bool, filterAction: function (filter_list)
export default class Search extends Component {
    constructor(props) {
        super(props);
        const first_field = this.props.fields ? this.props.fields[0] : "name"; /*soft require something passed to name*/
        this.state = {
            filter_list: [], // {name: first_field, op: 'ilike', val: ''}
            advanced: false,
            group: first_field,
            order: "",
            val_list: []
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.updateOrderBy = this.updateOrderBy.bind(this);
        this.toggleAdvanced = this.toggleAdvanced.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.filterInput = this.filterInput.bind(this);
        this.orderBy = this.orderBy.bind(this);
    }

    removeFilter(_, index) {  // Doesn't remove first item
        this.setState(state => {
            state.filter_list.splice(index, 1);
            state.val_list.splice(index, 1);
            return {
                filter_list: state.filter_list,
                val_list: state.val_list
            };
        })
    }

    // Having both the simple and advanced search empty yields 0 results !!!!!!!!
    handleFilterChange(event, index) {
        let name = event.target.name;
        let value = event.target.value;

        this.setState(state => {
            if (name === "val" && (state.filter_list[index].op === "ilike" || state.filter_list[index].op === "not_like")) { // Use pattern matching if op is ilike or
                state.filter_list.splice(index, 1, {...state.filter_list[index], [name]: `%${value}%`});
            } else {
                state.filter_list.splice(index, 1, {...state.filter_list[index], [name]: value});
            }
            if (name === 'val') {
                state.val_list[index] = value;
            }
            return {
                filter_list: state.filter_list,
                val_list: state.val_list
            };
        })
    }

    updateOrderBy(event) {
        const {name, value} = event.target;
        this.setState(() => {
            if (name === 'order') {
                return {
                    order: value
                };
            } // else
            return {
                group: value
            }
        })
    }

    toggleAdvanced() {
        this.setState({advanced: !this.state.advanced});
    }

    addFilter() {
        // console.log(this.state); // this is a great place to check state
        this.setState(state => ({
            filter_list: [...state.filter_list, {name: 'name', op: 'ilike', val: ''}],
            val_list: [...state.val_list, '']
        }));
    }

    filterInput(index = 0) {
        // purpose is to create a component that fills: {"name": String, "op": String, "val": %String%}
        return (
            <InputGroup onChange={(e) => this.handleFilterChange(e, index)}>
                {/* field select */}
                <InputGroup.Prepend>
                    <FormControl as="select" name="name" default={this.props.fields[0]}>
                        {this.props.fields.map((f) => {
                            return (<option>{f}</option>);
                        })}
                    </FormControl>
                </InputGroup.Prepend>
                {/*op select*/}
                <FormControl as="select" name="op">
                    <option value={"ilike"}>similar to</option>
                    <option value={"eq"}>equal</option>
                    <option value={"gte"}>greater than or equal</option>
                    <option value={"lte"}>less than or equal</option>
                    <option value={"gt"}>greater than</option>
                    <option value={"lt"}>less than</option>
                    <option value={"not_like"}>not similar to</option>
                    <option value={"neq"}>not equal</option>
                    {/* should we include these? they seem confusing */}
                    <option value={"in"}>in</option>
                    {/*<option value={"not_in"}>not in</option>*/}

                </FormControl>
                {/* value input box */}
                <FormControl placeholder="search" name="val"/>
                {/* remove button */}
                <InputGroup.Append>
                    <Button variant="light"
                            onClick={(e) => this.removeFilter(e, index)}>X</Button> {/*remove instead?*/}
                </InputGroup.Append>
            </InputGroup>
        );
    }

    orderBy() {
        return (
            <InputGroup onChange={this.updateOrderBy}>
                <InputGroup.Prepend>
                    <InputGroup.Text>Order By</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl as="select" name="name" default={this.props.fields[0]}>
                    {this.props.fields.map((field) => {
                        return (<option>{field}</option>);
                    })}
                </FormControl>
                <InputGroup.Append>
                    <FormControl as="select" default={""} name="order">
                        <option value={""}>Asc</option>
                        <option value={"-"}>Desc</option>
                    </FormControl>
                </InputGroup.Append>
            </InputGroup>
        );
    }

    handleSubmit(event) {
        event.preventDefault();
        let args = event.target[0].value;
        args = args.split(" ");
        let filters = [];
        let vals = [...this.state.val_list];

        for (let a in args) {
            filters.push({name: "name", op: "ilike", val: `%${args[a]}%`});
            vals.push(args[a]);
        }
        // let filter = {name: "name", op: "ilike", val: `%${event.target[0].value}%`}; /* should we use %val%? */

        if (this.state.advanced && !this.props.simple) {
            if (event.target[0].value === "") {
                filters = [{and: [...this.state.filter_list]}];
            } else {
                filters = [{and: [...this.state.filter_list, ...filters]}]; /* for now assume 'and' everything */
            }
        }

        filters = JSON.stringify(filters);

        if (this.props.simple) {
            this.props.filterAction(event.target[0].value)
        } else {
            // removed [] from around filters
            this.props.filterAction(`${filters}`, `${this.state.order + this.state.group}`, vals);
        }
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <InputGroup>
                    <Form.Control
                        placeholder="Search Foodcravings"
                        aria-label="search"
                        aria-describedby="search"
                    />
                    <InputGroup.Append>
                        <button type="submit">Search</button>
                    </InputGroup.Append>
                </InputGroup>
                {
                    this.props.simple ? "" : (
                        <div>
                            <Button title="Advanced" variant="dark" onClick={this.toggleAdvanced}>Advanced</Button>
                            {!this.state.advanced ? "" : (
                                <div>
                                    {this.orderBy()}
                                    {this.state.filter_list.map((_, i) => {
                                        return this.filterInput(i)
                                    })}
                                    <Button variant="success" onClick={this.addFilter}>Add
                                        Filter</Button> {/*make better*/}
                                </div>
                            )}
                        </div>
                    )
                }
            </form>
        );
    }

}