import React, {Component} from 'react';
import {Nav, Navbar, NavDropdown} from "react-bootstrap";
import Search from './search';
import './header.css';

class Header extends Component {
    constructor(props) {
        super(props);
        this.handleSearch = this.handleSearch.bind(this);
    }


    handleSearch(filters) {
        if (filters.length > 0) {
            this.props.history.push('/search/' + filters);
            window.location.reload();

        } else {
            alert("Please enter some search text!");
            this.setState({navigate: false});
            console.log(this.state);

        }
    }

    render() {
        return (
            <Navbar style={{backgroundColor: '#e8411b'}}>
                <Navbar.Brand href={"/"}>
                    <img
                        src={require("../Assets/foodlogo.svg")}
                        width="152"
                        height="35"
                        className="d-inline-block align-top"
                        alt="React Bootstrap logo"
                    />
                </Navbar.Brand>
                <Nav>
                    <Nav.Link href={"/restaurant"}>Restaurants</Nav.Link>
                    <Nav.Link href={"/recipes"}>Recipes</Nav.Link>
                    <Nav.Link href={"/groceries"}>Groceries</Nav.Link>
                    <NavDropdown title="Vizualizations" id="basic-nav-dropdown">
                        <NavDropdown.Item href="/cuisinecost">Average Cuisine Prices</NavDropdown.Item>
                        <NavDropdown.Item href="/menunutviz">Menu Item Nutrition</NavDropdown.Item>
                        <NavDropdown.Item href="/grocerynetwork">Groceries and Recipes Graph</NavDropdown.Item>
                        <NavDropdown.Divider/>
                        <NavDropdown.Item href="/cityrate">City Scores across the World</NavDropdown.Item>
                        <NavDropdown.Item href="/cityscore">Top City Scores</NavDropdown.Item>
                        <NavDropdown.Item href="/routespiechart">Transit Route Accessibility</NavDropdown.Item>
                    </NavDropdown>
                    <Nav.Link href={"/about"}>About</Nav.Link>
                </Nav>
                <div style={{marginLeft: 'auto'}}>
                    <Search simple={true} filterAction={this.handleSearch}/>
                </div>
            </Navbar>
        );
    }
}

export default Header;
