import React, {Component} from 'react';
import H from 'react-highlight-words';

export default class Highlighter extends Component {
    render() {
        let words = [];
        for (let i = 0; i < this.props.search.length; i++) {
            let filter = this.props.search[i];
            if (filter !== null && filter !== undefined) {
                words = words.concat(filter.toLowerCase().split(" "));
            }
        }

        const highlightStyle = {
            "padding": "0",
            "background-color": "#a4e5a4"
        };

        return (
            <H searchWords={words} highlightStyle={highlightStyle} autoEscape={true} textToHighlight={this.props.text}
               caseSensitive={false}/>
        );
    }
}
