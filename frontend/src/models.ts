// #gitlab
export interface Gitlab_commits {
    id: string;
    short_id: string;
    created_at: string;
    title: string;
    message: string;
    author_name: string;
    author_email: string;
    authored_date: string;
    committer_name: string;
    committer_email: string;
    committed_date: string;
    parent_ids: string[];
}

export interface Links {
    self: string;
    notes: string;
    award_emoji: string;
    project: string;
}

export interface TaskCompletionStatus {
    count: Number;
    completed_count: Number;
}

export interface TimeStats {
    time_estimate: Number;
    total_time_spent: Number;
    human_time_estimate: string | null;
    human_total_time_spent: string | null;
}

export interface Author {
    id: Number;
    name: string;
    username: string;
    state: string;
    avatar_url: string;
    web_url: string;
}

export interface Milestone {
    id: Number;
    iid: Number;
    project_id: Number;
    title: string;
    description: string;
    state: string;
    created_at: string;
    updated_at: string;
    due_date: string;
    start_date: string | null;
    web_url: string;
}

export interface Gitlab_issues {
    id: Number;
    iid: Number;
    project_id: Number;
    title: string;
    description: string | null;
    state: string;
    created_at: string;
    updated_at: string;
    closed_at: string | null;
    closed_by: Author | null;
    assignee: Author | null;
    user_notes_count: Number;
    merge_requests_count: Number;
    upvotes: Number;
    downvotes: Number;
    due_date: string | null;
    confidential: Boolean;
    discussion_locked: string | null;
    web_url: string;
    has_tasks: Boolean;
    weight: string | null;
    _links: Links;
    task_completion_status: TaskCompletionStatus;
    time_stats: TimeStats;
    author: Author;
    assignees: Author[];
    milestone: Milestone | null;
    labels: string[];
}

// # Recipes from Edamam
export interface RecipeItem {
    recipe: Recipe;
    bookmarked: boolean;
    bought: boolean;
    related_rest: { [key: string]: string };
}

export interface Recipe {
    uri: string;
    label: string;
    image: string;
    source: string;
    url: string;
    shareAs: string;
    yield: number;
    dietLabels: string[];
    healthLabels: string[];
    cautions: string[];
    ingredientLines: string[];
    ingredients: Ingredient[];
    calories: number;
    totalWeight: number;
    totalNutrients: { [key: string]: Total };
    totalDaily: { [key: string]: Total };
    digest: Digest[];
}

export interface Digest {
    label: string;
    tag: string;
    schemaOrgTag: null | string;
    total: number;
    hasRDI: boolean;
    daily: number;
    unit: string;
    sub?: Digest[];
}

export interface Ingredient {
    text: string;
    quantity: number;
    measure?: string | null;
    food: string;
    weight: number;
    grocery_link: string | null;
}

export interface Total {
    label: string;
    quantity: number;
    unit: string;
}

// GroceryItem from Walmart/USDA
export interface GroceryItem {
    item: Item;
    nutrients: Nutrients;
}

export interface Item {
    product: Product;
}

export interface Product {
    image: string;
    midasContext: MidasContext;
    related_recipes: { [key: string]: string };
    used_in_restaurant: { [key: string]: string };
}

export interface MidasContext {
    pageType: string;
    subType: string;
    productId: string;
    itemId: string;
    price: number;
    preorder: boolean;
    online: boolean;
    freeShipping: boolean;
    inStore: boolean;
    query: string;
    brand: string;
    categoryPathId: string;
    categoryPathName: string;
    manufacturer: string;
    verticalId: string;
    verticalTheme: string;
    smode: number;
    isTwoDayDeliveryTextEnabled: boolean;
    zip: string;
}

export interface Nutrients {
    foodClass: string;
    description: string;
    foodNutrients: FoodNutrient[];
    foodComponents: any[];
    foodAttributes: any[];
    tableAliasName: string;
    brandOwner: string;
    gtinUpc: string;
    dataSource: string;
    ingredients: string;
    modifiedDate: string;
    availableDate: string;
    servingSize: number;
    servingSizeUnit: string;
    householdServingFullText: string;
    labelNutrients: { [key: string]: LabelNutrient };
    brandedFoodCategory: string;
    fdcId: number;
    dataType: string;
    publicationDate: string;
    foodPortions: any[];
}

export interface FoodNutrient {
    type: string;
    id: number;
    nutrient: Nutrient;
    foodNutrientDerivation: FoodNutrientDerivationClass;
    amount: number;
}

export interface FoodNutrientDerivationClass {
    id: number;
    code: string;
    description: string;
    foodNutrientSource?: FoodNutrientDerivationClass;
}

export interface Nutrient {
    id: number;
    number: string;
    name: string;
    rank: number;
    unitName: string;
}

export interface LabelNutrient {
    value: number;
}

// Resteraunt Menu from xyzmenu.api
export interface RestaurantMenu {
    result: Result;
}

export interface Result {
    image: string;
    totalResults: number;
    data: Datum[];
    numResults: number;
    page: number;
    pages: number;
    morePages: boolean;
}

export interface Datum {
    address: Address;
    cuisines: string[];
    geo: Geo;
    item_id: number;
    menu_item_description: string;
    menu_item_name: string;
    menu_item_pricing: MenuItemPricing[];
    price_range: string;
    restaurant_hours: string;
    restaurant_id: number;
    restaurant_name: string;
    restaurant_phone: string;
    subsection: string;
    subsection_description: string;
    related_recipes: { [key: string]: string };
    related_grocery: { [key: string]: string };
}

export interface Address {
    city: string;
    formatted: string;
    postal_code: string;
    state: string;
    street: string;
}

export interface Geo {
    lat: number;
    lon: number;
}

export interface MenuItemPricing {
    currency: string;
    price: number;
    priceString: string;
}

export interface IssueCount {
    Alexistower: number;
    arkd: number;
    kevingao232: number;
    rguan: number;
    ribsthakkar: number;
}

export interface APIResponse {
    attributes: Object;
    id: number;
    links: APILinks;
    relationships: Object;
    type: string
}

export interface APILinks {
    self: string
}
