import React from 'react';
import ReactDOM from 'react-dom';
import About from "./about";
import {create} from "react-test-renderer";
import {gitComm, gitIss} from "./mockData";
import axios from "axios";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("about component", () => {
    const return_data = {data: true, status: 200};
    mockedAxios.get.mockImplementation(() => {
        return new Promise((resolve, reject) => {
            setTimeout(() =>
                resolve(return_data), 300);
        });
    });
    const about = create(<About/>);
    const inst = about.getInstance();

    test("gitlab calls mocked", async () => {
        inst.gitlabGetIssues().then((result: any) => {
            expect(result).toEqual(return_data);
        });
        inst.gitlabGetCommits().then((result: any) => {
            expect(result).toEqual(return_data);
        });
    });
    test("componentDidMount calls gitlab api", () => {
        mockedAxios.get.mockClear();
        inst.componentDidMount();
        expect(mockedAxios.get).toHaveBeenCalledTimes(2);
    });
    test("renders without crashing", () => {
        const div = document.createElement('div');
        ReactDOM.render(<About/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
    test("getIssueStats returns a count", () => {
        inst.state.issues = gitIss;
        expect(inst.getIssueStats()).toEqual([49,
            {Alexistower: 17, arkd: 13, kevingao232: 13, rguan: 14, ribsthakkar: 12}]);
    });
    test("getCommitStats returns a count", () => {
        inst.state.commits = gitComm;
        expect(inst.getCommitStats()).toEqual([70,
            {
                "aaronmdewitt@gmail.com": 13, "alexis.e.torres@utexas.edu": 18, "arkdengal@gmail.com": 8,
                "guan.robert@utexas.edu": 13, "kevin.gao232@gmail.com": 6, "rishabh.thakkar@gmail.com": 12,
            }]);
    });
    // test("Matches the snapshot", () => {
    //     const about = create(<About />);
    //     expect(about.toJSON()).toMatchSnapshot();
    // }); // use snapshots for mostly static or rarely changing components
});
