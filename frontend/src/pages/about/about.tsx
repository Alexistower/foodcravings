import React, {Component} from 'react';
import DocumentTitle from 'react-document-title'
import {Card, CardDeck, Col, ListGroup, ListGroupItem, Row, Table} from 'react-bootstrap';
import axios from 'axios';
import {Author, Gitlab_commits, Gitlab_issues, IssueCount} from "../../models";

interface Props {

}

interface State {
    issues: Gitlab_issues[];
    commits: Gitlab_commits[];
}

class About extends Component <Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            issues: [] as Gitlab_issues[],
            commits: [] as Gitlab_commits[]
        };

    }

    getIssueStats(): any {
        var issCount = 0;
        var counts: IssueCount = {kevingao232: 0, arkd: 0, ribsthakkar: 0, rguan: 0, Alexistower: 0};

        this.state.issues.forEach((i: Gitlab_issues) => {
            if (i.assignee) {
                ++issCount;
            }
            i.assignees.forEach((a: Author) => {
                counts[a["username"]] += 1;
            });
        });
        return [issCount, counts];
    }

    getCommitStats(): any {
        var commCount = 0;
        var counts: any = {
            "kevin.gao232@gmail.com": 0,
            "arkdengal@gmail.com": 0,
            "aaronmdewitt@gmail.com": 0,
            "rishabh.thakkar@gmail.com": 0,
            "guan.robert@utexas.edu": 0,
            "alexis.e.torres@utexas.edu": 0
        };

        this.state.commits.forEach((i: Gitlab_commits) => {
            ++commCount;
            counts[i["author_email"]] += 1;
        });
        return [commCount, counts];

    }

    gitlabGetIssues(page: number = 1, per_page: number = 200): Promise<Gitlab_issues> {
        return axios.get(`https://gitlab.com/api/v4/projects/14484432/issues`,
            {params: {per_page: per_page, page: page}})
    }

    gitlabGetCommits(page: number = 1, per_page: number = 200): Promise<Gitlab_commits> {
        return axios.get(`https://gitlab.com/api/v4/projects/14484432/repository/commits`,
            {params: {per_page: per_page, page: page}})
    }

    componentDidMount() {
        // Can't guarantee that this will immediately update state
        this.gitlabGetIssues().then((result: any) =>
            this.setState({
                    issues: result.data
                } as State
            ));
        this.gitlabGetCommits().then((result: any) =>
            this.setState({
                    commits: result.data
                } as State
            ));
    }

    render() {
        var stats = this.getIssueStats();
        var commitStats = this.getCommitStats();
        return (
            <DocumentTitle title="FoodCravings: About">
                <div className='container'>
                    <h2> About Us</h2>
                    <p style={{width: "35em", margin: "auto"}}>Our mission is to give you options when you are craving
                        some food, whether it be a snack, or a full meal.
                        We believe that you should be able to decide not only what you eat, but also where you eat,
                        depending
                        on factors like price range or specific ingredients.</p>

                    <h2>The Team</h2>
                    <CardDeck style={{marginTop: 20, justifyContent: "center"}}>
                        <Row style={{justifyContent: "center"}}>
                            <Col sm={6} lg={3}>
                                <Card style={{height: "400px"}}>
                                    <Card.Img variant="top"
                                              src={"https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png"}/>
                                    <Card.Body>
                                        <Card.Title>Kevin Gao</Card.Title>
                                        <Card.Text>Hit me baby one more time</Card.Text>
                                    </Card.Body>
                                    <ListGroup>
                                        <ListGroupItem>Responsibilites: Frontend</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col sm={6} lg={3}>
                                <Card style={{height: "400px"}}>
                                    <Card.Img variant="top"
                                              src={"https://assets.gitlab-static.net/uploads/-/system/user/avatar/4526777/avatar.png"}/>
                                    <Card.Body>
                                        <Card.Title>Aaron Dewitt</Card.Title>
                                        <Card.Text>To be or not to be</Card.Text>
                                    </Card.Body>
                                    <ListGroup>
                                        <ListGroupItem>Responsibilites: Backend, Frontend</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col sm={6} lg={3}>
                                <Card style={{height: "400px"}}>
                                    <Card.Img variant="top"
                                              src={"https://assets.gitlab-static.net/uploads/-/system/user/avatar/4564993/avatar.png"}/>
                                    <Card.Body>
                                        <Card.Title>Rishabh Thakkar</Card.Title>
                                        <Card.Text> I love F1</Card.Text>
                                    </Card.Body>
                                    <ListGroup>
                                        <ListGroupItem>Responsibilites: Frontend, Backend</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                        </Row>
                    </CardDeck>
                    <CardDeck style={{marginTop: 20, justifyContent: "center"}}>
                        <Row style={{justifyContent: "center"}}>
                            <Col sm={6} lg={5}>
                                <Card style={{height: "400px"}}>
                                    <Card.Img variant="top"
                                              src={"https://assets.gitlab-static.net/uploads/-/system/user/avatar/3468811/avatar.png"}/>
                                    <Card.Body>
                                        <Card.Title>Robert Guan</Card.Title>
                                        <Card.Text> CS Junior</Card.Text>
                                    </Card.Body>
                                    <ListGroup>
                                        <ListGroupItem>Responsibilites: Frontend</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>
                            <Col sm={6} lg={5}>
                                <Card style={{height: "400px"}}>
                                    <Card.Img variant="top"
                                              src={"https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png"}
                                              width="200px" height="200px"/>
                                    <Card.Body>
                                        <Card.Title>Alexis Torres</Card.Title>
                                        <Card.Text>Senior just trying to get by</Card.Text>
                                    </Card.Body>
                                    <ListGroup>
                                        <ListGroupItem>Responsibilites: AWS, Frontend</ListGroupItem>
                                    </ListGroup>
                                </Card>
                            </Col>

                        </Row>
                    </CardDeck>
                    <br></br>
                    <h2> Gitlab Stats </h2>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Commits</th>
                            <th>Issues</th>
                            <th>Unit Tests</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Kevin Gao</td>
                            <td>{commitStats["1"]["kevin.gao232@gmail.com"]} </td>
                            <td>{stats["1"]["kevingao232"]}</td>
                            <td>1</td>
                        </tr>
                        <tr>
                            <td>Aaron Dewitt</td>
                            <td>{commitStats["1"]["arkdengal@gmail.com"] + commitStats["1"]["aaronmdewitt@gmail.com"]}</td>
                            <td>{stats["1"]["arkd"]}</td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td>Rishabh Thakkar</td>
                            <td>{commitStats["1"]["rishabh.thakkar@gmail.com"]}</td>
                            <td>{stats["1"]["ribsthakkar"]}</td>
                            <td>16</td>
                        </tr>
                        <tr>
                            <td>Robert Guan</td>
                            <td>{commitStats["1"]["guan.robert@utexas.edu"]}</td>
                            <td>{stats["1"]["rguan"]}</td>
                            <td>14</td>
                        </tr>
                        <tr>
                            <td>Alexis Torres</td>
                            <td>{commitStats["1"]["alexis.e.torres@utexas.edu"]}</td>
                            <td>{stats["1"]["Alexistower"]}</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td>Totals</td>
                            <td>{commitStats["0"]}</td>
                            <td>{stats["0"]}</td>
                            <td>41</td>
                        </tr>

                        </tbody>
                    </Table>
                    <br></br>

                    <h2>Data Sources</h2>
                    <Table striped bordered hover>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><a href="https://www.edamam.com">Edamam</a></td>
                            <td>Information on recipes</td>
                        </tr>
                        <tr>
                            <td><a href="https://www.walmart.com">Walmart</a></td>
                            <td>Information on grocery items like price and brand options</td>
                        </tr>
                        <tr>
                            <td><a href="https://fdc.nal.usda.gov/">U.S. Department of Agriculture</a></td>
                            <td>Nutrition data on grocery items</td>
                        </tr>
                        <tr>
                            <td><a href="https://spoonacular.com/food-api">Spoonacular</a></td>
                            <td>Matching menu items to similar recipes</td>
                        </tr>
                        <tr>
                            <td><a href="https://rapidapi.com/restaurantmenus/api/us-restaurant-menus">MenuAppXYZ </a>
                            </td>
                            <td>Finding restaurants in a local area and getting menu information</td>
                        </tr>
                        </tbody>
                    </Table>
                    <br></br>
                    <h2>Tools</h2>
                    <p><a href="https://aws.amazon.com">AWS:</a> Web hosting</p>
                    <p><a href="https://sass-lang.com">Sass:</a> CSS extension</p>
                    <p><a href="https://gitlab.com">Gitlab:</a> Version control</p>
                    <br></br>
                    <h2>Links</h2>
                    <p><a href="https://gitlab.com/Alexistower/foodcravings">Gitlab repository</a></p>
                    <p><a href="https://documenter.getpostman.com/view/8953925/SVtbP5HS?version=latest">Postman API</a>
                    </p>
                </div>
            </DocumentTitle>
        );
    }
}

export default About;
