import {Gitlab_commits, Gitlab_issues} from "../../models";

export const gitIss: Gitlab_issues[] = [
    {
        "id": 25828762,
        "iid": 73,
        "project_id": 14484432,
        "title": "User Story 10 - Price Range",
        "description": "As a user, I would like the \"Price Range\" attribute in the restaurants model to be more specific. Somewhere on this page, could you clarify what each dollar sign means? Ex: typical price for a single meal for \"$\" would be <$10, \"$$\" would be $10-20, \"$$$\" would be $20-30, etc etc. You can make the benchmarks whatever you like, but I feel like making this information available to the user somewhere on the page would be useful in allowing them to determine where to eat.",
        "state": "opened",
        "created_at": "2019-10-10T22:25:30.144Z",
        "updated_at": "2019-10-10T22:25:30.144Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/73",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/73",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/73/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/73/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25828730,
        "iid": 72,
        "project_id": 14484432,
        "title": "User Story 9 - Splash Page Pictures",
        "description": "As a user, I feel as though the transition between the pictures of food is a bit too long. It seems to take about 5 seconds, but I think 3 or 4 seconds would keep the website from feeling sluggish. If this change is implemented and you do not like the faster transition time, we can discuss it and potentially reverse it.",
        "state": "opened",
        "created_at": "2019-10-10T22:22:12.062Z",
        "updated_at": "2019-10-10T22:22:12.062Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/72",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/72",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/72/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/72/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25828569,
        "iid": 71,
        "project_id": 14484432,
        "title": "User Story 8 - More instances",
        "description": "As a user, I would like to see each model page have a good deal of information. Please have at least 10 instances for each model. If for some reason you feel as though a model does not need 10 or more instances, we can discuss it and change the benchmark.",
        "state": "opened",
        "created_at": "2019-10-10T22:16:31.053Z",
        "updated_at": "2019-10-10T22:16:31.053Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/71",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/71",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/71/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/71/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25828540,
        "iid": 70,
        "project_id": 14484432,
        "title": "User Story 7 - Pagination",
        "description": "As a user, I would like to have pagination implemented for each model's page. Once there are enough instances to implement pagination, please do so. The number of instances per page can range anywhere from 10 - 50, whatever you feel works best for your site.",
        "state": "opened",
        "created_at": "2019-10-10T22:14:27.090Z",
        "updated_at": "2019-10-10T22:14:27.090Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/70",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/70",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/70/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/70/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25828525,
        "iid": 69,
        "project_id": 14484432,
        "title": "User Story 6 - Calorie Rounding",
        "description": "As a user, I would like to have the calorie attribute altered a bit. Could you please round the calorie attribute to the nearest calorie? This is for ease of use and UI clarity.",
        "state": "opened",
        "created_at": "2019-10-10T22:12:18.523Z",
        "updated_at": "2019-10-10T22:12:18.523Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/69",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/69",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/69/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/69/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25786322,
        "iid": 68,
        "project_id": 14484432,
        "title": "react dockerfile serverside?",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-10T00:42:31.752Z",
        "updated_at": "2019-10-10T00:42:31.752Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/68",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/68",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/68/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/68/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25786213,
        "iid": 67,
        "project_id": 14484432,
        "title": "make dockerfile that uses python base image and installs flask",
        "description": "pip install flask\npip install -r requirements.txt\nstart image on port 80",
        "state": "opened",
        "created_at": "2019-10-10T00:27:57.874Z",
        "updated_at": "2019-10-10T00:27:57.874Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            }
        ],
        "author": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/67",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/67",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/67/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/67/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704658,
        "iid": 66,
        "project_id": 14484432,
        "title": "create user story for ethicalemployers.club",
        "description": null,
        "state": "closed",
        "created_at": "2019-10-07T23:53:28.529Z",
        "updated_at": "2019-10-08T23:42:27.682Z",
        "closed_at": "2019-10-08T23:42:27.655Z",
        "closed_by": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "labels": [],
        "milestone": null,
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/66",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/66",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/66/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/66/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704620,
        "iid": 65,
        "project_id": 14484432,
        "title": "clean up frontend directory organization",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:47:43.251Z",
        "updated_at": "2019-10-07T23:47:43.251Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/65",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/65",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/65/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/65/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704605,
        "iid": 64,
        "project_id": 14484432,
        "title": "UML diagram using PlantUML and put it on GitLab as a PNG",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:47:11.015Z",
        "updated_at": "2019-10-07T23:47:11.015Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Docs"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/64",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/64",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/64/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/64/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704584,
        "iid": 63,
        "project_id": 14484432,
        "title": "restful documentation",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:44:41.167Z",
        "updated_at": "2019-10-07T23:44:41.167Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Docs"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/63",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/63",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/63/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/63/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704573,
        "iid": 62,
        "project_id": 14484432,
        "title": "refine technical report",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:44:05.839Z",
        "updated_at": "2019-10-07T23:45:03.119Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Docs"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/62",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/62",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/62/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/62/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704556,
        "iid": 61,
        "project_id": 14484432,
        "title": "acceptance tests frontend",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:42:52.713Z",
        "updated_at": "2019-10-07T23:42:52.713Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/61",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/61",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/61/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/61/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704538,
        "iid": 60,
        "project_id": 14484432,
        "title": "backend pagination",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:40:54.729Z",
        "updated_at": "2019-10-07T23:40:54.729Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/60",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/60",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/60/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/60/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704522,
        "iid": 59,
        "project_id": 14484432,
        "title": "Api Layer flask-restless",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:38:54.416Z",
        "updated_at": "2019-10-07T23:38:54.416Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/59",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/59",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/59/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/59/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704508,
        "iid": 58,
        "project_id": 14484432,
        "title": "Pagination for source tables",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:37:01.197Z",
        "updated_at": "2019-10-07T23:47:07.764Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/58",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/58",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/58/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/58/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704492,
        "iid": 57,
        "project_id": 14484432,
        "title": "connect frontend to backend api",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:34:22.942Z",
        "updated_at": "2019-10-07T23:34:22.942Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/57",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/57",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/57/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/57/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704482,
        "iid": 56,
        "project_id": 14484432,
        "title": "San Francisco (Apple): font-ify the frontend",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:33:51.791Z",
        "updated_at": "2019-10-07T23:33:51.791Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/56",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/56",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/56/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/56/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704443,
        "iid": 55,
        "project_id": 14484432,
        "title": "server side rendering? on aws using docker aka frontend hosted on docker",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:29:29.180Z",
        "updated_at": "2019-10-07T23:29:29.180Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/55",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/55",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/55/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/55/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704427,
        "iid": 54,
        "project_id": 14484432,
        "title": "mounting docker on aws",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:26:41.664Z",
        "updated_at": "2019-10-07T23:26:41.664Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/54",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/54",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/54/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/54/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704301,
        "iid": 53,
        "project_id": 14484432,
        "title": "relational connections between models",
        "description": "food to restaurant to recipes to grocery",
        "state": "opened",
        "created_at": "2019-10-07T23:14:07.072Z",
        "updated_at": "2019-10-07T23:23:38.139Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/53",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/53",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/53/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/53/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704297,
        "iid": 52,
        "project_id": 14484432,
        "title": "scrape grocery data",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:13:35.862Z",
        "updated_at": "2019-10-07T23:13:35.862Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/52",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/52",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/52/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/52/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704282,
        "iid": 51,
        "project_id": 14484432,
        "title": "scrape recipe data",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:11:39.600Z",
        "updated_at": "2019-10-07T23:11:39.600Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/51",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/51",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/51/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/51/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704264,
        "iid": 50,
        "project_id": 14484432,
        "title": "create RDS backend",
        "description": "",
        "state": "closed",
        "created_at": "2019-10-07T23:09:33.199Z",
        "updated_at": "2019-10-09T17:42:33.521Z",
        "closed_at": "2019-10-09T17:42:33.486Z",
        "closed_by": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/50",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/50",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/50/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/50/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704260,
        "iid": 49,
        "project_id": 14484432,
        "title": "Format Table Widths in Instance Pages",
        "description": "headerStyle: (colum, colIndex) => {\n          return { width: '80px', textAlign: 'center' };\n        }\n\n**A note for me, Kevin.**",
        "state": "closed",
        "created_at": "2019-10-07T23:09:18.817Z",
        "updated_at": "2019-10-09T23:35:49.283Z",
        "closed_at": "2019-10-09T23:35:49.255Z",
        "closed_by": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/49",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/49",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/49/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/49/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704241,
        "iid": 48,
        "project_id": 14484432,
        "title": "eager scrape restaurant data",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:07:47.210Z",
        "updated_at": "2019-10-07T23:07:47.210Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/48",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/48",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/48/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/48/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704219,
        "iid": 47,
        "project_id": 14484432,
        "title": "grocery api model definition backend",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:05:25.536Z",
        "updated_at": "2019-10-07T23:05:25.536Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/47",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/47",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/47/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/47/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704204,
        "iid": 46,
        "project_id": 14484432,
        "title": "UI improvements",
        "description": "",
        "state": "closed",
        "created_at": "2019-10-07T23:04:07.512Z",
        "updated_at": "2019-10-10T00:18:39.268Z",
        "closed_at": "2019-10-10T00:18:39.232Z",
        "closed_by": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/46",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/46",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/46/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/46/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704166,
        "iid": 45,
        "project_id": 14484432,
        "title": "restaurant api model definition backend",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T23:00:32.995Z",
        "updated_at": "2019-10-07T23:00:32.995Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/45",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/45",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/45/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/45/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25704155,
        "iid": 44,
        "project_id": 14484432,
        "title": "recipe api model definition",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-07T22:59:17.807Z",
        "updated_at": "2019-10-09T00:55:51.833Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/44",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/44",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/44/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/44/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25703909,
        "iid": 43,
        "project_id": 14484432,
        "title": "Theme issue assessment on ethicalemployers.club",
        "description": "write a comment evaluating the work they did for our customer issue",
        "state": "closed",
        "created_at": "2019-10-07T22:49:13.765Z",
        "updated_at": "2019-10-09T22:26:03.021Z",
        "closed_at": "2019-10-09T22:26:02.995Z",
        "closed_by": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/43",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/43",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/43/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/43/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25703712,
        "iid": 42,
        "project_id": 14484432,
        "title": "Provide 5 issue stories for our Developer",
        "description": "Each group member writes one.",
        "state": "opened",
        "created_at": "2019-10-07T22:35:26.989Z",
        "updated_at": "2019-10-07T22:35:26.989Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/42",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/42",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/42/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/42/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25593536,
        "iid": 41,
        "project_id": 14484432,
        "title": "Create links between instance pages",
        "description": "Need to get relations between instances.",
        "state": "closed",
        "created_at": "2019-10-03T16:21:31.922Z",
        "updated_at": "2019-10-03T19:13:36.917Z",
        "closed_at": "2019-10-03T19:13:36.893Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/41",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/41",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/41/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/41/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25593516,
        "iid": 40,
        "project_id": 14484432,
        "title": "Can't reload the component between model pages",
        "description": "The model component doesn't display new information when we try to look at different model pages",
        "state": "closed",
        "created_at": "2019-10-03T16:20:50.800Z",
        "updated_at": "2019-10-03T16:27:36.199Z",
        "closed_at": "2019-10-03T16:27:36.162Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Bug",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/40",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/40",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/40/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/40/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569535,
        "iid": 39,
        "project_id": 14484432,
        "title": "write tests for phase 1 frontend",
        "description": "",
        "state": "opened",
        "created_at": "2019-10-03T06:06:46.579Z",
        "updated_at": "2019-10-07T23:48:52.460Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            },
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            },
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/39",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/39",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/39/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/39/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569430,
        "iid": 38,
        "project_id": 14484432,
        "title": "technical report pdf pushed to github",
        "description": "",
        "state": "closed",
        "created_at": "2019-10-03T05:59:03.573Z",
        "updated_at": "2019-10-03T19:12:41.054Z",
        "closed_at": "2019-10-03T19:12:41.022Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/38",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/38",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/38/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/38/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569390,
        "iid": 37,
        "project_id": 14484432,
        "title": "CI pipeline requirement",
        "description": "we are required to have a gitlab pipeline",
        "state": "closed",
        "created_at": "2019-10-03T05:54:57.327Z",
        "updated_at": "2019-10-04T00:36:24.627Z",
        "closed_at": "2019-10-04T00:36:24.598Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/37",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/37",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/37/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/37/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569369,
        "iid": 36,
        "project_id": 14484432,
        "title": "ResterauntMenu in restaurant.tsx resolve type definition/ data mismatch",
        "description": "may have been borked by merge",
        "state": "closed",
        "created_at": "2019-10-03T05:53:26.134Z",
        "updated_at": "2019-10-03T19:13:01.260Z",
        "closed_at": "2019-10-03T19:13:01.236Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Bug",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 2,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/36",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/36",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/36/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/36/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569273,
        "iid": 35,
        "project_id": 14484432,
        "title": "model source page not removed upon navigation to instance page. unmount?",
        "description": "",
        "state": "closed",
        "created_at": "2019-10-03T05:48:13.234Z",
        "updated_at": "2019-10-03T19:13:23.333Z",
        "closed_at": "2019-10-03T19:13:23.301Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Bug",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/35",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/35",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/35/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/35/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25569230,
        "iid": 34,
        "project_id": 14484432,
        "title": "react components do not properly update on route changes",
        "description": "changing route does not effect component state",
        "state": "closed",
        "created_at": "2019-10-03T05:45:33.055Z",
        "updated_at": "2019-10-03T19:13:12.386Z",
        "closed_at": "2019-10-03T19:13:12.353Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Bug",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/34",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/34",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/34/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/34/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25524252,
        "iid": 33,
        "project_id": 14484432,
        "title": "User Story 5 - Nav Bar",
        "description": "As a customer, I would like the website to have a color theme other than black and white. This is to make the website more aesthetically pleasing to users. In order to begin this shift in color theme, could the nav bar color be changed from black to something else? Any other color will do - choose one that you feel will best fit with the feel of your final website.",
        "state": "closed",
        "created_at": "2019-10-02T01:01:31.099Z",
        "updated_at": "2019-10-10T21:45:10.892Z",
        "closed_at": "2019-10-03T03:57:30.374Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/33",
        "time_stats": {
            "time_estimate": 7200,
            "total_time_spent": 21600,
            "human_time_estimate": "2h",
            "human_total_time_spent": "6h"
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/33",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/33/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/33/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25524196,
        "iid": 32,
        "project_id": 14484432,
        "title": "User Story 4 - Splash Page",
        "description": "As a customer, I would like the home page to display something other than lyrics. Please change the title of the home page to the title of the project, and the body of the web page to give a brief description of the project. This will help the customer to see what goal of the final version of the website is.",
        "state": "closed",
        "created_at": "2019-10-02T00:54:38.308Z",
        "updated_at": "2019-10-10T21:44:51.137Z",
        "closed_at": "2019-10-03T05:39:01.590Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/32",
        "time_stats": {
            "time_estimate": 7200,
            "total_time_spent": 7200,
            "human_time_estimate": "2h",
            "human_total_time_spent": "2h"
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/32",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/32/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/32/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25524160,
        "iid": 31,
        "project_id": 14484432,
        "title": "User Story 3 - About Page",
        "description": "As a customer, I would like the About Page to have stats dynamically derived from GitLab. This is so I can see how productive the developers I've hired are being. The statistics I would like derived from GitLab are total number of commits, total number of issues, and number of unit tests (probably 0 tests so far, but I would still like it to appear on the page).",
        "state": "closed",
        "created_at": "2019-10-02T00:49:50.104Z",
        "updated_at": "2019-10-10T21:44:31.167Z",
        "closed_at": "2019-10-03T03:56:55.699Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/31",
        "time_stats": {
            "time_estimate": 10800,
            "total_time_spent": 18000,
            "human_time_estimate": "3h",
            "human_total_time_spent": "5h"
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/31",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/31/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/31/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25524119,
        "iid": 30,
        "project_id": 14484432,
        "title": "User Story 2 - Instances",
        "description": "As a user, I would like to see data on three instances for Recipes, Restaurants, and Grocery Items on their respective pages. This is so I could see what kind of instances would be included in each model, and get a preview of the types of information that will be shared in the final website. The data can be displayed in any visual format for this step; I just want it to appear on the page.",
        "state": "closed",
        "created_at": "2019-10-02T00:44:52.607Z",
        "updated_at": "2019-10-10T21:43:51.022Z",
        "closed_at": "2019-10-03T05:40:00.406Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            },
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/30",
        "time_stats": {
            "time_estimate": 36000,
            "total_time_spent": 72000,
            "human_time_estimate": "1d 2h",
            "human_total_time_spent": "2d 4h"
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/30",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/30/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/30/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25524012,
        "iid": 29,
        "project_id": 14484432,
        "title": "User Story 1 - Project Name",
        "description": "As a user, I would like your website to include at least the project/website name in place of \"Logo\" at the top left corner. This is so I can know clearly what website I'm on and the purpose of the project. The font/styling can stay the same, I would just like the content to clearly reflect the website for now.",
        "state": "closed",
        "created_at": "2019-10-02T00:39:19.488Z",
        "updated_at": "2019-10-10T21:42:52.387Z",
        "closed_at": "2019-10-03T05:40:32.579Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Customer"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4532084,
            "name": "Ella Robertson",
            "username": "ella.robertson",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4532084/avatar.png",
            "web_url": "https://gitlab.com/ella.robertson"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/29",
        "time_stats": {
            "time_estimate": 14400,
            "total_time_spent": 18000,
            "human_time_estimate": "4h",
            "human_total_time_spent": "5h"
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/29",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/29/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/29/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25478311,
        "iid": 28,
        "project_id": 14484432,
        "title": "create source table for page for our three models",
        "description": "create a table with three instance page links and basic data about each of those instances",
        "state": "closed",
        "created_at": "2019-09-30T23:21:10.412Z",
        "updated_at": "2019-10-03T05:41:20.762Z",
        "closed_at": "2019-10-03T05:41:20.739Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/28",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/28",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/28/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/28/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25477760,
        "iid": 27,
        "project_id": 14484432,
        "title": "give our customer team access to our gitlab to make issues",
        "description": "",
        "state": "closed",
        "created_at": "2019-09-30T22:25:32.059Z",
        "updated_at": "2019-10-02T03:16:45.676Z",
        "closed_at": "2019-10-02T03:16:45.653Z",
        "closed_by": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "labels": [
            "Customer",
            "Doing"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/27",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/27",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/27/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/27/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25477580,
        "iid": 26,
        "project_id": 14484432,
        "title": "ask our developer team for customer access to make issues",
        "description": "",
        "state": "closed",
        "created_at": "2019-09-30T22:21:01.359Z",
        "updated_at": "2019-10-03T05:41:37.659Z",
        "closed_at": "2019-10-03T05:41:37.636Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Customer",
            "Doing"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/26",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/26",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/26/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/26/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25358365,
        "iid": 25,
        "project_id": 14484432,
        "title": "create relational mapping from recipes to grocery items",
        "description": "find prices on recipe ingredients from grocery stores (nearby?)",
        "state": "opened",
        "created_at": "2019-09-26T20:30:14.462Z",
        "updated_at": "2019-10-03T18:33:13.257Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/25",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/25",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/25/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/25/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25358331,
        "iid": 24,
        "project_id": 14484432,
        "title": "create relational mapping from recipes to restaurants",
        "description": "find ratings on recipes that are served by restaurants (nearby?)",
        "state": "opened",
        "created_at": "2019-09-26T20:28:34.821Z",
        "updated_at": "2019-10-03T18:33:13.219Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/24",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/24",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/24/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/24/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25358273,
        "iid": 23,
        "project_id": 14484432,
        "title": "everyone join postman",
        "description": "",
        "state": "closed",
        "created_at": "2019-09-26T20:25:23.148Z",
        "updated_at": "2019-10-01T20:02:09.602Z",
        "closed_at": "2019-10-01T20:02:09.574Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            },
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            },
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            },
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 2,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/23",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/23",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/23/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/23/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25358060,
        "iid": 22,
        "project_id": 14484432,
        "title": "get foodcravings.com from aws route 53",
        "description": "I (arkd) don't mind paying for this",
        "state": "closed",
        "created_at": "2019-09-26T20:20:52.894Z",
        "updated_at": "2019-10-01T22:19:17.662Z",
        "closed_at": "2019-09-27T21:09:31.817Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Doing",
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-28",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/22",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/22",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/22/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/22/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357855,
        "iid": 21,
        "project_id": 14484432,
        "title": "add gitlab to slack",
        "description": "alexis must do this since he owns the git",
        "state": "closed",
        "created_at": "2019-09-26T20:17:22.067Z",
        "updated_at": "2019-09-26T23:43:01.627Z",
        "closed_at": "2019-09-26T23:43:01.600Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/21",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/21",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/21/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/21/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357789,
        "iid": 20,
        "project_id": 14484432,
        "title": "create cloudfront distribution for our s3 bucket to support https",
        "description": "",
        "state": "closed",
        "created_at": "2019-09-26T20:15:27.287Z",
        "updated_at": "2019-10-01T22:19:43.058Z",
        "closed_at": "2019-09-27T20:38:25.314Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-30",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/20",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/20",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/20/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/20/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357764,
        "iid": 19,
        "project_id": 14484432,
        "title": "create a s3 container for our website & enable website hosting",
        "description": "http stage",
        "state": "closed",
        "created_at": "2019-09-26T20:13:47.865Z",
        "updated_at": "2019-10-01T22:20:01.596Z",
        "closed_at": "2019-09-27T20:38:35.093Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-28",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/19",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/19",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/19/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/19/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357606,
        "iid": 18,
        "project_id": 14484432,
        "title": "create AWS account and give access IAM access to team",
        "description": "",
        "state": "closed",
        "created_at": "2019-09-26T20:11:41.521Z",
        "updated_at": "2019-09-27T20:37:58.130Z",
        "closed_at": "2019-09-27T20:37:58.074Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-27",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/18",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/18",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/18/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/18/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357489,
        "iid": 17,
        "project_id": 14484432,
        "title": "grocery model",
        "description": "use scraped data to create a model for frontend and backend json2ts (frontend) then use the ts model and fill in the extra info for making flask models.",
        "state": "closed",
        "created_at": "2019-09-26T20:06:56.424Z",
        "updated_at": "2019-10-03T03:58:27.269Z",
        "closed_at": "2019-10-03T03:58:27.246Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Backend",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-28",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/17",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/17",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/17/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/17/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357477,
        "iid": 16,
        "project_id": 14484432,
        "title": "restraurant model",
        "description": "use scraped data to create a model for frontend and backend using json2ts (frontend) then use the ts model and fill in the extra info for making flask models.",
        "state": "closed",
        "created_at": "2019-09-26T20:06:18.952Z",
        "updated_at": "2019-10-03T03:58:22.915Z",
        "closed_at": "2019-10-03T03:58:22.890Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Backend",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-28",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/16",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/16",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/16/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/16/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357457,
        "iid": 15,
        "project_id": 14484432,
        "title": "recipe model",
        "description": "use scraped data to create a model for frontend and backend\njson2ts (frontend)\nthen use the ts model and fill in the extra info for making flask models.",
        "state": "closed",
        "created_at": "2019-09-26T20:05:21.537Z",
        "updated_at": "2019-10-03T03:58:13.839Z",
        "closed_at": "2019-10-03T03:58:13.805Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Backend",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-09-28",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/15",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/15",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/15/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/15/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357156,
        "iid": 14,
        "project_id": 14484432,
        "title": "recipe api",
        "description": "use flask and aws to serve recipe data scraped and stored on rds",
        "state": "closed",
        "created_at": "2019-09-26T19:53:45.018Z",
        "updated_at": "2019-10-03T05:41:57.718Z",
        "closed_at": "2019-10-03T05:41:57.695Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-02",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/14",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/14",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/14/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/14/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357139,
        "iid": 13,
        "project_id": 14484432,
        "title": "restaurant api",
        "description": "use flask to service data scraped from rds",
        "state": "closed",
        "created_at": "2019-09-26T19:52:42.497Z",
        "updated_at": "2019-10-03T05:42:09.335Z",
        "closed_at": "2019-10-03T05:42:09.307Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-02",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/13",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/13",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/13/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/13/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25357117,
        "iid": 12,
        "project_id": 14484432,
        "title": "grocery api",
        "description": "use flask and aws to serve grocery data scraped and stored on rds",
        "state": "closed",
        "created_at": "2019-09-26T19:51:15.741Z",
        "updated_at": "2019-10-03T05:42:17.387Z",
        "closed_at": "2019-10-03T05:42:17.358Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            }
        ],
        "author": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "assignee": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-01",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/12",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/12",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/12/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/12/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305529,
        "iid": 11,
        "project_id": 14484432,
        "title": "Integrate RESTFul API into Website",
        "description": null,
        "state": "opened",
        "created_at": "2019-09-25T16:12:07.410Z",
        "updated_at": "2019-10-08T00:41:16.618Z",
        "closed_at": null,
        "closed_by": null,
        "labels": [
            "Frontend"
        ],
        "milestone": {
            "id": 1014884,
            "iid": 2,
            "project_id": 14484432,
            "title": "phase 2",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:47:44.948Z",
            "updated_at": "2019-09-26T19:48:29.172Z",
            "due_date": "2019-10-22",
            "start_date": "2019-10-03",
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/2"
        },
        "assignees": [],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": null,
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/11",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/11",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/11/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/11/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305459,
        "iid": 10,
        "project_id": 14484432,
        "title": "Integrate Bootstrap CSS",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:09:04.563Z",
        "updated_at": "2019-10-01T22:23:03.442Z",
        "closed_at": "2019-09-27T20:42:37.323Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [],
        "milestone": null,
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            },
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4526777,
                "name": "Aaron Dewitt",
                "username": "arkd",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
                "web_url": "https://gitlab.com/arkd"
            },
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            },
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": null,
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/10",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/10",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/10/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/10/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305435,
        "iid": 9,
        "project_id": 14484432,
        "title": "Publish website on AWS",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:07:35.141Z",
        "updated_at": "2019-10-01T22:20:36.741Z",
        "closed_at": "2019-09-29T00:43:50.573Z",
        "closed_by": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "labels": [
            "Ops"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/9",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/9",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/9/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/9/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305426,
        "iid": 8,
        "project_id": 14484432,
        "title": "Implement Grocery Model Page",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:07:12.770Z",
        "updated_at": "2019-10-03T03:58:55.522Z",
        "closed_at": "2019-10-03T03:58:55.500Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/8",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/8",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/8/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/8/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305415,
        "iid": 7,
        "project_id": 14484432,
        "title": "Implement Restaurant model page",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:06:25.393Z",
        "updated_at": "2019-10-03T05:42:49.391Z",
        "closed_at": "2019-10-03T05:42:49.366Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 3468811,
                "name": "Robert Guan",
                "username": "rguan",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
                "web_url": "https://gitlab.com/rguan"
            },
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 3468811,
            "name": "Robert Guan",
            "username": "rguan",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f9bd2e4c1fbefa8bdd3e901fbd82403b?s=80&d=identicon",
            "web_url": "https://gitlab.com/rguan"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/7",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/7",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/7/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/7/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305410,
        "iid": 6,
        "project_id": 14484432,
        "title": "Implement Recipe Model Page",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:06:02.824Z",
        "updated_at": "2019-10-03T03:58:47.937Z",
        "closed_at": "2019-10-03T03:58:47.914Z",
        "closed_by": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "labels": [
            "Frontend"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 4556793,
                "name": "Kevin Gao",
                "username": "kevingao232",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
                "web_url": "https://gitlab.com/kevingao232"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4556793,
            "name": "Kevin Gao",
            "username": "kevingao232",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/4556793/avatar.png",
            "web_url": "https://gitlab.com/kevingao232"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/6",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/6",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/6/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/6/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305377,
        "iid": 5,
        "project_id": 14484432,
        "title": "Create basic framework for Splash Page",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:04:54.482Z",
        "updated_at": "2019-10-03T05:42:26.585Z",
        "closed_at": "2019-10-03T05:42:26.564Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend",
            "Frontend"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/5",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/5",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/5/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/5/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305369,
        "iid": 4,
        "project_id": 14484432,
        "title": "Scrape Recipe Data",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:04:30.764Z",
        "updated_at": "2019-10-01T23:30:47.666Z",
        "closed_at": "2019-10-01T23:30:47.636Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/4",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/4",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/4/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/4/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305362,
        "iid": 3,
        "project_id": 14484432,
        "title": "Scrape Grocery Data",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:04:20.787Z",
        "updated_at": "2019-10-01T23:30:41.891Z",
        "closed_at": "2019-10-01T23:30:41.865Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/3",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/3",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/3/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/3/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305357,
        "iid": 2,
        "project_id": 14484432,
        "title": "Scrape Restaurant Data",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:04:05.279Z",
        "updated_at": "2019-10-02T00:44:52.783Z",
        "closed_at": "2019-10-01T23:30:32.581Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend",
            "Ops"
        ],
        "milestone": null,
        "assignees": [
            {
                "id": 4564993,
                "name": "Rishabh Thakkar",
                "username": "ribsthakkar",
                "state": "active",
                "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
                "web_url": "https://gitlab.com/ribsthakkar"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "user_notes_count": 1,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/2",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/2",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/2/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/2/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    },
    {
        "id": 25305339,
        "iid": 1,
        "project_id": 14484432,
        "title": "Implement About Page",
        "description": null,
        "state": "closed",
        "created_at": "2019-09-25T16:03:31.392Z",
        "updated_at": "2019-10-03T05:42:32.887Z",
        "closed_at": "2019-10-03T05:42:32.858Z",
        "closed_by": {
            "id": 4526777,
            "name": "Aaron Dewitt",
            "username": "arkd",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/7759f8e57b768f00a24d24d4737b9248?s=80&d=identicon",
            "web_url": "https://gitlab.com/arkd"
        },
        "labels": [
            "Backend",
            "Frontend"
        ],
        "milestone": {
            "id": 1014883,
            "iid": 1,
            "project_id": 14484432,
            "title": "phase 1",
            "description": "80 pts, 8% of total grade",
            "state": "active",
            "created_at": "2019-09-26T19:46:58.282Z",
            "updated_at": "2019-09-26T19:46:58.282Z",
            "due_date": "2019-10-03",
            "start_date": null,
            "web_url": "https://gitlab.com/Alexistower/foodcravings/-/milestones/1"
        },
        "assignees": [
            {
                "id": 3447536,
                "name": "Alexis Torres",
                "username": "Alexistower",
                "state": "active",
                "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
                "web_url": "https://gitlab.com/Alexistower"
            }
        ],
        "author": {
            "id": 4564993,
            "name": "Rishabh Thakkar",
            "username": "ribsthakkar",
            "state": "active",
            "avatar_url": "https://secure.gravatar.com/avatar/f3b932e2f5844d271ad4a352d46b4a44?s=80&d=identicon",
            "web_url": "https://gitlab.com/ribsthakkar"
        },
        "assignee": {
            "id": 3447536,
            "name": "Alexis Torres",
            "username": "Alexistower",
            "state": "active",
            "avatar_url": "https://assets.gitlab-static.net/uploads/-/system/user/avatar/3447536/avatar.png",
            "web_url": "https://gitlab.com/Alexistower"
        },
        "user_notes_count": 0,
        "merge_requests_count": 0,
        "upvotes": 0,
        "downvotes": 0,
        "due_date": "2019-10-03",
        "confidential": false,
        "discussion_locked": null,
        "web_url": "https://gitlab.com/Alexistower/foodcravings/issues/1",
        "time_stats": {
            "time_estimate": 0,
            "total_time_spent": 0,
            "human_time_estimate": null,
            "human_total_time_spent": null
        },
        "task_completion_status": {
            "count": 0,
            "completed_count": 0
        },
        "has_tasks": false,
        "_links": {
            "self": "https://gitlab.com/api/v4/projects/14484432/issues/1",
            "notes": "https://gitlab.com/api/v4/projects/14484432/issues/1/notes",
            "award_emoji": "https://gitlab.com/api/v4/projects/14484432/issues/1/award_emoji",
            "project": "https://gitlab.com/api/v4/projects/14484432"
        },
        "weight": null
    }
];

export const gitComm: Gitlab_commits[] = [
    {
        "id": "430641d905b8394ed4dadb0c7017bb5c4400dc49",
        "short_id": "430641d9",
        "created_at": "2019-10-04T18:49:50.000Z",
        "parent_ids": [
            "26c71d6382f3b1c1a765da6db6d26e89ccb1006b"
        ],
        "title": "Added git SHA to readme for Phase 1",
        "message": "Added git SHA to readme for Phase 1",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-04T18:49:50.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-04T18:49:50.000Z"
    },
    {
        "id": "26c71d6382f3b1c1a765da6db6d26e89ccb1006b",
        "short_id": "26c71d63",
        "created_at": "2019-10-04T18:33:37.000Z",
        "parent_ids": [
            "2c2dd48719fb1b17d979b646665048bc7a30c340",
            "64fe9ed8a236941f2879ffbc3b4ea2d8b8e2fd12"
        ],
        "title": "Merge branch 'ad/fix_ci_again' into 'master'",
        "message": "Merge branch 'ad/fix_ci_again' into 'master'\n\nAd/fix ci again\n\nSee merge request Alexistower/foodcravings!23",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-04T18:33:37.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-04T18:33:37.000Z"
    },
    {
        "id": "2c2dd48719fb1b17d979b646665048bc7a30c340",
        "short_id": "2c2dd487",
        "created_at": "2019-10-04T18:32:39.000Z",
        "parent_ids": [
            "23c10a7bddc5de23df6b8f055416a29e3fc82065",
            "a0ce6d0571752b3ca3632f85fe5f87d643b941cf"
        ],
        "title": "Merge branch 'rt/improvetable' into 'master'",
        "message": "Merge branch 'rt/improvetable' into 'master'\n\nImproved the restaurant instacnce table just a little bit\n\nSee merge request Alexistower/foodcravings!21",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-04T18:32:39.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-04T18:32:39.000Z"
    },
    {
        "id": "a0ce6d0571752b3ca3632f85fe5f87d643b941cf",
        "short_id": "a0ce6d05",
        "created_at": "2019-10-04T18:32:39.000Z",
        "parent_ids": [
            "23c10a7bddc5de23df6b8f055416a29e3fc82065"
        ],
        "title": "Improved the restaurant instacnce table just a little bit",
        "message": "Improved the restaurant instacnce table just a little bit\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-04T18:32:39.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-04T18:32:39.000Z"
    },
    {
        "id": "23c10a7bddc5de23df6b8f055416a29e3fc82065",
        "short_id": "23c10a7b",
        "created_at": "2019-10-04T18:31:42.000Z",
        "parent_ids": [
            "610583b46707fca99074708c664b6878f2db849a",
            "85c093d7484d03d711f635a49c4ea6fa41512223"
        ],
        "title": "Merge branch 'rt/techincalReport' into 'master'",
        "message": "Merge branch 'rt/techincalReport' into 'master'\n\nRt/techincal report\n\nSee merge request Alexistower/foodcravings!22",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-04T18:31:42.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-04T18:31:42.000Z"
    },
    {
        "id": "85c093d7484d03d711f635a49c4ea6fa41512223",
        "short_id": "85c093d7",
        "created_at": "2019-10-04T18:31:42.000Z",
        "parent_ids": [
            "610583b46707fca99074708c664b6878f2db849a"
        ],
        "title": "Rt/techincal report",
        "message": "Rt/techincal report\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-04T18:31:42.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-04T18:31:42.000Z"
    },
    {
        "id": "64fe9ed8a236941f2879ffbc3b4ea2d8b8e2fd12",
        "short_id": "64fe9ed8",
        "created_at": "2019-10-04T18:29:31.000Z",
        "parent_ids": [
            "b97fef53fcc6aadf847033f152d6d4451d9b34e1"
        ],
        "title": "return to master only",
        "message": "return to master only\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-04T18:29:31.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-04T18:29:31.000Z"
    },
    {
        "id": "b97fef53fcc6aadf847033f152d6d4451d9b34e1",
        "short_id": "b97fef53",
        "created_at": "2019-10-04T16:17:45.000Z",
        "parent_ids": [
            "f3dfba1fc063df1adc88e754368ecc6d1daae621"
        ],
        "title": "cache correct folder",
        "message": "cache correct folder\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-04T16:17:45.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-04T16:17:45.000Z"
    },
    {
        "id": "f3dfba1fc063df1adc88e754368ecc6d1daae621",
        "short_id": "f3dfba1f",
        "created_at": "2019-10-04T02:08:25.000Z",
        "parent_ids": [
            "610583b46707fca99074708c664b6878f2db849a"
        ],
        "title": "lets try to fix deploy",
        "message": "lets try to fix deploy\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-04T02:08:25.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-04T02:08:25.000Z"
    },
    {
        "id": "610583b46707fca99074708c664b6878f2db849a",
        "short_id": "610583b4",
        "created_at": "2019-10-04T01:03:16.000Z",
        "parent_ids": [
            "02d1ee348f6a1e9c08cf35308a87e22aa171f08d",
            "a56b9834eccacf5adc035e2022fd488895a69be4"
        ],
        "title": "Merge branch 'as/fix_return_required' into 'master'",
        "message": "Merge branch 'as/fix_return_required' into 'master'\n\nfix: lambda must return\n\nSee merge request Alexistower/foodcravings!20",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-04T01:03:16.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-04T01:03:16.000Z"
    },
    {
        "id": "a56b9834eccacf5adc035e2022fd488895a69be4",
        "short_id": "a56b9834",
        "created_at": "2019-10-04T00:30:34.000Z",
        "parent_ids": [
            "02d1ee348f6a1e9c08cf35308a87e22aa171f08d"
        ],
        "title": "fix: lambda must return",
        "message": "fix: lambda must return\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-04T00:30:34.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-04T00:30:34.000Z"
    },
    {
        "id": "02d1ee348f6a1e9c08cf35308a87e22aa171f08d",
        "short_id": "02d1ee34",
        "created_at": "2019-10-04T00:07:18.000Z",
        "parent_ids": [
            "25121e860b3b3ec66a5ab0225c0ae656600574bc",
            "5f12de949b09010e11f4867259c988539db96560"
        ],
        "title": "Merge branch 'ad/gitlab_ci' into 'master'",
        "message": "Merge branch 'ad/gitlab_ci' into 'master'\n\nAd/gitlab ci\n\nSee merge request Alexistower/foodcravings!19",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-04T00:07:18.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-04T00:07:18.000Z"
    },
    {
        "id": "5f12de949b09010e11f4867259c988539db96560",
        "short_id": "5f12de94",
        "created_at": "2019-10-04T00:07:18.000Z",
        "parent_ids": [
            "25121e860b3b3ec66a5ab0225c0ae656600574bc"
        ],
        "title": "feat: gitlab CI",
        "message": "feat: gitlab CI\nrefactor: remove stories clutter\ncloses: 37\n",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-04T00:07:18.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-04T00:07:18.000Z"
    },
    {
        "id": "25121e860b3b3ec66a5ab0225c0ae656600574bc",
        "short_id": "25121e86",
        "created_at": "2019-10-03T23:27:33.000Z",
        "parent_ids": [
            "d47359f4892c7364b8c854a395752511ac1c2790",
            "e93206fc555d295e4e6d8903217b449f5841c940"
        ],
        "title": "Merge branch 'rt/updatesourcetable' into 'master'",
        "message": "Merge branch 'rt/updatesourcetable' into 'master'\n\nUpdated Source Table Aesthetic and Postman Docs\n\nSee merge request Alexistower/foodcravings!17",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T23:27:33.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T23:27:33.000Z"
    },
    {
        "id": "e93206fc555d295e4e6d8903217b449f5841c940",
        "short_id": "e93206fc",
        "created_at": "2019-10-03T23:27:33.000Z",
        "parent_ids": [
            "d47359f4892c7364b8c854a395752511ac1c2790"
        ],
        "title": "Revert \"Updated Source Table and technical report\"",
        "message": "Revert \"Updated Source Table and technical report\"\n\nThis reverts commit af1620a02bdbc8a1c67664394d4297e3bcec0cd3.\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T23:27:33.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T23:27:33.000Z"
    },
    {
        "id": "d47359f4892c7364b8c854a395752511ac1c2790",
        "short_id": "d47359f4",
        "created_at": "2019-10-03T23:14:26.000Z",
        "parent_ids": [
            "f1301fec53db8176b0151348e7dd71660e959831",
            "39c256d2182c6179ccbd627251de6803bd961098"
        ],
        "title": "Merge branch 'aboutFix' into 'master'",
        "message": "Merge branch 'aboutFix' into 'master'\n\nFixed commit count and readme\n\nSee merge request Alexistower/foodcravings!16",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T23:14:26.000Z",
        "committer_name": "Rishabh Thakkar",
        "committer_email": "rishabh.thakkar@gmail.com",
        "committed_date": "2019-10-03T23:14:26.000Z"
    },
    {
        "id": "39c256d2182c6179ccbd627251de6803bd961098",
        "short_id": "39c256d2",
        "created_at": "2019-10-03T20:29:44.000Z",
        "parent_ids": [
            "f1301fec53db8176b0151348e7dd71660e959831"
        ],
        "title": "Fixed commit count and readme",
        "message": "Fixed commit count and readme\n",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T20:29:44.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T20:29:44.000Z"
    },
    {
        "id": "f1301fec53db8176b0151348e7dd71660e959831",
        "short_id": "f1301fec",
        "created_at": "2019-10-03T19:14:30.000Z",
        "parent_ids": [
            "633ac9f75be3ada0b2b9909dccbb39f5d20221f2",
            "f568c6ff66b74daed6e577ca27b1a740840f1380"
        ],
        "title": "Merge branch 'rt/techincalReport' into 'master'",
        "message": "Merge branch 'rt/techincalReport' into 'master'\n\nUploaded technical report\n\nSee merge request Alexistower/foodcravings!15",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T19:14:30.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T19:14:30.000Z"
    },
    {
        "id": "f568c6ff66b74daed6e577ca27b1a740840f1380",
        "short_id": "f568c6ff",
        "created_at": "2019-10-03T19:14:30.000Z",
        "parent_ids": [
            "633ac9f75be3ada0b2b9909dccbb39f5d20221f2"
        ],
        "title": "Uploaded technical report",
        "message": "Uploaded technical report\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T19:14:30.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T19:14:30.000Z"
    },
    {
        "id": "633ac9f75be3ada0b2b9909dccbb39f5d20221f2",
        "short_id": "633ac9f7",
        "created_at": "2019-10-03T19:13:34.000Z",
        "parent_ids": [
            "55ebbda2a9bc0444bc944e9c35bc47c7df4a5272",
            "7f2b9c91635cda221e64cab3e255f359d2d7d889"
        ],
        "title": "Merge branch 'rt/pageRouting' into 'master'",
        "message": "Merge branch 'rt/pageRouting' into 'master'\n\nFixed Page Routing and added images\n\nSee merge request Alexistower/foodcravings!14",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T19:13:34.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T19:13:34.000Z"
    },
    {
        "id": "7f2b9c91635cda221e64cab3e255f359d2d7d889",
        "short_id": "7f2b9c91",
        "created_at": "2019-10-03T19:13:34.000Z",
        "parent_ids": [
            "55ebbda2a9bc0444bc944e9c35bc47c7df4a5272"
        ],
        "title": "Fixed Page Routing and added images",
        "message": "Fixed Page Routing and added images\n\nFixed routing across pages in the nav bar and added images for restaurants\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T19:13:34.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T19:13:34.000Z"
    },
    {
        "id": "55ebbda2a9bc0444bc944e9c35bc47c7df4a5272",
        "short_id": "55ebbda2",
        "created_at": "2019-10-03T18:33:12.000Z",
        "parent_ids": [
            "b4988205735e30ac68eb9d70ddb65264ea61c041",
            "111d6493ed5e841ce279547dd78c9fa0647fbdf1"
        ],
        "title": "Merge branch 'PageLinking' into 'master'",
        "message": "Merge branch 'PageLinking' into 'master'\n\nAdded support for instance pages to link with one another and fixed other...\n\nSee merge request Alexistower/foodcravings!13",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T18:33:12.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T18:33:12.000Z"
    },
    {
        "id": "111d6493ed5e841ce279547dd78c9fa0647fbdf1",
        "short_id": "111d6493",
        "created_at": "2019-10-03T18:33:12.000Z",
        "parent_ids": [
            "b4988205735e30ac68eb9d70ddb65264ea61c041"
        ],
        "title": "Added support for instance pages to link with one another and fixed other routing bug with restaurant page",
        "message": "Added support for instance pages to link with one another and fixed other routing bug with restaurant page\n\nThis addresses #24, #25, #36, #41\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T18:33:12.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T18:33:12.000Z"
    },
    {
        "id": "b4988205735e30ac68eb9d70ddb65264ea61c041",
        "short_id": "b4988205",
        "created_at": "2019-10-03T16:07:15.000Z",
        "parent_ids": [
            "6693a8c401d874020edd1c57c9a12806ca939c96",
            "d380c7ce1fd178a82a9133c28ac2fa291f17aeb3"
        ],
        "title": "Merge branch 'ad/source_array_to_string' into 'master'",
        "message": "Merge branch 'ad/source_array_to_string' into 'master'\n\nfix: parseField should return concat string for arrays\n\nSee merge request Alexistower/foodcravings!12",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T16:07:15.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T16:07:15.000Z"
    },
    {
        "id": "6693a8c401d874020edd1c57c9a12806ca939c96",
        "short_id": "6693a8c4",
        "created_at": "2019-10-03T05:36:23.000Z",
        "parent_ids": [
            "b3610dac174f2da7a5c1830079bd3bf6d343d038",
            "3be5b35272a1c2641d4bb857accdaff6e7446039"
        ],
        "title": "Merge branch 'restaurantInstance' into 'master'",
        "message": "Merge branch 'restaurantInstance' into 'master'\n\nRestaurant instance\n\nSee merge request Alexistower/foodcravings!8",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-03T05:36:23.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-03T05:36:23.000Z"
    },
    {
        "id": "3be5b35272a1c2641d4bb857accdaff6e7446039",
        "short_id": "3be5b352",
        "created_at": "2019-10-03T05:35:15.000Z",
        "parent_ids": [
            "c05c24dc9980af6e7b72eb5129ce6bfb4210ec3d",
            "b3610dac174f2da7a5c1830079bd3bf6d343d038"
        ],
        "title": "Merge branches 'master' and 'restaurantInstance' of https://gitlab.com/Alexistower/foodcravings into restaurantInstance",
        "message": "Merge branches 'master' and 'restaurantInstance' of https://gitlab.com/Alexistower/foodcravings into restaurantInstance\n\n Conflicts:\n\tfrontend/src/App.tsx\n\tfrontend/src/static_data/recipes.tsx\n\tfrontend/src/static_data/restaurants.tsx\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T05:35:15.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T05:35:15.000Z"
    },
    {
        "id": "d380c7ce1fd178a82a9133c28ac2fa291f17aeb3",
        "short_id": "d380c7ce",
        "created_at": "2019-10-03T05:21:28.000Z",
        "parent_ids": [
            "b3610dac174f2da7a5c1830079bd3bf6d343d038"
        ],
        "title": "fix: parseField should return concat string for arrays",
        "message": "fix: parseField should return concat string for arrays\n\nfeat: fields should have titles\n\nfix: some fieldtable data incorrect\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T05:21:28.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T05:21:28.000Z"
    },
    {
        "id": "c05c24dc9980af6e7b72eb5129ce6bfb4210ec3d",
        "short_id": "c05c24dc",
        "created_at": "2019-10-03T04:07:09.000Z",
        "parent_ids": [
            "d320c66283f0813268ae2709d82bd5e1ce59eeae"
        ],
        "title": "restaurant pages ugly but working",
        "message": "restaurant pages ugly but working\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T04:07:09.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T04:07:09.000Z"
    },
    {
        "id": "b3610dac174f2da7a5c1830079bd3bf6d343d038",
        "short_id": "b3610dac",
        "created_at": "2019-10-03T03:42:07.000Z",
        "parent_ids": [
            "516c36a47a89126563b0a324f8175fa0716162f1",
            "b7cee16cf9786ea29a9fd5ec084f5a2406bee310"
        ],
        "title": "Merge branch 'aboutPage' into 'master'",
        "message": "Merge branch 'aboutPage' into 'master'\n\nhave basic about page and splash\n\nSee merge request Alexistower/foodcravings!7",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-03T03:42:07.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-03T03:42:07.000Z"
    },
    {
        "id": "b7cee16cf9786ea29a9fd5ec084f5a2406bee310",
        "short_id": "b7cee16c",
        "created_at": "2019-10-03T03:42:07.000Z",
        "parent_ids": [
            "516c36a47a89126563b0a324f8175fa0716162f1"
        ],
        "title": "have basic about page and splash",
        "message": "have basic about page and splash\n",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T03:42:07.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-03T03:42:07.000Z"
    },
    {
        "id": "516c36a47a89126563b0a324f8175fa0716162f1",
        "short_id": "516c36a4",
        "created_at": "2019-10-03T03:26:30.000Z",
        "parent_ids": [
            "07fd901c46cb804b02d8213f15e9127e1c2a8638",
            "6f2248f314c0c631c13655890f933654b132c3d1"
        ],
        "title": "Merge branch 'rg/sourcetable' into 'master'",
        "message": "Merge branch 'rg/sourcetable' into 'master'\n\nRg/sourcetable\n\nSee merge request Alexistower/foodcravings!11",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T03:26:30.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T03:26:30.000Z"
    },
    {
        "id": "6f2248f314c0c631c13655890f933654b132c3d1",
        "short_id": "6f2248f3",
        "created_at": "2019-10-03T03:23:25.000Z",
        "parent_ids": [
            "80b9cfbc397a3b19f01c6596f524b51335791398",
            "07fd901c46cb804b02d8213f15e9127e1c2a8638"
        ],
        "title": "Merge branches 'master' and 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable",
        "message": "Merge branches 'master' and 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable\n\n Conflicts:\n\tfrontend/src/App.tsx\n\tfrontend/src/components/pages/recipes.js\n\tfrontend/src/static_data/recipes.tsx\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T03:23:25.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T03:23:25.000Z"
    },
    {
        "id": "80b9cfbc397a3b19f01c6596f524b51335791398",
        "short_id": "80b9cfbc",
        "created_at": "2019-10-03T03:15:11.000Z",
        "parent_ids": [
            "351d303e9c0723bd662345310f0dda24ae6e2750"
        ],
        "title": "feat: source table",
        "message": "feat: source table\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T03:15:11.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T03:15:11.000Z"
    },
    {
        "id": "07fd901c46cb804b02d8213f15e9127e1c2a8638",
        "short_id": "07fd901c",
        "created_at": "2019-10-03T03:10:23.000Z",
        "parent_ids": [
            "02b200561891a6ea6d0076e20cb293660a52bb30",
            "780ea6ecc405c3f13c93da07970ae96f7a1add60"
        ],
        "title": "Merge branch 'recipeInstance' into 'master'",
        "message": "Merge branch 'recipeInstance' into 'master'\n\nRecipeInstance\n\nSee merge request Alexistower/foodcravings!6",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-03T03:10:23.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-03T03:10:23.000Z"
    },
    {
        "id": "d320c66283f0813268ae2709d82bd5e1ce59eeae",
        "short_id": "d320c662",
        "created_at": "2019-10-03T02:41:00.000Z",
        "parent_ids": [
            "ddbafbe55aa2c97cc42df5743fa71a2e854ad6e0",
            "02b200561891a6ea6d0076e20cb293660a52bb30"
        ],
        "title": "Merge branch 'master' of https://gitlab.com/Alexistower/foodcravings into restaurantInstance",
        "message": "Merge branch 'master' of https://gitlab.com/Alexistower/foodcravings into restaurantInstance\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T02:41:00.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T02:41:00.000Z"
    },
    {
        "id": "780ea6ecc405c3f13c93da07970ae96f7a1add60",
        "short_id": "780ea6ec",
        "created_at": "2019-10-03T02:35:54.000Z",
        "parent_ids": [
            "163266d2c88e40089ff02fbb038a533fe246813c"
        ],
        "title": "recipeinstance ugly but working",
        "message": "recipeinstance ugly but working\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T02:35:54.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T02:35:54.000Z"
    },
    {
        "id": "163266d2c88e40089ff02fbb038a533fe246813c",
        "short_id": "163266d2",
        "created_at": "2019-10-03T01:14:21.000Z",
        "parent_ids": [
            "72d1035343f52c0d0a9944170825e20e2db56cf2",
            "02b200561891a6ea6d0076e20cb293660a52bb30"
        ],
        "title": "resolving merge stuff",
        "message": "resolving merge stuff\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T01:14:21.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T01:14:21.000Z"
    },
    {
        "id": "351d303e9c0723bd662345310f0dda24ae6e2750",
        "short_id": "351d303e",
        "created_at": "2019-10-03T00:48:16.000Z",
        "parent_ids": [
            "35c8b2c1f68c0c40a45eeb04b0d7deecd0ef43dc"
        ],
        "title": "fix: source static data",
        "message": "fix: source static data\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T00:48:16.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T00:48:16.000Z"
    },
    {
        "id": "35c8b2c1f68c0c40a45eeb04b0d7deecd0ef43dc",
        "short_id": "35c8b2c1",
        "created_at": "2019-10-03T00:45:51.000Z",
        "parent_ids": [
            "b7ddde4dd0884d896b647be4e694bf1a0e3bdb35",
            "368569285242b7a99f4c4e079f1956f28a3ea937"
        ],
        "title": "Merge branch 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable",
        "message": "Merge branch 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T00:45:51.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T00:45:51.000Z"
    },
    {
        "id": "b7ddde4dd0884d896b647be4e694bf1a0e3bdb35",
        "short_id": "b7ddde4d",
        "created_at": "2019-10-03T00:45:28.000Z",
        "parent_ids": [
            "46af76a149a8dc8d105f939c4db7e99f46aeee61"
        ],
        "title": "moved sourcedfields",
        "message": "moved sourcedfields\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T00:45:28.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T00:45:28.000Z"
    },
    {
        "id": "368569285242b7a99f4c4e079f1956f28a3ea937",
        "short_id": "36856928",
        "created_at": "2019-10-03T00:39:04.000Z",
        "parent_ids": [
            "14e34c778c274a0a1e94a85640f5007bbb1b3f6c"
        ],
        "title": "fix: idk",
        "message": "fix: idk\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T00:39:04.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T00:39:04.000Z"
    },
    {
        "id": "14e34c778c274a0a1e94a85640f5007bbb1b3f6c",
        "short_id": "14e34c77",
        "created_at": "2019-10-03T00:32:57.000Z",
        "parent_ids": [
            "8eb6dc5debf6b3a26e261ac40d032d10e13e3ae3",
            "46af76a149a8dc8d105f939c4db7e99f46aeee61"
        ],
        "title": "Merge branch 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable",
        "message": "Merge branch 'rg/sourcetable' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable\n\n Conflicts:\n\tfrontend/package-lock.json\n\tfrontend/src/App.tsx\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T00:32:57.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T00:32:57.000Z"
    },
    {
        "id": "8eb6dc5debf6b3a26e261ac40d032d10e13e3ae3",
        "short_id": "8eb6dc5d",
        "created_at": "2019-10-03T00:29:26.000Z",
        "parent_ids": [
            "1f277168f27279296a6b4568356aa8d461f4ea3b",
            "02b200561891a6ea6d0076e20cb293660a52bb30"
        ],
        "title": "Merge branch 'master' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable",
        "message": "Merge branch 'master' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-03T00:29:26.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-03T00:29:26.000Z"
    },
    {
        "id": "02b200561891a6ea6d0076e20cb293660a52bb30",
        "short_id": "02b20056",
        "created_at": "2019-10-03T00:28:22.000Z",
        "parent_ids": [
            "01a2f401e28fe2daf6ddf7af122669a84940b593",
            "d5f0b21f6b9639350b2bb1eef7f2a78faf5d12c6"
        ],
        "title": "Merge branch 'rt/groceryInstance' into 'master'",
        "message": "Merge branch 'rt/groceryInstance' into 'master'\n\nRt/grocery instance\n\nSee merge request Alexistower/foodcravings!9",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-10-03T00:28:22.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-10-03T00:28:22.000Z"
    },
    {
        "id": "46af76a149a8dc8d105f939c4db7e99f46aeee61",
        "short_id": "46af76a1",
        "created_at": "2019-10-03T00:28:21.000Z",
        "parent_ids": [
            "a6705ec918b9accb0d71345f4efa3c6afb00c75d"
        ],
        "title": "some work on sourcetable",
        "message": "some work on sourcetable\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-03T00:28:21.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-03T00:28:21.000Z"
    },
    {
        "id": "d5f0b21f6b9639350b2bb1eef7f2a78faf5d12c6",
        "short_id": "d5f0b21f",
        "created_at": "2019-10-03T00:11:59.000Z",
        "parent_ids": [
            "43f86ce74a9d0a2843a49455149dcbea029879d0"
        ],
        "title": "Finished implementation of grocery instance page",
        "message": "Finished implementation of grocery instance page\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-03T00:11:59.000Z",
        "committer_name": "ribsthakkar",
        "committer_email": "rishabh.thakkar@gmail.com",
        "committed_date": "2019-10-03T00:11:59.000Z"
    },
    {
        "id": "a6705ec918b9accb0d71345f4efa3c6afb00c75d",
        "short_id": "a6705ec9",
        "created_at": "2019-10-02T23:03:38.000Z",
        "parent_ids": [
            "734053c8eb81b7562dd833e0fe3b871071eed737",
            "43f86ce74a9d0a2843a49455149dcbea029879d0"
        ],
        "title": "Merge branch 'rt/groceryInstance' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable",
        "message": "Merge branch 'rt/groceryInstance' of https://gitlab.com/Alexistower/foodcravings into rg/sourcetable\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-02T23:03:38.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-02T23:03:38.000Z"
    },
    {
        "id": "ddbafbe55aa2c97cc42df5743fa71a2e854ad6e0",
        "short_id": "ddbafbe5",
        "created_at": "2019-10-02T22:02:41.000Z",
        "parent_ids": [
            "e2addb98f63511041e314dab463da1c8c6f35857",
            "01a2f401e28fe2daf6ddf7af122669a84940b593"
        ],
        "title": "Merge branch 'master' into 'restaurantInstance'",
        "message": "Merge branch 'master' into 'restaurantInstance'\n\n# Conflicts:\n#   frontend/static_data/recipes.tsx",
        "author_name": "Kevin Gao",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-02T22:02:41.000Z",
        "committer_name": "Kevin Gao",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-02T22:02:41.000Z"
    },
    {
        "id": "43f86ce74a9d0a2843a49455149dcbea029879d0",
        "short_id": "43f86ce7",
        "created_at": "2019-10-02T21:58:25.000Z",
        "parent_ids": [
            "01a2f401e28fe2daf6ddf7af122669a84940b593"
        ],
        "title": "Initial attempt at putting together GroceryInstance Page",
        "message": "Initial attempt at putting together GroceryInstance Page\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-02T21:58:25.000Z",
        "committer_name": "ribsthakkar",
        "committer_email": "rishabh.thakkar@gmail.com",
        "committed_date": "2019-10-02T21:58:25.000Z"
    },
    {
        "id": "e2addb98f63511041e314dab463da1c8c6f35857",
        "short_id": "e2addb98",
        "created_at": "2019-10-02T20:09:51.000Z",
        "parent_ids": [
            "9bcc4de81e5c06b1e50484e7942e80fbbd4f87de"
        ],
        "title": "Update",
        "message": "Update\n",
        "author_name": "kevingao232",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-02T20:09:51.000Z",
        "committer_name": "kevingao232",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-02T20:09:51.000Z"
    },
    {
        "id": "72d1035343f52c0d0a9944170825e20e2db56cf2",
        "short_id": "72d10353",
        "created_at": "2019-10-02T20:07:50.000Z",
        "parent_ids": [
            "fd3064828574a0a756539f45c06ed7aae0d6f96c"
        ],
        "title": "Updated, but not complete",
        "message": "Updated, but not complete\n",
        "author_name": "kevingao232",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-02T20:07:50.000Z",
        "committer_name": "kevingao232",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-02T20:07:50.000Z"
    },
    {
        "id": "9bcc4de81e5c06b1e50484e7942e80fbbd4f87de",
        "short_id": "9bcc4de8",
        "created_at": "2019-10-02T20:06:48.000Z",
        "parent_ids": [
            "3620dd468da8b378338a08c365103bfd14efbaa6"
        ],
        "title": "prototype instance",
        "message": "prototype instance\n",
        "author_name": "kevingao232",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-02T20:06:48.000Z",
        "committer_name": "kevingao232",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-02T20:06:48.000Z"
    },
    {
        "id": "3620dd468da8b378338a08c365103bfd14efbaa6",
        "short_id": "3620dd46",
        "created_at": "2019-10-02T19:37:30.000Z",
        "parent_ids": [
            "fd3064828574a0a756539f45c06ed7aae0d6f96c"
        ],
        "title": "Finished a general restaurantInstance.",
        "message": "Finished a general restaurantInstance.\n",
        "author_name": "kevingao232",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-02T19:37:30.000Z",
        "committer_name": "kevingao232",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-02T19:37:30.000Z"
    },
    {
        "id": "734053c8eb81b7562dd833e0fe3b871071eed737",
        "short_id": "734053c8",
        "created_at": "2019-10-02T17:25:43.000Z",
        "parent_ids": [
            "02c15544617188568e9d7154590768713d5e09ba",
            "01a2f401e28fe2daf6ddf7af122669a84940b593"
        ],
        "title": "merge resolves",
        "message": "merge resolves\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-02T17:25:43.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-02T17:25:43.000Z"
    },
    {
        "id": "02c15544617188568e9d7154590768713d5e09ba",
        "short_id": "02c15544",
        "created_at": "2019-10-02T17:24:37.000Z",
        "parent_ids": [
            "1f277168f27279296a6b4568356aa8d461f4ea3b"
        ],
        "title": "merge stuff",
        "message": "merge stuff\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-02T17:24:37.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-02T17:24:37.000Z"
    },
    {
        "id": "01a2f401e28fe2daf6ddf7af122669a84940b593",
        "short_id": "01a2f401",
        "created_at": "2019-10-02T04:48:03.000Z",
        "parent_ids": [
            "8020ac7724b440b4056633a533f389d3938908d9",
            "a19bd5310af7984626b3c0a305bbe85ec472725b"
        ],
        "title": "Merge branch 'ad/models' into 'master'",
        "message": "Merge branch 'ad/models' into 'master'\n\nfeat: create models\n\nSee merge request Alexistower/foodcravings!5",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-02T04:48:03.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-02T04:48:03.000Z"
    },
    {
        "id": "a19bd5310af7984626b3c0a305bbe85ec472725b",
        "short_id": "a19bd531",
        "created_at": "2019-10-02T02:30:53.000Z",
        "parent_ids": [
            "34973a46bf7d7545738090f382557a09148284b6"
        ],
        "title": "Updated data and object models",
        "message": "Updated data and object models\n",
        "author_name": "ribsthakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-02T02:30:53.000Z",
        "committer_name": "ribsthakkar",
        "committer_email": "rishabh.thakkar@gmail.com",
        "committed_date": "2019-10-02T02:30:53.000Z"
    },
    {
        "id": "fd3064828574a0a756539f45c06ed7aae0d6f96c",
        "short_id": "fd306482",
        "created_at": "2019-10-01T23:43:01.000Z",
        "parent_ids": [
            "8020ac7724b440b4056633a533f389d3938908d9"
        ],
        "title": "Need to generalize the recipe reference.",
        "message": "Need to generalize the recipe reference.\n",
        "author_name": "kevingao232",
        "author_email": "kevin.gao232@gmail.com",
        "authored_date": "2019-10-01T23:43:01.000Z",
        "committer_name": "kevingao232",
        "committer_email": "kevin.gao232@gmail.com",
        "committed_date": "2019-10-01T23:43:01.000Z"
    },
    {
        "id": "34973a46bf7d7545738090f382557a09148284b6",
        "short_id": "34973a46",
        "created_at": "2019-10-01T23:25:07.000Z",
        "parent_ids": [
            "8020ac7724b440b4056633a533f389d3938908d9"
        ],
        "title": "feat: create models",
        "message": "feat: create models\n",
        "author_name": "arkd",
        "author_email": "aaronmdewitt@gmail.com",
        "authored_date": "2019-10-01T23:25:07.000Z",
        "committer_name": "arkd",
        "committer_email": "aaronmdewitt@gmail.com",
        "committed_date": "2019-10-01T23:25:07.000Z"
    },
    {
        "id": "8020ac7724b440b4056633a533f389d3938908d9",
        "short_id": "8020ac77",
        "created_at": "2019-10-01T20:03:01.000Z",
        "parent_ids": [
            "cfe80f6900026f8a8632dea8ff24000551762bcc",
            "36d76d8e8a73a21cea585de69cb255611dffcf7f"
        ],
        "title": "Merge branch 'rt/staticdatasources' into 'master'",
        "message": "Merge branch 'rt/staticdatasources' into 'master'\n\nAdded 3 recipes, grocery items, and resteraunts for static website phase 1\n\nSee merge request Alexistower/foodcravings!4",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-10-01T20:03:01.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-01T20:03:01.000Z"
    },
    {
        "id": "36d76d8e8a73a21cea585de69cb255611dffcf7f",
        "short_id": "36d76d8e",
        "created_at": "2019-10-01T20:03:01.000Z",
        "parent_ids": [
            "cfe80f6900026f8a8632dea8ff24000551762bcc"
        ],
        "title": "Added 3 recipes, grocery items, and resteraunts for static website phase 1",
        "message": "Added 3 recipes, grocery items, and resteraunts for static website phase 1\n\nFor menus: used menusapi.xyz\nFor recipes: used edamam api\nFor groceries: used walmart and edamam api\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-10-01T20:03:01.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-10-01T20:03:01.000Z"
    },
    {
        "id": "1f277168f27279296a6b4568356aa8d461f4ea3b",
        "short_id": "1f277168",
        "created_at": "2019-10-01T00:16:39.000Z",
        "parent_ids": [
            "ef4dc76e0fc48f419cce5385afbca13199e62ab3",
            "cfe80f6900026f8a8632dea8ff24000551762bcc"
        ],
        "title": "merge conflict resolving",
        "message": "merge conflict resolving\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-01T00:16:39.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-01T00:16:39.000Z"
    },
    {
        "id": "ef4dc76e0fc48f419cce5385afbca13199e62ab3",
        "short_id": "ef4dc76e",
        "created_at": "2019-10-01T00:13:54.000Z",
        "parent_ids": [
            "4c9d3305b12156100d1ab746d958a4fac9aaaa5a"
        ],
        "title": "merge stuff",
        "message": "merge stuff\n",
        "author_name": "rguan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-10-01T00:13:54.000Z",
        "committer_name": "rguan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-10-01T00:13:54.000Z"
    },
    {
        "id": "cfe80f6900026f8a8632dea8ff24000551762bcc",
        "short_id": "cfe80f69",
        "created_at": "2019-09-30T23:57:40.000Z",
        "parent_ids": [
            "4c9d3305b12156100d1ab746d958a4fac9aaaa5a",
            "1ad201c16acc69021e9812e5576635ad61ce5dc5"
        ],
        "title": "Merge branch 'AT/basicFramework' into 'master'",
        "message": "Merge branch 'AT/basicFramework' into 'master'\n\nmade the basic frontend framework\n\nSee merge request Alexistower/foodcravings!3",
        "author_name": "Robert Guan",
        "author_email": "guan.robert@utexas.edu",
        "authored_date": "2019-09-30T23:57:40.000Z",
        "committer_name": "Robert Guan",
        "committer_email": "guan.robert@utexas.edu",
        "committed_date": "2019-09-30T23:57:40.000Z"
    },
    {
        "id": "1ad201c16acc69021e9812e5576635ad61ce5dc5",
        "short_id": "1ad201c1",
        "created_at": "2019-09-30T23:49:44.000Z",
        "parent_ids": [
            "4c9d3305b12156100d1ab746d958a4fac9aaaa5a"
        ],
        "title": "made the basic frontend framework",
        "message": "made the basic frontend framework\n",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-09-30T23:49:44.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-09-30T23:49:44.000Z"
    },
    {
        "id": "4c9d3305b12156100d1ab746d958a4fac9aaaa5a",
        "short_id": "4c9d3305",
        "created_at": "2019-09-27T20:35:03.000Z",
        "parent_ids": [
            "e1c5a8dc2b7670f045f34f1a70f80258bd74a0f7",
            "a83288a11ef1b3e2f8f139c5df87a547c4744183"
        ],
        "title": "Merge branch 'base_flask_app' into 'master'",
        "message": "Merge branch 'base_flask_app' into 'master'\n\nImplemented Hello World Flask App\n\nSee merge request Alexistower/foodcravings!1",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-09-27T20:35:03.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-09-27T20:35:03.000Z"
    },
    {
        "id": "a83288a11ef1b3e2f8f139c5df87a547c4744183",
        "short_id": "a83288a1",
        "created_at": "2019-09-27T20:35:03.000Z",
        "parent_ids": [
            "e1c5a8dc2b7670f045f34f1a70f80258bd74a0f7"
        ],
        "title": "Implemented Hello World Flask App",
        "message": "Implemented Hello World Flask App\n\nPrepared the backend framework with the inital setup of the backend Flask application. The application runs using docker\n",
        "author_name": "Rishabh Thakkar",
        "author_email": "rishabh.thakkar@gmail.com",
        "authored_date": "2019-09-27T20:35:03.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-09-27T20:35:03.000Z"
    },
    {
        "id": "e1c5a8dc2b7670f045f34f1a70f80258bd74a0f7",
        "short_id": "e1c5a8dc",
        "created_at": "2019-09-27T20:33:29.000Z",
        "parent_ids": [
            "27d5523381def1bba4f9634a09649e7e97b6bc98",
            "41bbae0ce533e26a40c504b77f86937a100bd2cf"
        ],
        "title": "Merge branch 'ad/initialize_react_app' into 'master'",
        "message": "Merge branch 'ad/initialize_react_app' into 'master'\n\nAd/initialize react app\n\nSee merge request Alexistower/foodcravings!2",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-09-27T20:33:29.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-09-27T20:33:29.000Z"
    },
    {
        "id": "41bbae0ce533e26a40c504b77f86937a100bd2cf",
        "short_id": "41bbae0c",
        "created_at": "2019-09-27T20:33:29.000Z",
        "parent_ids": [
            "27d5523381def1bba4f9634a09649e7e97b6bc98"
        ],
        "title": "feat: react app",
        "message": "feat: react app\n\nfeat: added storybook\n\nfeat: added bootstrap\n\nfeat: added react-router\n",
        "author_name": "Aaron Dewitt",
        "author_email": "arkdengal@gmail.com",
        "authored_date": "2019-09-27T20:33:29.000Z",
        "committer_name": "Aaron Dewitt",
        "committer_email": "arkdengal@gmail.com",
        "committed_date": "2019-09-27T20:33:29.000Z"
    },
    {
        "id": "27d5523381def1bba4f9634a09649e7e97b6bc98",
        "short_id": "27d55233",
        "created_at": "2019-09-25T02:19:11.000Z",
        "parent_ids": [],
        "title": "Initial commit",
        "message": "Initial commit",
        "author_name": "Alexis Torres",
        "author_email": "alexis.e.torres@utexas.edu",
        "authored_date": "2019-09-25T02:19:11.000Z",
        "committer_name": "Alexis Torres",
        "committer_email": "alexis.e.torres@utexas.edu",
        "committed_date": "2019-09-25T02:19:11.000Z"
    }
];
