import React from 'react';
import './app.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../../index.css';

import {createBrowserHistory} from 'history';
// components
import Header from '../../components/header'
import Splash from "../splash/splash";
import Grocery from "../grocery/grocery";
import Recipe from "../recipe/recipe";
import About from "../about/about";
import Model from "../model/model";
import Restaurant from "../restaurant/restaurant";
import NutritionalVisual from "../visualizations/menut/nutritionVisual";
import Search from "../search/search";
import GroceryNetworkGraph from '../visualizations/freqgroc/grocNetworkGraph'
import CuisineGraph from "../visualizations/cuisinecost/CuisineCost";
import CityBarChart from "../visualizations/devVisuals/CityBarChart"
import CitMap from "../visualizations/map/map";

import RoutePieChart from "../visualizations/routespiechart/RoutePieChart";

function App() {
    let history = createBrowserHistory();
    return (
        <Router>
            <div className="App">
                <Header history={history}/>
                <Switch>
                    <Route exact path='/' component={Splash}/>
                    <Route exact path='/groceries/:id' component={Grocery}/>
                    <Route exact path='/recipes/:id' component={Recipe}/>
                    <Route exact path='/Restaurant/:id' component={Restaurant}/>
                    <Switch>
                        <Route exact path='/About' component={About}/>
                        <Route exact path='/grocerynetwork' component={GroceryNetworkGraph}/>
                        <Route exact path='/menunutviz' component={NutritionalVisual}/>
                        <Route exact path='/cityscore' component={CityBarChart}/>
                        <Route exact path='/cuisinecost' component={CuisineGraph}/>
                        <Route exact path='/routespiechart' component={RoutePieChart}/>
                        <Route exact path='/cityrate' component={CitMap}/>
                        <Route exact path='/:pageName' component={Model}/>
                        <Route exact path='/search/:searchTerm' component={Search}/>
                    </Switch>
                </Switch>
            </div>
        </Router>
    );
}

export default App;
