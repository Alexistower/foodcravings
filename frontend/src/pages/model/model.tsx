import React, {Component} from 'react';
import {RouteComponentProps} from 'react-router';
import {Fields, fieldTable} from "../../static_data/sourcefields";
import {Utilities} from "../../utilities";
import Axios, {AxiosResponse} from "axios";
import {APIResponse} from "../../models";
import {Pagination} from "react-bootstrap";
import "./model.css";
import AnimatedList from "./animation";
import Search from "../../components/search";

interface Props extends RouteComponentProps {
}

interface State {
    expanded: boolean;
    pageName: string;
    data: any;
    fields: Fields;
    field_list: string[];
    page: number;
    perPage: number;
    lastPage: number;
    done: boolean;
    filters: string
    sort: string
    searchTerms: string[];
}

export default class Model extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        const name: string = (props.match.params as any).pageName;
        const fieldtable = fieldTable[name];
        this.state = {
            expanded: false,
            pageName: name,
            page: 1,
            perPage: 9,
            data: {},
            lastPage: 0,
            fields: fieldtable,
            field_list: Object.values(fieldtable.details),
            done: false,
            filters: "",
            sort: "name",
            searchTerms: [""]
        } as State;
        this.getData(1, this.state.perPage, name).then((r: AxiosResponse) => {
            this.setState({
                data: r.data,
                lastPage: Math.ceil(r.data.meta.total / this.state.perPage),
                done: true
            } as State);
        });

        this.handleFilter = this.handleFilter.bind(this);
    }

    handleFilter(passed_filters: string, sort: string, vals: string[]) {
        // Maybe make async later
        this.setState({
            filters: passed_filters,
            page: 1,
            done: false,
            sort: sort,
            searchTerms: vals
        })
    }

    getData(page: number, perPage: number = this.state.perPage,
            name: string = this.state.pageName, filters: String = "", sort: String = "name") {
        let parameters = {'page[number]': page, 'page[size]': perPage, 'sort': sort};

        if (filters !== "") {
            parameters["filter[objects]"] = filters
        }
        let temp = `https://api.foodcravings.net/api/${name}`;
        return Axios.get(temp, {params: parameters});
    }

    setPage(page: number) {  // Doesn't reset page when 
        this.getData(page, this.state.perPage, this.state.pageName, this.state.filters, this.state.sort).then((r: AxiosResponse) => {
            this.setState({
                page: page,
                data: r.data as APIResponse[],
            } as State);
        });
    }

    shouldComponentUpdate(nextProps: Readonly<Props>, nextState: Readonly<State>, nextContext: any): boolean {
        return (this.props.match.params as any).pageName !== (nextProps.match.params as any).pageName ||
            this.state.done !== nextState.done || this.state.page !== nextState.page || this.state.filters !== nextState.filters
    }

    componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any): void {
        const name = (this.props.match.params as any).pageName;
        this.getData(this.state.page, this.state.perPage, name, this.state.filters, this.state.sort).then((r: AxiosResponse) => {
            this.setState({
                pageName: name,
                lastPage: Math.ceil(r.data.meta.total / this.state.perPage),
                data: r.data as APIResponse[],
                fields: fieldTable[name],
                done: true
            } as State);
        });
    }

    parseField(str: string, obj: object): string {
        const field = Utilities.arrayStringEval(str, obj);
        if (field instanceof Array) {
            return field.join(', ');
        } else {
            return field;
        }
    }

    render() {
        if (!this.state.done) {
            return (<div><h1>Loading</h1></div>)
        }
        return (
            <div>
                <Search simple={false} fields={this.state.field_list} filterAction={this.handleFilter}/>
                <div className={"group"}>

                    {this.state.data.data.map((source: APIResponse) => this.card(source)
                    )}
                    <div className="paginate">
                        {this.pagination()}
                    </div>
                </div>
            </div>


        )
    }

    pagination() {
        const p = this.state.page;
        const m = this.state.lastPage;
        return (
            <Pagination className="paginate">
                <Pagination.First onClick={() => this.setPage(1)}/>
                {p > 1 ? <Pagination.Prev onClick={() => this.setPage(p - 1)}/> : ''}
                {p > 2 ? <Pagination.Ellipsis/> : ''}
                {p > 1 ? <Pagination.Item onClick={() => this.setPage(p - 1)}>{p - 1}</Pagination.Item> : ''}
                <Pagination.Item active>{p}</Pagination.Item>
                {p < m ? <Pagination.Item onClick={() => this.setPage(p + 1)}>{p + 1}</Pagination.Item> : ''}
                {p < m - 1 ? <Pagination.Ellipsis/> : ''}
                {p < m ? <Pagination.Next onClick={() => this.setPage(p + 1)}/> : ''}
                <Pagination.Last onClick={() => this.setPage(m)}/>
            </Pagination>
        );
    };

    card(source: APIResponse) {
        let {attributes, id} = source;
        console.log(attributes);
        let name = this.parseField(this.state.fields.details["name"], attributes);
        let img = this.parseField(this.state.fields.img, attributes);
        let fields: string[] = [];
        Object.keys(this.state.fields.details).forEach((detail: string) => {
            if (detail !== "name") {
                let field: any = this.parseField(this.state.fields.details[detail], attributes);
                fields.push(detail + ': ' + field)
            }
        });


        return (
            <AnimatedList name={name}
                          img={img}
                          fields={fields}
                          link={`/${this.state.pageName}/${id}`}
                          search={this.state.searchTerms}>
            </AnimatedList> as any
        ) as any;
    }
}