import React from 'react';
import Model from './model'
import {createLocation, createMemoryHistory} from 'history'
import axios from 'axios'
import {mount} from 'enzyme';

let MockAdapter = require('axios-mock-adapter');
let mock = new MockAdapter(axios);
describe("model component", () => {
    const history = createMemoryHistory();

    test("renders without crashing", () => {
        const match = {params: {pageName: 'groceries'}, isExact: true, path: "/:pageName", url: ""};
        const location = createLocation(match.url);
        const model = mount(
            <Model match={match} history={history} location={location}/>
        );
        expect(model.instance()).toBeDefined();
    });
    // test("link changes location", () =>{
    //     mock.onGet("https://api.foodcravings.net/groceries", {params: {'page[size]': 6, 'page[number]': 1}}).reply(200, {data:[{brand: "", category: "Home Page/Food/Condiments, Sauces & Spices/Sauces", created_at: "2019-10-16T13:53:38.959692", image_url: "https://i5.walmartimages.com/asr/65887ef9-dbfe-44a…265f17cd3_1.971897322f3b9694b2d166ad3214ee01.jpeg", name: "(3 Pack) Ricos Gourmet Nacho Cheddar Cheese Sauce, 15.0 OZ"}]});
    //
    //     const match = {params: {pageName: 'groceries'}, isExact: true, path: "/:pageName", url: ""};
    //     let location = createLocation(match.url);
    //     const model = mount(
    //         <Model match={match} history={history} location={location} />
    //     );
    //     model.instance().setPage(1);
    //     model.update();
    //     console.log(model.instance().state);
    //
    // });

});
