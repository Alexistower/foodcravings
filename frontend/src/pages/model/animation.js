import React, {Component} from "react";
import {Flipped, Flipper} from "react-flip-toolkit";

import "./model.css";
import {Image} from "react-bootstrap";
import {Link} from "react-router-dom";
import {Utilities} from "../../utilities";
import Highlighter from "../../components/highlighter"

//This class is a modified version of a file from this repo:
//https://github.com/aholachek/react-flip-toolkit#basic-props
//Credit where credit is due


class AnimatedList extends Component {
    constructor(props) {
        super(props);
        const createCardFlipId = index => `listItem-${index}`;
        const shouldFlip = index => (prev, current) =>
            index === prev || index === current;
        this.state = {
            listData: [...Array(1).keys()],
            ListItem: ({index, onClick}) => {
                return (
                    <Flipped
                        flipId={createCardFlipId(index)}
                        stagger="card"
                        shouldInvert={shouldFlip(index)}
                    >
                        <div className="listItem" onClick={() => onClick(index)}>
                            <Flipped inverseFlipId={createCardFlipId(index)}>
                                <div className="listItemContent">
                                    <Flipped
                                        flipId={`avatar-${index}`}
                                        stagger="card-content"
                                        shouldFlip={shouldFlip(index)}
                                        delayUntil={createCardFlipId(index)}
                                    >
                                        <div><Image src={this.props.img} className="avatar"/></div>

                                    </Flipped>
                                    <div className="description">

                                        <Flipped
                                            flipId={`description-${index}-$name`}
                                            stagger="card-content"
                                            shouldFlip={shouldFlip(index)}
                                            delayUntil={createCardFlipId(index)}
                                        >
                                            <Link to={this.props.link}>
                                                <div>
                                                    <Highlighter text={this.props.name} search={this.props.search}/>
                                                </div>
                                            </Link>
                                        </Flipped>

                                    </div>
                                </div>
                            </Flipped>
                        </div>
                    </Flipped>
                );
            },
            ExpandedListItem: ({index, onClick}) => {
                return (
                    <Flipped
                        className="span"
                        flipId={createCardFlipId(index)}
                        stagger="card"
                        onStart={el => {
                            setTimeout(() => {
                                el.classList.add("animated-in");
                            }, 400);
                        }}
                    >
                        <div className="expandedListItem" onClick={() => onClick(index)}>
                            <Flipped inverseFlipId={createCardFlipId(index)}>
                                <div className="expandedListItemContent">
                                    <Flipped
                                        flipId={`avatar-${index}`}
                                        stagger="card-content"
                                        delayUntil={createCardFlipId(index)}
                                    >
                                        <div>
                                            <Image src={this.props.img} className="avatarExpanded"/>
                                        </div>
                                    </Flipped>
                                    <Link to={this.props.link}>
                                        <div className="description">
                                            <Highlighter search={this.props.search} text={this.props.name}/>
                                        </div>
                                    </Link>
                                    <div className="additional-content">
                                        {Object.keys(this.props.fields).map(field => {
                                                return (
                                                    <div className="additional-content">
                                                        <Flipped
                                                            flipId={`description-${index}-${field}`}
                                                            stagger="card-content"
                                                            delayUntil={createCardFlipId(index)}>
                                                            <div className="additional-content">
                                                                <Highlighter text={this.props.fields[field]}
                                                                             search={this.props.search}/>
                                                            </div>
                                                        </Flipped>
                                                    </div>
                                                )
                                            }
                                        )}
                                    </div>
                                </div>
                            </Flipped>
                        </div>
                    </Flipped>
                );
            }
        }


    }

    parseField(str, obj) {
        const field = Utilities.arrayStringEval(str, obj);
        if (field instanceof Array) {
            return ''.concat(...field);
        } else {
            return field;
        }
    }

    state = {focused: null};
    onClick = index =>
        this.setState({
            focused: this.state.focused === index ? null : index

        });

    render() {
        const ListItem = this.state.ListItem;
        const ExpandedListItem = this.state.ExpandedListItem;

        return (
            <Flipper
                flipKey={this.state.focused}
                className={"staggered-list-content" + (this.state.focused === 0 ? "Expanded" : "")}
                spring="gentle"
                staggerConfig={{
                    card: {
                        reverse: this.state.focused !== null
                    }
                }}
                decisionData={this.state.focused}>

                {this.state.listData.map(index => {
                    return (
                        <div key={index}>
                            {index === this.state.focused ? (
                                <ExpandedListItem
                                    index={this.state.focused}
                                    onClick={this.onClick}
                                />
                            ) : (
                                <ListItem index={index} key={index} onClick={this.onClick}/>
                            )}
                        </div>
                    );
                })}

            </Flipper>
        );
    }
}

export default AnimatedList

