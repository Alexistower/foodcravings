import React, {Component} from 'react';
import {Container, Image, Jumbotron, Table} from 'react-bootstrap/'
import axios from 'axios';
import {OverlayTrigger} from "react-bootstrap";
import Tooltip from "react-bootstrap/Tooltip";
import Button from "react-bootstrap/Button";

const ingredient_url_prefix = "https://api.foodcravings.net/api/ingredients/";
const restaurant_url_prefix = "https://api.foodcravings.net/api/restaurant/";
const menu_item_url_prefix = "https://api.foodcravings.net/api/menu_item/";
const recipe_prefix = "https://api.foodcravings.net/api/recipes/";
const grocery_prefix = "https://api.foodcravings.net/api/groceries/";

export default class Restaurant extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            menu_items: {},
            groceries: {},
            id: props.match.params.id,
            address: "",
            image_url: "",
            name: "",
            phone: "",
            priceRange: "",
            done: false
        };
    }

    componentDidMount() {
        this.setUpThis(this.state.id).then(() => {
                this.setUpMenuItems()
            }
        )
    }

    setUpThis(id) {
        return axios.get(restaurant_url_prefix + id).then(
            response => {
                this.setState({data: response.data.data})
            }
        );
    }

    async setUpMenuItems() {
        // Get array of menu items
        const menu_items = this.state.data.relationships.menu_items.data;
        for (let i = 0; i < menu_items.length; i++) {
            let menuItems = this.state.menu_items;
            const menu_resp = await axios.get(menu_item_url_prefix + menu_items[i].id);
            const menu_data = menu_resp.data.data;
            const menu_attr = { // set up menu for display
                name: menu_data.attributes.name,
                description: menu_data.attributes.description || "No description available",
                price: menu_data.attributes.price_string || "Not available"
            };
            let related_rec = {};
            let groceries = {};
            if (menu_data.relationships.recipes.data.length) {
                // Get the first related recipe info
                await axios.get(recipe_prefix + menu_data.relationships.recipes.data[0].id).then(recipe_resp => {
                    const rdata = recipe_resp.data.data;
                    related_rec = {name: rdata.attributes.name, id: rdata.id};
                    // Iterate through recipe ingredients to get three groceries
                    return rdata.relationships.ingredients.data[0].id;

                }).then((ingred_id) => this.getGroceries(ingred_id).then((resp) => groceries = resp));


            }
            const id = menu_data.id;
            menuItems[id] = {"menu_attributes": menu_attr, "related_recipe": related_rec, "related_groc": groceries};
            this.setState({menu_items: menuItems, done: true});
        }

    }

    async getGroceries(id) {
        const ingr = await axios.get(ingredient_url_prefix + id);
        const rel = ingr.data.data.relationships.grocery_items.data;
        if (rel.length) {
            const grocery = await axios.get(grocery_prefix + rel[0].id);
            const groc_data = grocery.data.data;
            return {name: groc_data.attributes.name, id: groc_data.id};
        }
        return {name: "", id: 101}
    }

    render() {

        if (!this.state.done) {
            return (<div><h1>Loading</h1></div>)
        }
        let curr = this.state.data;
        let img_url = curr.attributes.image_url;
        let name = curr.attributes.name;
        let address = curr.attributes.name;
        let phone = curr.attributes.phone;
        let price = curr.attributes.priceRange;
        let data = this.state.menu_items;
        let cuisines = curr.attributes.cuisines.join(', ');
        let priceMap = {'$': '~$5', '$$': '~$10', '$$$': '~$15', '$$$$': '~$20',};
        return (
            <div>
                <Jumbotron>
                    <Container>
                        <h1 className="title">{name} </h1>
                        <Image src={img_url} alt="new" fluid/>
                    </Container>
                </Jumbotron>
                <h3>Restaurant Details</h3>
                <p>
                    <strong>Address: </strong> {address}
                </p>
                <p>
                    <strong>Telephone: </strong> {phone}
                </p>
                <p>
                    <strong>Cuisines: </strong> {cuisines}
                </p>
                <strong>Price Range: </strong>
                <OverlayTrigger
                    placement="right"
                    overlay={
                        <Tooltip><strong>{priceMap[price]}</strong></Tooltip>}>
                    <Button variant="success">
                        <strong>{price}</strong>
                    </Button>
                </OverlayTrigger>


                <h3>Menu</h3>
                <Table striped bordered hover size="md">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Related Recipes</th>
                        <th>Related Grocery Items</th>
                    </tr>
                    </thead>
                    <tbody>
                    {Object.keys(data).map((element) => {
                        return (
                            <React.Fragment>
                                {<tr key={element}>
                                    <td>{data[element].menu_attributes.name}</td>
                                    <td>{data[element].menu_attributes.description}</td>
                                    <td>{data[element].menu_attributes.price}</td>

                                    <td><a
                                        href={`/recipes/${data[element].related_recipe.id}`}> {data[element].related_recipe.name}</a>
                                    </td>
                                    <td><a
                                        href={`/groceries/${data[element].related_groc.id}`}>{data[element].related_groc.name}</a>
                                    </td>

                                </tr>}
                            </React.Fragment>
                        )

                    })}
                    </tbody>
                </Table>
            </div>


        );
    }
}
