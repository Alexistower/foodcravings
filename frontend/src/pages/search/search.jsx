import React, {Component} from 'react';
import {Container, Pagination} from 'react-bootstrap/'
import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import {fieldTable} from "../../static_data/sourcefields";
import {Utilities} from "../../utilities";
import {Link} from "react-router-dom";
import Axios from "axios";
import "./search.css";
import Highlighter from "../../components/highlighter";


class Model extends Component {

    constructor(props) {
        super(props);
        let name = props.name;
        let searchTerm = props.searchTerm;
        let perPage = props.perPage;
        this.state = {
            pageName: name,
            searchTerm: searchTerm,
            page: 1,
            perPage: perPage,
            data: {},
            lastPage: 0,
            fields: fieldTable[name],
            done: false
        };
        this.getData(1, name).then((r) => {
            this.setState({
                data: r.data,
                lastPage: Math.ceil(r.data.meta.total / this.state.perPage),
                done: true
            });
        });
    }

    getData(page, name = this.state.pageName, perPage = this.state.perPage, term = this.state.searchTerm) {
        const args = term.split(" ");
        let filters = [];
        for (let a in args) {
            filters.push({name: "name", op: "ilike", val: "%" + args[a] + "%"});
        }
        filters = JSON.stringify(filters);
        // "[{\"name\":\"name\",\"op\":\"ilike\",\"val\":\"%" + term + "%\"}]"
        let parameters = {'page[number]': page, 'page[size]': perPage, "filter[objects]": filters};
        return Axios.get(`https://api.foodcravings.net/api/${name}`, {params: parameters})
    }

    setPage(page) {
        this.getData(page).then((r) => {
            this.setState({
                page: page,
                data: r.data,
            });
        });
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return (this.props).pageName !== (nextProps).pageName ||
            this.state.done !== nextState.done || this.state.page !== nextState.page
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.getData(this.state.page).then((r) => {
            this.setState({
                data: r.data,
                fields: fieldTable[this.state.pageName]
            });
        });
    }

    parseField(str, obj) {
        const field = Utilities.arrayStringEval(str, obj);
        if (field instanceof Array) {
            return ''.concat(...field);
        } else {
            return field;
        }
    }

    render() {
        if (!this.state.done) {
            return (<div><h1>Loading</h1></div>)
        }
        if (this.state.data.data.length === 0) {
            return (<div><h4>No results found</h4></div>)
        }
        return (
            <Container>
                <CardDeck>
                    {this.state.data.data.map((source) => this.card(source))}
                </CardDeck>
                <br/>
                {this.pagination()}
            </Container>
        )
    }

    pagination() {
        const p = this.state.page;
        const m = this.state.lastPage;
        return (
            <Pagination>
                <Pagination.First onClick={() => this.setPage(1)}/>
                {p > 1 ? <Pagination.Prev onClick={() => this.setPage(p - 1)}/> : ''}
                {p > 2 ? <Pagination.Ellipsis/> : ''}
                {p > 1 ? <Pagination.Item onClick={() => this.setPage(p - 1)}>{p - 1}</Pagination.Item> : ''}
                <Pagination.Item active>{p}</Pagination.Item>
                {p < m ? <Pagination.Item onClick={() => this.setPage(p + 1)}>{p + 1}</Pagination.Item> : ''}
                {p < m - 1 ? <Pagination.Ellipsis/> : ''}
                {p < m ? <Pagination.Next onClick={() => this.setPage(p + 1)}/> : ''}
                <Pagination.Last onClick={() => this.setPage(m)}/>
            </Pagination>
        );
    };

    card(source) {
        let {attributes, id} = source;
        return (
            <Link to={`/${this.state.pageName}/${id}`} className="carLink">
                <span data-testid="linky"/>
                <Card className="carCard">
                    <Card.Body>
                        {(attributes).image_url !== '' &&
                        <Card.Img variant="top" className={"carImg"}
                                  src={this.parseField(this.state.fields.img, attributes)}/>}
                        <Card.Title>
                            <Highlighter search={[this.state.searchTerm]}
                                         text={this.parseField(this.state.fields.details["name"], attributes)}></Highlighter>
                        </Card.Title>
                        {Object.keys(this.state.fields.details).map((field) => {
                            if (field !== "name") {
                                return (
                                    <Card.Text>
                                        <Highlighter search={[this.state.searchTerm]}
                                                     text={field + ': ' + this.parseField(this.state.fields.details[field], attributes)}></Highlighter>
                                    </Card.Text>);

                            }
                            return '';
                        })}
                    </Card.Body>
                </Card>
            </Link>
        );
    }
}

export default class Search extends Component {
    render() {
        let term = this.props.match.params.searchTerm;
        return (
            <div>
                <h1>Search Results</h1>
                <div>
                    <h3>Restaurants</h3>
                    <Model name={"restaurant"} searchTerm={term} perPage={3}/>
                </div>
                <div>
                    <h3>Recipes</h3>
                    <Model name={"recipes"} searchTerm={term} perPage={3}/>
                </div>
                <div>
                    <h3>Groceries</h3>
                    <Model name={"groceries"} searchTerm={term} perPage={3}/>
                </div>
            </div>

        );
    }
}

