import React, { Component } from 'react';
import {Col, Container, Image, Jumbotron, Row, Table} from 'react-bootstrap/'
import axios from 'axios';
import RelationshipList from "../../components/relationships";

const ingredient_url_prefix = "https://api.foodcravings.net/api/ingredients/";
const restaurant_url_prefix = "https://api.foodcravings.net/api/restaurant/";
const menu_item_url_prefix = "https://api.foodcravings.net/api/menu_item/";
const recipe_prefix = "https://api.foodcravings.net/api/recipes/";
const grocery_prefix = "https://api.foodcravings.net/api/groceries/";
const nutrition_label_prefix = "https://api.foodcravings.net/api/nutrition_label/";
const groceryLabelUnit = {"calcium": "mg", "calories": "Kcal", "carbohydrates": "g", "cholesterol": "mg", "fat": "g", "fiber": "g", "iron": "mg", "protein": "g", "saturatedFat": "g", "sodium": "mg", "sugars": "g", "transFat": "g"};

const rest_model_prefix = "/restaurant/";
const rec_model_prefix = "/recipes/";

export default class Grocery extends Component{
    constructor(props) {
        super(props);
        this.state = {
            data: 1,
            id: (props.match.params ).id,
            ingredients: [],
            restaurantRel : {},
            recipeRel : {},
            groceryLabel: {
            "calcium": 0,
            "calories": 0,
            "carbohydrates": 0,
            "cholesterol": 0,
            "fat": 0,
            "fiber": 0,
            "iron": 0,
            "protein": 0,
            "saturatedFat": 0,
            "sodium": 0,
            "sugars": 0,
            "transFat": 0
            },
            done: false
        };

        this.setUpThis(this.state.id).then(async () => {
           await this.setupIngredientsandRecipe();
            this.setupLabel();

        }
            ).then(() => {
                this.setState({done: true});
        });
    }
    setupLabel() {
        let tempLabel = {};
        axios.get(nutrition_label_prefix+this.state.data.data.relationships.label.data.id).then(
            response => {
                for (let key in this.state.groceryLabel) {
                    if(key in response.data.data.attributes && key in groceryLabelUnit) {
                        tempLabel[key] = (response.data.data.attributes[key].toFixed(2)).toString() + " " + groceryLabelUnit[key];
                    }
                }
            }
        ).then(() => this.setState({groceryLabel:tempLabel}));
    }

    async setupIngredientsandRecipe() {
        this.state.data.data.relationships.ingredients.data.map(related =>
                axios.get(ingredient_url_prefix+related.id).then( async response => {
                    this.state.ingredients.push(response.data);
                    let id = response.data.data.relationships.recipe.data.id
                       await axios.get(recipe_prefix + id).then( recipe_rep => {
                           let temp = this.state.recipeRel;
                            temp[response.data.data.id] = {
                                "name": recipe_rep.data.data.attributes.name,
                                "url": rec_model_prefix + recipe_rep.data.data.id,
                                "image_url": recipe_rep.data.data.attributes.image_url
                            };
                           axios.get(menu_item_url_prefix + recipe_rep.data.data.relationships.menu_items.data[0].id).then(menu_resp => {
                               let temp2 = this.state.restaurantRel;
                               axios.get(restaurant_url_prefix + menu_resp.data.data.relationships.restaurant.data.id).then(response => {
                                   temp2[response.data.data.id] = {
                                       "name": response.data.data.attributes.name,
                                       "url": rest_model_prefix + response.data.data.id,
                                       "image_url": response.data.data.attributes.image_url
                                   };
                               }).then(() => this.setState({restaurantRel: temp2}))
                           this.setState({recipeRel:temp})
                        });
                       }
            );
    }))
    }

    setUpThis(id)
    {
        return axios.get(grocery_prefix+id).then(
            response => {
                this.setState({data: response.data});
            }
        );
    }

        render(){
            if(!this.state.done) {
                return <h1>Loading</h1>
            }

            let curr= this.state.data;
            let img;
            let groceryLabel = this.state.groceryLabel;
            let restaurantRel = this.state.restaurantRel;
            let recipeRel = this.state.recipeRel;
            if (curr.data.attributes.image_url) {
            img = <Image src={curr.data.attributes.image_url} alt="new" fluid/>;
            }


        return (
            <div>
            <Jumbotron>
                <Container>
                <h1 className="title">{curr.data.attributes.name} </h1>
                    {img}
                </Container>
            </Jumbotron>
                <Container>
                    <Row>
                        <Col>
                <h3>Item Details</h3>
                <p>
                    <strong>Price:</strong> ${curr.data.attributes.price}
                </p>
                <p>
                    <strong>Brand:</strong> {curr.data.attributes.brand}
                </p>
                <p>
                    <strong>Category:</strong> {curr.data.attributes.category}
                </p>

                <h3>Recipes Using this Item:</h3>
                <RelationshipList relatedItems={Object.values(recipeRel)}/>


                <p></p>
                <p></p>
                <p></p>
                <h3>Popular Restaurants Using this Item:</h3>
                <RelationshipList relatedItems={Object.values(restaurantRel)}/>
                        </Col>
                        <Col>
                        <h3>Primary Nutritional Label</h3>
                        <p>
                            <strong>Serving Size:</strong> {curr.data.attributes.serving_size}
                        </p>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>Label</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>Calories</td>
                                <td>{groceryLabel["calories"]? groceryLabel["calories"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Fat</td>
                                <td>{groceryLabel["fat"]? groceryLabel["fat"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Saturated Fat</td>
                                <td>{groceryLabel["saturatedFat"]? groceryLabel["saturatedFat"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Trans Fat</td>
                                <td>{groceryLabel["transFat"]? groceryLabel["transFat"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Cholesterol</td>
                                <td>{groceryLabel["cholesterol"]? groceryLabel["cholesterol"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Sodium</td>
                                <td>{groceryLabel["sodium"]? groceryLabel["sodium"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Carbohydrates</td>
                                <td>{groceryLabel["carbohydrates"]? groceryLabel["carbohydrates"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Fiber</td>
                                <td>{groceryLabel["fiber"]? groceryLabel["fiber"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Sugars</td>
                                <td>{groceryLabel["sugars"]? groceryLabel["sugars"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Protein</td>
                                <td>{groceryLabel["protein"]? groceryLabel["protein"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Calcium</td>
                                <td>{groceryLabel["calcium"]? groceryLabel["calcium"]:'N/A'}</td>
                            </tr>
                            <tr>
                                <td>Iron</td>
                                <td>{groceryLabel["iron"]? groceryLabel["iron"]:'N/A'}</td>
                            </tr>
                            </tbody>
                        </Table>
                        </Col>

                    </Row>
                </Container>
            </div>

        );
    }
}

