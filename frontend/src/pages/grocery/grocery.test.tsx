import React from 'react';
import {createLocation, createMemoryHistory} from 'history'

describe("grocery component", () => {
    const history = createMemoryHistory();
    const match = {params: {id: 1}, isExact: true, path: "/grocery/:id", url: ""};
    const location = createLocation(match.url);

    test("renders without crashing", async () => {
        // const grocery = await render(<Grocery match={match} history={history} location={location} />);
        // expect(grocery.container).toBeDefined();
    });

    test("renders fragment", async () => {
        // const grocery = await render(<Grocery match={match} history={history} location={location} />);
        // expect(grocery.findByLabelText('React.Fragment')).toBeDefined();
    });
});
