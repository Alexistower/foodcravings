import React, {Component} from 'react';
import {Carousel} from 'react-bootstrap';


class Splash extends Component {
    render() {
        return (
            <div className="container-fluid" style={{backgroundColor: '#faf9f7', width: "100%", height: "100%"}}>
                <table style={{width: "100%", height: "100%"}}>
                    <tr>
                        <td align="left" style={{verticalAlign: "middle", paddingTop: "2%", paddingBottom: "2%"}}>
                            <h1 text-align="left" style={{color: '#535c27', width: "100%", fontFamily: "Montserrat"}}>
                                Welcome to FoodCravings!
                            </h1>
                            <h5 text-align="left" style={{color: '#859c41', fontFamily: "Open Sans"}}>
                                Our mission is to give you options when you are craving some food, whether it be a
                                snack, or a full meal.
                                We believe that you should be able to decide not only what you eat, but also where you
                                eat, depending
                                on factors like price range or specific ingredients.
                            </h5>
                        </td>
                        <td align="right" style={{paddingTop: "2%", paddingBottom: "2%"}}>
                            <Carousel interval={3500} style={{width: 900, height: 600, marginRight: ".5%"}}>
                                <Carousel.Item>
                                    <img width="900" height="600"
                                         src={require("../../Assets/IceCream.jpg")}
                                         alt="First slide"
                                    />
                                    <Carousel.Caption>
                                        <h3 style={{color: '#f0eeeb'}}>#SweetTooth</h3>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img width="900" height="600"

                                         src={require("../../Assets/Pizza.jpg")}
                                         alt="Third slide"
                                    />
                                    <Carousel.Caption>
                                        <h3 style={{color: '#f0eeeb'}}>#PizzaTooth</h3>
                                    </Carousel.Caption>
                                </Carousel.Item>
                                <Carousel.Item>
                                    <img width="900" height="600"
                                         src={require("../../Assets/Sandwhich.jpg")}
                                         alt="Third slide"
                                    />
                                    <Carousel.Caption>
                                        <h3 style={{color: '#f0eeeb'}}>#WichTooth</h3>
                                    </Carousel.Caption>
                                </Carousel.Item>

                            </Carousel>
                        </td>

                    </tr>
                </table>


            </div>
        );
    }
}

export default Splash;
