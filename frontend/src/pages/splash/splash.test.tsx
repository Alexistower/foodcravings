import React from 'react';
import ReactDOM from 'react-dom';
import Splash from './splash';

describe("app component", () => {
    test('renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<Splash/>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});

