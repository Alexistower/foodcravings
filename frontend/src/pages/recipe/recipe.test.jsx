import React from 'react';
import Recipe from './recipe'
import {render} from '@testing-library/react';


describe("Recipe component", () => {
    const match = {params: {id: 253}, isExact: true, path: "/recipes/:id", url: ""};
    //  const inst = recipe.getInstance();
    // const inst = recipe.getInstance();


    test("renders without crashing", async () => {
        const recipe = await render(<Recipe match={match}/>);
        expect(recipe.container).toBeDefined();
        // console.log(recipe)
    });

    // test("setup state data" , () =>{
    //     const mock = new MockAdapter(axios);
    //     mock.onGet("https://api.foodcravings.net/api/recipes/253").reply(200, recipeDat);
    //     mock.onGet("https://api.foodcravings.net/api/ingredients/2323").reply(200, ingr_data1);
    //     mock.onGet("https://api.foodcravings.net/api/ingredients/2324").reply(200, ingr_data2);
    //     mock.onGet("https://api.foodcravings.net/api/groceries/335").reply(200, groc_data2);
    //     const match = {params: {}};
    //     const recipe = create(<Recipe match={match}/>);
    //     const inst = recipe.getInstance();
    //     inst.setupThis(253).then(() =>
    //         inst.setupIngredientsandGrocs().then(result => {
    //             expect(inst.state.ingredients).toEqual(ingredient_data);
    //             expect(inst.state.groceryRel).toEqual(grocery_data);
    //         }))
    //     // inst.setupThis(253).then(result => {
    //     //     console.log(inst.state.data);
    //     //     console.log(recipeDat);
    //     //     expect(inst.state.data).toEqual(recipeDat);
    //     // })
    //
    // });

    // test("setup ingredients and grocery data" , () =>{
    //     const mock = new MockAdapter(axios);
    //     mock.onGet("https://api.foodcravings.net/api/recipes/253").reply(200, recipeDat);
    //     mock.onGet("https://api.foodcravings.net/api/ingredients/2323").reply(200, ingr_data1);
    //     mock.onGet("https://api.foodcravings.net/api/ingredients/2324").reply(200, ingr_data2);
    //     mock.onGet("https://api.foodcravings.net/api/groceries/335").reply(200, groc_data2);
    //     const match = {params: {}};
    //     const recipe = create(<Recipe match={match}/>);
    //     const inst = recipe.getInstance();
    //     inst.setupThis(253).then((result) => {
    //     expect(inst.state.data).toEqual(recipeDat);
    //     inst.setupIngredientsandGrocs().then(result => {
    //         expect(inst.state.ingredients).toEqual(ingredient_data);
    //         expect(inst.state.groceryRel).toEqual(grocery_data);
    //     })})
    // });
});
