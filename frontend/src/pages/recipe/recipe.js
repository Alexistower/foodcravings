import React, {Component} from 'react';
import {Col, Container, Image, Jumbotron, Row, Table} from 'react-bootstrap/'
import axios from "axios";
import RelationshipList from "../../components/relationships";

const ingredient_url_prefix = "https://api.foodcravings.net/api/ingredients/";
const restaurant_url_prefix = "https://api.foodcravings.net/api/restaurant/";
const menu_item_url_prefix = "https://api.foodcravings.net/api/menu_item/";
const recipe_prefix = "https://api.foodcravings.net/api/recipes/";
const grocery_prefix = "https://api.foodcravings.net/api/groceries/";

const recipe_label_prefix = "https://api.foodcravings.net/api/recipe_label/";
const recipeLabelUnit = {
    "calcium": "mg",
    "calories": "Kcal",
    "carbohydrates": "g",
    "cholesterol": "mg",
    "fat": "g",
    "fiber": "g",
    "iron": "mg",
    "protein": "g",
    "saturatedFat": "g",
    "sodium": "mg",
    "sugars": "g",
    "transFat": "g"
};
const recipeLabelName = {
    "calcium": "Calcium",
    "calories": "Calories",
    "carbohydrates": "Carbohydrates",
    "cholesterol": "Cholesterol",
    "fat": "Fat",
    "fiber": "Fiber",
    "iron": "Iron",
    "protein": "Protein",
    "saturatedFat": "Saturated Fat",
    "sodium": "Sodium",
    "sugars": "Sugars",
    "transFat": "Trans Fat"
};

const groc_model_prefix = "/groceries/";
const rest_model_prefix = "/restaurant/";


export default class Recipe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: undefined,
            id: (props.match.params).id,
            ingredients: [],
            groceryRel: {},
            restaurantRel: {},
            recipeLabel: {
                "calcium": 0,
                "calories": 0,
                "carbohydrates": 0,
                "cholesterol": 0,
                "fat": 0,
                "fiber": 0,
                "iron": 0,
                "protein": 0,
                "saturatedFat": 0,
                "sodium": 0,
                "sugars": 0,
                "transFat": 0
            },
            done: false
        };
        this.setupThis(this.state.id).then(() => {
                this.setupIngredientsandGrocs();
                this.setupLabel();
                this.setupRestaurants();
            }
        ).then(() => {
            this.setState({done: true});
        });
    }

    setupRestaurants() {
        let items = this.state.data.data.relationships.menu_items.data;
        for (let i = 0; i < Math.min(3, items.length); i++) {
            let item = items[Math.floor(Math.random() * items.length)];
            axios.get(menu_item_url_prefix + item.id).then(menu_resp => {
                axios.get(restaurant_url_prefix + menu_resp.data.data.relationships.restaurant.data.id).then(response => {
                    let temp = this.state.restaurantRel;
                    temp[response.data.data.id] = {
                        "name": response.data.data.attributes.name,
                        "url": rest_model_prefix + response.data.data.id,
                        "image_url": response.data.data.attributes.image_url
                    };
                    this.setState({restaurantRel: temp});
                });
            });
        }
    }

    setupLabel() {
        let tempLabel = {};
        try { //Incase the
            axios.get(recipe_label_prefix + this.state.data.data.relationships.recipe_label.data.id).then(
                response => {
                    for (let key in this.state.recipeLabel) {
                        if (key in response.data.data.attributes && key in recipeLabelUnit) {
                            tempLabel[key] = (response.data.data.attributes[key].toFixed(2)).toString() + " " + recipeLabelUnit[key];
                        }
                    }
                }
            ).then(() => this.setState({recipeLabel: tempLabel}));
        } catch (e) {
        }
    }

    setupIngredientsandGrocs() {
        this.state.data.data.relationships.ingredients.data.forEach(related =>
            axios.get(ingredient_url_prefix + related.id).then(response => {
                    this.state.ingredients.push(response.data);
                    response.data.data.relationships.grocery_items.data.forEach(related => {
                        axios.get(grocery_prefix + related.id).then(groc_rep => {
                            let temp = this.state.groceryRel;
                            temp[response.data.data.id] = {
                                "name": groc_rep.data.data.attributes.name,
                                "url": groc_model_prefix + groc_rep.data.data.id,
                                "image_url": groc_rep.data.data.attributes.image_url
                            };
                            this.setState({groceryRel: temp});
                        });
                    });
                }
            ));
    }

    setupThis(id) {
        return axios.get(recipe_prefix + id).then(
            response => {
                this.setState({data: response.data});
            }
        );
    }

    render() {
        if (!this.state.done) {
            return (<div><h1>Loading</h1></div>)
        }
        let curr = this.state.data;
        let ingredients = this.state.ingredients;
        let groceryRel = this.state.groceryRel;
        let restaurantRel = this.state.restaurantRel;
        let recipeLabel = this.state.recipeLabel;
        return (
            <div>
                <Jumbotron>
                    <Container>
                        <h1 className="title">{curr.data.attributes.name} </h1>
                        <Image src={curr.data.attributes.image_url} alt="new" fluid/>
                    </Container>
                </Jumbotron>
                <Container>
                    <Row>
                        <Row>
                            <Col sm={{span: 3, offset: 0}}>
                                <h3>Item Details</h3>
                                <a href={curr.data.attributes.instructions}>
                                    <strong>Link To Instructions</strong>
                                </a>
                                <p>
                                    <strong>Health Labels:</strong> {curr.data.attributes.health_labels.map(item => {
                                    return (
                                        <React.Fragment>
                                            <p>{item}</p>
                                        </React.Fragment>
                                    )
                                })}
                                </p>
                                <p>
                                    <strong>Servings Made:</strong> {curr.data.attributes.servings}
                                </p>
                                <p>
                                    <strong>Total Calories:</strong> {curr.data.attributes.calories.toFixed(0)}
                                </p>
                                <p>
                                    <strong>Diet Labels:</strong> {curr.data.attributes.diet_labels.map(item => {
                                    return (
                                        <React.Fragment>
                                            <p>{item}</p>
                                        </React.Fragment>
                                    )
                                })}
                                </p>
                            </Col>


                            <Col md={{span: 5}}>
                                <Table striped bordered hover>
                                    <thead>
                                    <tr>
                                        <th>Ingredients</th>
                                        <th>Related Grocery Items</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {ingredients && ingredients.length > 0 && ingredients.map(item => {
                                        if (!(item.data.id in groceryRel)) {
                                            return (
                                                <React.Fragment>
                                                    {<tr key={item.data.id}>
                                                        <td>{item.data.attributes.text}</td>
                                                        <td>Related grocery item unavailable</td>
                                                    </tr>}
                                                </React.Fragment>
                                            )
                                        } else {
                                            return (
                                                <React.Fragment>
                                                    {<tr key={item.data.id}>
                                                        <td>{item.data.attributes.text}</td>
                                                        <td><a href={groceryRel[item.data.id].url}>
                                                            <strong>{groceryRel[item.data.id].name}</strong>
                                                        </a></td>
                                                    </tr>}
                                                </React.Fragment>
                                            )
                                        }
                                    })}
                                    </tbody>
                                </Table>
                                <p>
                                    <strong>Restaurants with Similar Dishes:</strong>
                                </p>
                                <RelationshipList relatedItems={Object.values(restaurantRel)}/>
                            </Col>
                            <Col md={{span: 4}}>
                                <h3>Detailed Nutritional Information</h3>
                                <Table striped bordered hover>
                                    <thead>
                                    <tr>
                                        <th>Nutrient</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {recipeLabel && Object.keys(recipeLabel).length > 0 && Object.keys(recipeLabel).map((key) => {
                                        return (
                                            <React.Fragment>
                                                {<tr key={key}>
                                                    <td>{recipeLabelName[key]}</td>
                                                    <td>{this.state.recipeLabel[key]}</td>
                                                </tr>}
                                            </React.Fragment>
                                        )
                                    })}
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>


                    </Row>
                </Container>
            </div>

        );
    }
}
