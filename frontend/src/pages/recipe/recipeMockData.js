export const recipeDat = {
    "data": {
        "attributes": {
            "calories": 1266.8200000000002,
            "created_at": "2019-10-16T17:59:18.026637",
            "diet_labels": [],
            "health_labels": ["Vegetarian", "Egg-Free", "Peanut-Free", "Tree-Nut-Free", "Soy-Free", "Fish-Free", "Shellfish-Free"],
            "image_url": "https://www.edamam.com/web-img/1a7/1a721316811eab3a30933e67a99839ba.jpg",
            "instructions": "http://www.seriouseats.com/recipes/2010/06/pop-tart-ice-cream-sandwiches-cakespy.html",
            "name": "Cakespy: Pop-Tarts Ice Cream Sandwiches Recipe",
            "servings": 6,
            "updated_at": "2019-10-16T17:59:18.026637"
        },
        "id": "253",
        "links": {"self": "http://api.foodcravings.net/api/recipes/253"},
        "relationships": {
            "ingredients": {
                "data": [{"id": "2323", "type": "ingredients"}, {
                    "id": "2324",
                    "type": "ingredients"
                }],
                "links": {
                    "related": "/api/recipes/253/ingredients",
                    "self": "/api/recipes/253/relationships/ingredients"
                }
            },
            "menu_items": {
                "data": [{"id": "142", "type": "menu_item"}],
                "links": {"related": "/api/recipes/253/menu_items", "self": "/api/recipes/253/relationships/menu_items"}
            },
            "recipe_label": {
                "data": null,
                "links": {
                    "related": "/api/recipes/253/recipe_label",
                    "self": "/api/recipes/253/relationships/recipe_label"
                }
            }
        },
        "type": "recipes"
    }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/recipes/253"}, "meta": {}
};
export const ingr_data1 = {
    "data": {
        "data": {
            "attributes": {
                "measure": "<unit>",
                "name": "Pop-Tarts",
                "quantity": 4.0,
                "text": "4 Pop-Tarts"
            },
            "id": "2323",
            "links": {"self": "http://api.foodcravings.net/api/ingredients/2323"},
            "relationships": {
                "grocery_items": {
                    "data": [],
                    "links": {
                        "related": "/api/ingredients/2323/grocery_items",
                        "self": "/api/ingredients/2323/relationships/grocery_items"
                    }
                },
                "recipe": {
                    "data": {"id": "253", "type": "recipes"},
                    "links": {
                        "related": "/api/ingredients/2323/recipe",
                        "self": "/api/ingredients/2323/relationships/recipe"
                    }
                }
            },
            "type": "ingredients"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/ingredients/2323"}, "meta": {}
    }
};
export const ingr_data2 = {
    "data": {
        "data": {
            "attributes": {
                "measure": "cup",
                "name": "ice cream",
                "quantity": 1.5,
                "text": "1 1/2 cups ice cream"
            },
            "id": "2324",
            "links": {"self": "http://api.foodcravings.net/api/ingredients/2324"},
            "relationships": {
                "grocery_items": {
                    "data": [{"id": "335", "type": "groceries"}],
                    "links": {
                        "related": "/api/ingredients/2324/grocery_items",
                        "self": "/api/ingredients/2324/relationships/grocery_items"
                    }
                },
                "recipe": {
                    "data": {"id": "253", "type": "recipes"},
                    "links": {
                        "related": "/api/ingredients/2324/recipe",
                        "self": "/api/ingredients/2324/relationships/recipe"
                    }
                }
            },
            "type": "ingredients"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/ingredients/2324"}, "meta": {}
    }
};
export const groc_data2 = {
    "data": {
        "data": {
            "attributes": {
                "brand": "Dean's Country Fresh",
                "category": "Home Page/Food/Frozen Foods/Ice Cream & Novelties",
                "created_at": "2019-10-16T17:59:18.026637",
                "image_url": "https://i5.walmartimages.com/asr/50466525-1891-49c7-b410-ebc74661ccff_1.f776060a069ae6ce9d25c4cd77cfc8ce.jpeg",
                "name": "Dean's Country Fresh Moose Tracks Ice Cream, 1.5 qt",
                "price": 0.0,
                "serving_size": 68.0,
                "serving_size_unit": "g",
                "updated_at": "2019-10-16T17:59:18.026637"
            }, "id": "335", "links": {"self": "http://api.foodcravings.net/api/groceries/335"}, "relationships": {
                "ingredients": {
                    "data": [{"id": "2324", "type": "ingredients"}, {
                        "id": "2748",
                        "type": "ingredients"
                    }, {"id": "2750", "type": "ingredients"}, {"id": "2755", "type": "ingredients"}, {
                        "id": "2759",
                        "type": "ingredients"
                    }, {"id": "2763", "type": "ingredients"}, {"id": "8694", "type": "ingredients"}, {
                        "id": "8968",
                        "type": "ingredients"
                    }, {"id": "9055", "type": "ingredients"}, {"id": "9323", "type": "ingredients"}, {
                        "id": "9338",
                        "type": "ingredients"
                    }, {"id": "9617", "type": "ingredients"}, {"id": "9684", "type": "ingredients"}, {
                        "id": "10126",
                        "type": "ingredients"
                    }, {"id": "11533", "type": "ingredients"}, {"id": "19872", "type": "ingredients"}, {
                        "id": "20065",
                        "type": "ingredients"
                    }, {"id": "26296", "type": "ingredients"}, {"id": "26691", "type": "ingredients"}, {
                        "id": "28309",
                        "type": "ingredients"
                    }, {"id": "28312", "type": "ingredients"}, {"id": "28319", "type": "ingredients"}],
                    "links": {
                        "related": "/api/groceries/335/ingredients",
                        "self": "/api/groceries/335/relationships/ingredients"
                    }
                },
                "label": {
                    "data": {"id": "451", "type": "nutrition_label"},
                    "links": {"related": "/api/groceries/335/label", "self": "/api/groceries/335/relationships/label"}
                }
            }, "type": "groceries"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/groceries/335"}, "meta": {}
    }
};
export const ingredient_data = {
    "2323": ingr_data1,
    "2324": ingr_data2
};

export const grocery_data = {
    "2324": {
        "name": "Dean's Country Fresh Moose Tracks Ice Cream, 1.5 qt",
        "url": "/groceries/335",
        "image_url": "https://i5.walmartimages.com/asr/50466525-1891-49c7-b410-ebc74661ccff_1.f776060a069ae6ce9d25c4cd77cfc8ce.jpeg"
    }
};


