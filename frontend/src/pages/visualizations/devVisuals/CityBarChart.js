import React, {Component} from 'react';
import * as d3 from 'd3'
import data from "./cities"

export default class CityBarChart extends Component {
    constructor(props) {
        super(props);
        this.createBarChart = this.createBarChart.bind(this)
    }

    componentDidMount() {
        this.createBarChart()
    }

    componentDidUpdate() {
        this.createBarChart()
    }

    createBarChart() {
        let margin = ({top: 20, right: 0, bottom: 30, left: 40});
        let width = 1000;
        let height = 500;

        let x = d3.scaleBand()
            .domain(data.map(d => d.name))
            .range([margin.left, width - margin.right])
            .padding(0.1);

        let y = d3.scaleLinear()
            .domain([65, d3.max(data, d => d.score)]).nice()
            .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select(".domain").remove());


        const svg = d3.select("#body8");

        svg.append("g")
            .attr("fill", "steelblue")
            .selectAll("rect").data(data).enter().append("rect")
            .attr("x", d => x(d.name))
            .attr("y", d => y(d.score))
            .attr("height", d => y(65) - y(d.score))
            .attr("width", x.bandwidth());

        svg.append("g")
            .call(xAxis);

        svg.append("g")
            .call(yAxis);
    }

    render() {
        return (
            <div style={{justifyContent: "center"}}>
                <h1> Top 10 Cities Based on Overall Score </h1>
                <p></p>
                <div>
                    <svg id='body8' width="80%" height={500}></svg>
                </div>

            </div>
        )

    }
}