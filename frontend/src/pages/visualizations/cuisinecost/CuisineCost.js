import React, {Component} from 'react';
import * as d3 from "d3";
import data from "./cuisineprice"
import "./cuisine.css"

class CuisineCost extends Component {
    componentDidMount() {
        this.createPetriChart()
    }

    componentDidUpdate() {
        this.createPetriChart();
    }

    createPetriChart() {
        let margin = ({top: 20, right: 0, bottom: 30, left: 40});
        let width = 1700;
        let height = 700;
        console.log(data);
        let x = d3.scaleBand()
            .domain(data.map(d => d.Cuisine))
            .range([margin.left, width - margin.right])
            .padding(0.1);

        let y = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.AveragePrice)]).nice()
            .range([height - margin.bottom, margin.top]);

        let xAxis = g => g
            .attr("transform", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));

        let yAxis = g => g
            .attr("transform", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select(".domain").remove());


        const svg = d3.select("#body3");

        svg.append("g")
            .attr("fill", "steelblue")
            .selectAll("rect").data(data).enter().append("rect")
            .attr("x", d => x(d.Cuisine))
            .attr("y", d => y(d.AveragePrice))
            .attr("height", d => y(0) - y(d.AveragePrice))
            .attr("width", x.bandwidth());

        svg.append("g")
            .call(xAxis);

        svg.append("g")
            .call(yAxis);

    }

    render() {
        return (
            <div>
                <h1>Average Price per Cuisine</h1>
                <p> This visualization shows the average price per cuisine across all of the menu items and restaurants
                    in our database.</p>
                <div className={"mdiv"}>
                    <svg id='body3' className={"viz"}
                         width={1700} height={700}>
                    </svg>
                </div>
            </div>
        );
    }
}

export default CuisineCost