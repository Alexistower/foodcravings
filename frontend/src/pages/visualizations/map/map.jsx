import React, {Component} from 'react';
import * as d3 from "d3";
import d3Tip from 'd3-tip'
import top from "./land-50m"
import "./map.css"
import * as topojson from "topojson-client";
import axios from "axios";


class CitMap extends Component {

    constructor(props) {
        super(props);
        this.state = {
            done: false,
            data: {}
        };
        axios.get("https://api.foodcravings.net/api/empcities").then(response => {
            this.setState({done: true, data: response.data})
        });
    }

    componentDidMount() {
        this.createMap()
    }

    componentDidUpdate() {
        this.createMap();
    }

    createMap() {
        var width = 1300;
        var height = 500;
        var projection = d3.geoMercator().scale(width / 2 / Math.PI)
            .translate([width / 2, height / 2]);
        var path = d3.geoPath()
            .projection(projection);
        let places = this.state.data;
        var svg = d3.select("#map");
        var swiss = top;
        var cantons = topojson.feature(swiss, swiss.objects.land);


        var group = svg.selectAll("g")
            .data(cantons.features)
            .enter()
            .append("g");


        var tip = d3Tip()
            .attr('class', 'd3-tip')
            .offset([-5, 0])
            .style("left", "300px")
            .style("top", "400px")
            .html(function (d) {
                return ("<strong style='color: #ffa84d'>" + d.name + "</strong>");
            });

        svg.call(tip);
        svg.selectAll(".pin")
            .data(places)
            .enter().append("circle", ".pin")
            .attr("r", 2.55)
            .attr("transform", function (d) {
                return "translate(" + projection([
                    d.location.longitude,
                    d.location.latitude
                ]) + ")";
            }).attr("fill", "black")
            .on('mouseover', tip.show)
            .on('click', tip.hide);
        group.append("path")
            .attr("d", path)
            .attr("class", "area")
            .attr("fill", "gray");

    }

    render() {
        if (!this.state.done) {
            return <h1>Loading</h1>
        }
        return (
            <div className={"mapdiv"}>
                <h1>Cities and their Scores around the Globe</h1>
                <p> This visualization shows the scores of cities across the globe </p>
                <svg className={"viz"} id='map' width={1500} height={600}/>
            </div>
        );
    }
}

export default CitMap