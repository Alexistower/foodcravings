import React, {Component} from 'react';
import * as d3 from "d3";
import "./routesinfo.css";

class RoutePieChart extends Component {
    componentDidMount() {
        this.createPieChart()
    }

    componentDidUpdate() {
        this.createPieChart();
    }

    createPieChart() {


        let width = 600;
        let height = 600;
        let margin = 40;

        // The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
        let radius = Math.min(width, height) / 2 - margin;

        // append the svg object to the div called 'my_dataviz'
        const svg = d3.select("#body3")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        // Create dummy data
        let data = {"BW": 318, "B": 78, "W": 153, "Neither": 2398};
        // set the color scale
        let color = d3.scaleOrdinal()
            .domain(data)
            .range(d3.schemeSet2);

        // Compute the position of each group on the pie:
        let pie = d3.pie()
            .value(function (d) {
                return d.value;
            });
        let data_ready = pie(d3.entries(data));
        // Now I know that group A goes from 0 degrees to x degrees and so on.

        // shape helper to build arcs:
        let arcGenerator = d3.arc()
            .innerRadius(0)
            .outerRadius(radius);

        // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
        svg
            .selectAll('mySlices')
            .data(data_ready)
            .enter()
            .append('path')
            .attr('d', arcGenerator)
            .attr('fill', function (d) {
                return (color(d.data.key))
            })
            .attr("stroke", "black")
            .style("stroke-width", "2px")
            .style("opacity", 0.7);

        // Now add the annotation. Use the centroid method to get the best coordinates
        svg
            .selectAll('mySlices')
            .data(data_ready)
            .enter()
            .append('text')
            .text(function (d) {
                return d.data.key
            })
            .attr("transform", function (d) {
                return "translate(" + arcGenerator.centroid(d) + ")";
            })
            .style("text-anchor", "middle")
            .style("font-size", 17)


    }

    render() {
        let proportions = {
            "BW": 318 / (318 + 78 + 153 + 2398),
            "B": 78 / (318 + 78 + 153 + 2398),
            "W": 153 / (318 + 78 + 153 + 2398),
            "Neither": 2398 / (318 + 78 + 153 + 2398)
        };
        return (
            <div>
                <h1>Breakdown of Bike & Wheelchair Support on Transit Routes</h1>
                <div>
                    <p><strong>BW</strong> : Bike and Wheelchair Support: {proportions["BW"].toFixed(2)}</p>
                    <p><strong>B</strong> : Only Bike Support {proportions["B"].toFixed(2)}</p>
                    <p><strong>W</strong> : Only Wheelchair Support {proportions["W"].toFixed(2)}</p>
                    <p><strong>Neither</strong> : No Support {proportions["Neither"].toFixed(2)}</p>

                    <svg id='body3' className={"maindiv"}
                         width={650} height={650}>
                    </svg>


                </div>
            </div>
        );
    }
}

export default RoutePieChart