import React, {Component} from 'react';
import * as d3 from 'd3'

export default class GroceryNetworkGraph extends Component {
    constructor(props) {
        super(props);
        this.createNetworkGraph = this.createNetworkGraph.bind(this)
    }

    componentDidMount() {
        this.createNetworkGraph()
    }

    componentDidUpdate() {
        this.createNetworkGraph()
    }

    createNetworkGraph() {
        const width = 700;
        const height = 700;

        const svg = d3.select("#body")
            .append("svg")
            .attr("width", width)
            .attr("height", height);

        d3.json('./frequent_grocery_data.json').then(function (graph, error) {
            if (error) throw error;     // Our json is the error that is being thrown ?
                                        // The args for the callback were backwards, lol

            let simulation = d3.forceSimulation(graph.nodes)
                .force("link", d3.forceLink(graph.links).id(function (d) {
                    return d.id;
                }))
                .force("charge", d3.forceManyBody().strength(-1000))
                .force("center", d3.forceCenter(width / 2, height / 2));

            simulation.on("tick", ticked);

            var g = svg.append("g")
                .attr("class", "everything");

            // Make lines for links
            var link = g.append("g")
                .attr("class", "links")
                .style("stroke", "#aaa")
                .selectAll("line")
                .data(graph.links)
                .enter().append("line")
                .style("stroke-width", 2);

            // Make circles for nodes
            var node = g.append("g")
                .attr("class", "nodes")
                .selectAll("circle")
                .data(graph.nodes)
                .enter().append("circle")
                .attr("r", circleSize)
                .style("fill", circleColour)
                .style("stroke", "#969696")
                .style("stroke-width", "1px");

            var label = g.append("g")
                .attr("class", "labels")
                .selectAll("text")
                .data(graph.nodes)
                .enter().append("text")
                .attr("class", "label")
                .text(function (d) {
                    return d.name;
                });

            var drag_handler = d3.drag()
                .on("start", drag_start)
                .on("drag", drag_drag)
                .on("end", drag_end);

            drag_handler(node);


            var zoom_handler = d3.zoom()
                .on("zoom", zoom_actions);

            zoom_handler(svg);
            svg.call(zoom_handler)
                .call(zoom_handler.transform, d3.zoomIdentity.translate(100, 50).scale(0.5));


            function drag_start(d) {
                if (!d3.event.active) simulation.alphaTarget(0.3).restart();
                d.fx = d.x;
                d.fy = d.y;
            }

            function drag_drag(d) {
                d.fx = d3.event.x;
                d.fy = d3.event.y;
            }

            function drag_end(d) {
                if (!d3.event.active) simulation.alphaTarget(0);
                d.fx = null;
                d.fy = null;
            }

            function zoom_actions() {
                g.attr("transform", d3.event.transform);
            }

            // Function to choose what size the node will be
            function circleSize(d) {
                return d.size;
            }

            //Function to choose what color the node will be
            function circleColour(d) {
                if (d.type === "g") {
                    return "green";
                } else {
                    return "red";
                }
            }

            function ticked() {
                link
                    .attr("x1", function (d) {
                        return d.source.x;
                    })
                    .attr("y1", function (d) {
                        return d.source.y;
                    })
                    .attr("x2", function (d) {
                        return d.target.x;
                    })
                    .attr("y2", function (d) {
                        return d.target.y;
                    });

                node
                    .attr("cx", function (d) {
                        return d.x;
                    })
                    .attr("cy", function (d) {
                        return d.y;
                    });

                label
                    .attr("x", function (d) {
                        return d.x;
                    })
                    .attr("y", function (d) {
                        return d.y;
                    })
                    .style("font-size", "20px").style("fill", "black");

            }

        });

    }

    render() {
        return (
            <div>
                <h1> Top 10 Most Frequent Groceries in Recipes </h1>
                <p> This visualization shows the top ten most frequent appearing groceries in all recipes with 50 links
                    each to some of their respective related recipes.</p>
                <div>
                    <svg id='body' width={700} height={700}></svg>
                </div>

            </div>
        )

    }
}