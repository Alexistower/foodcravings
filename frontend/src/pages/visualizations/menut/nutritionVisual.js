import React, {Component} from 'react';
import * as d3 from "d3";
import axios from "axios";
import "./menunut.css"


class NutritionalVisual extends Component {
    constructor(props) {
        super(props);
        this.state = {
            done: false,
            data: {}
        };
        axios.get("https://api.foodcravings.net/api/menutrition").then(response => {
            this.setState({done: true, data: response.data})
        });
    }

    componentDidMount() {
        this.createPetriChart()
    }

    componentDidUpdate() {
        this.createPetriChart();
    }

    createPetriChart() {
        let width = 700;
        let height = 700;
        let data = this.state.data;
        console.log(data);
        let pack = data => d3.pack()
            .size([width, height])
            .padding(3)(d3.hierarchy(data)
                .sum(d => d.value)
                .sort((a, b) => b.value - a.value));
        let color = d3.scaleLinear()
            .domain([0, 5])
            .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
            .interpolate(d3.interpolateHcl);
        const root = pack(data);
        let focus = root;
        let view;

        const svg = d3.select("#body3")
            .attr("viewBox", `-${width / 2} -${height / 2} ${width} ${height}`)
            .style("display", "block")
            .style("margin", "0 -14px")
            .style("background", color(0))
            .style("cursor", "pointer")
            .on("click", () => zoom(root));

        const node = svg.append("g")
            .selectAll("circle")
            .data(root.descendants().slice(1))
            .join("circle")
            .attr("fill", d => d.children ? color(d.depth) : "white")
            .attr("pointer-events", d => !d.children ? "none" : null)
            .on("mouseover", function () {
                d3.select(this).attr("stroke", "#000");
            })
            .on("mouseout", function () {
                d3.select(this).attr("stroke", null);
            })
            .on("click", d => focus !== d && (zoom(d), d3.event.stopPropagation()));

        const label = svg.append("g")
            .style("font", "10px sans-serif")
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .selectAll("text")
            .data(root.descendants())
            .join("text")
            .style("fill-opacity", d => d.parent === root ? 1 : 0)
            .style("display", d => d.parent === root ? "inline" : "none")
            .text(d => d.data.name);

        zoomTo([root.x, root.y, root.r * 2]);

        function zoomTo(v) {
            const k = width / v[2];

            view = v;

            label.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
            node.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
            node.attr("r", d => d.r * k);
        }

        function zoom(d) {

            focus = d;

            const transition = svg.transition()
                .duration(d3.event.altKey ? 7500 : 750)
                .tween("zoom", d => {
                    const i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2]);
                    return t => zoomTo(i(t));
                });

            label
                .filter(function (d) {
                    return d.parent === focus || this.style.display === "inline";
                })
                .transition(transition)
                .style("fill-opacity", d => d.parent === focus ? 1 : 0)
                .on("start", function (d) {
                    if (d.parent === focus) this.style.display = "inline";
                })
                .on("end", function (d) {
                    if (d.parent !== focus) this.style.display = "none";
                });
        }

        console.log("HELLO!!!!!!!!!");

    }

    render() {
        return (
            <div>
                <h1>Relative Nutritional Composition of Various Dishes at Restaurants</h1>
                <p> This visualization shows the primary nutrional components of ten random dishes from each restaurant.
                    The circles have sizes relative to their composition in grams per serving.</p>
                <div className={"div"}>
                    <svg id='body3' className={"viz"}
                         width={700} height={700}>
                    </svg>
                </div>
            </div>
        );
    }
}

export default NutritionalVisual