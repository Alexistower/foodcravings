const recipes = [

    {
        "data": {
            "attributes": {
                "calories": 618.0367551922876,
                "created_at": "2019-10-16T17:59:18.026637",
                "diet_labels": ["High-Fiber", "Low-Carb"],
                "health_labels": ["Paleo", "Dairy-Free", "Gluten-Free", "Egg-Free", "Peanut-Free", "Tree-Nut-Free", "Soy-Free", "Shellfish-Free"],
                "image_url": "https://www.edamam.com/web-img/f26/f26a9c011285c7f069a8b96681a855e8.jpg",
                "instructions": "http://www.marthastewart.com/314578/lemon-tuna-avocado-snack",
                "name": "Lemon Tuna Avocado Snack",
                "servings": 1,
                "updated_at": "2019-10-16T17:59:18.026637"
            },
            "id": "213",
            "links": {"self": "http://127.0.0.1:5000/api/recipes/213"},
            "relationships": {
                "ingredients": {
                    "data": [{"id": "1937", "type": "ingredients"}, {
                        "id": "1938",
                        "type": "ingredients"
                    }, {"id": "1939", "type": "ingredients"}, {"id": "1940", "type": "ingredients"}, {
                        "id": "1941",
                        "type": "ingredients"
                    }, {"id": "1942", "type": "ingredients"}, {"id": "1943", "type": "ingredients"}],
                    "links": {
                        "related": "/api/recipes/213/ingredients",
                        "self": "/api/recipes/213/relationships/ingredients"
                    }
                },
                "menu_items": {
                    "data": [{"id": "119", "type": "menu_item"}, {
                        "id": "120",
                        "type": "menu_item"
                    }, {"id": "1016", "type": "menu_item"}, {"id": "1039", "type": "menu_item"}],
                    "links": {
                        "related": "/api/recipes/213/menu_items",
                        "self": "/api/recipes/213/relationships/menu_items"
                    }
                }
            },
            "type": "recipes"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/recipes/213"}, "meta": {}
    },

    {
        "data": {
            "attributes": {
                "calories": 2354.0413057461005,
                "created_at": "2019-10-16T17:59:18.026637",
                "diet_labels": ["Low-Carb"],
                "health_labels": ["Peanut-Free", "Tree-Nut-Free", "Soy-Free", "Fish-Free", "Shellfish-Free"],
                "image_url": "https://www.edamam.com/web-img/be7/be7f379b683522d48a06af1ba2c9e2c7.jpg",
                "instructions": "http://www.foodrepublic.com/2011/07/25/spicy-sausage-stuffed-mushrooms-recipe",
                "name": "Spicy Sausage Stuffed Mushrooms Recipe",
                "servings": 6,
                "updated_at": "2019-10-16T17:59:18.026637"
            },
            "id": "233",
            "links": {"self": "http://127.0.0.1:5000/api/recipes/233"},
            "relationships": {
                "ingredients": {
                    "data": [{"id": "2131", "type": "ingredients"}, {
                        "id": "2132",
                        "type": "ingredients"
                    }, {"id": "2133", "type": "ingredients"}, {"id": "2134", "type": "ingredients"}, {
                        "id": "2135",
                        "type": "ingredients"
                    }, {"id": "2136", "type": "ingredients"}, {"id": "2137", "type": "ingredients"}, {
                        "id": "2138",
                        "type": "ingredients"
                    }, {"id": "2139", "type": "ingredients"}],
                    "links": {
                        "related": "/api/recipes/233/ingredients",
                        "self": "/api/recipes/233/relationships/ingredients"
                    }
                },
                "menu_items": {
                    "data": [{"id": "131", "type": "menu_item"}, {
                        "id": "132",
                        "type": "menu_item"
                    }, {"id": "2426", "type": "menu_item"}],
                    "links": {
                        "related": "/api/recipes/233/menu_items",
                        "self": "/api/recipes/233/relationships/menu_items"
                    }
                }
            },
            "type": "recipes"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/recipes/233"}, "meta": {}
    },

    {
        "data": {
            "attributes": {
                "calories": 1266.8200000000002,
                "created_at": "2019-10-16T17:59:18.026637",
                "diet_labels": [],
                "health_labels": ["Vegetarian", "Egg-Free", "Peanut-Free", "Tree-Nut-Free", "Soy-Free", "Fish-Free", "Shellfish-Free"],
                "image_url": "https://www.edamam.com/web-img/1a7/1a721316811eab3a30933e67a99839ba.jpg",
                "instructions": "http://www.seriouseats.com/recipes/2010/06/pop-tart-ice-cream-sandwiches-cakespy.html",
                "name": "Cakespy: Pop-Tarts Ice Cream Sandwiches Recipe",
                "servings": 6,
                "updated_at": "2019-10-16T17:59:18.026637"
            },
            "id": "253",
            "links": {"self": "http://127.0.0.1:5000/api/recipes/253"},
            "relationships": {
                "ingredients": {
                    "data": [{"id": "2323", "type": "ingredients"}, {
                        "id": "2324",
                        "type": "ingredients"
                    }],
                    "links": {
                        "related": "/api/recipes/253/ingredients",
                        "self": "/api/recipes/253/relationships/ingredients"
                    }
                },
                "menu_items": {
                    "data": [{"id": "142", "type": "menu_item"}],
                    "links": {
                        "related": "/api/recipes/253/menu_items",
                        "self": "/api/recipes/253/relationships/menu_items"
                    }
                }
            },
            "type": "recipes"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/recipes/253"}, "meta": {}
    }

];