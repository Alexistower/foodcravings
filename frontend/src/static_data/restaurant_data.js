const restaurants = [

    {
        "data": {
            "attributes": {
                "address": "1908 Guadalupe St AUSTIN, TX 78705",
                "created_at": "2019-10-17T02:09:31.619899",
                "cuisines": ["Noodles", "Vietnamese"],
                "image_url": "",
                "name": "Pho Thaison",
                "phone": "(512) 482-0146",
                "priceRange": "$$",
                "updated_at": "2019-10-17T02:09:31.619899"
            }, "id": "30", "links": {"self": "http://127.0.0.1:5000/api/restaurant/30"}, "relationships": {
                "menu_items": {
                    "data": [{"id": "1783", "type": "menu_item"}, {"id": "1784", "type": "menu_item"}, {
                        "id": "1785",
                        "type": "menu_item"
                    }, {"id": "1786", "type": "menu_item"}, {"id": "1787", "type": "menu_item"}, {
                        "id": "1788",
                        "type": "menu_item"
                    }, {"id": "1789", "type": "menu_item"}, {"id": "1790", "type": "menu_item"}, {
                        "id": "1791",
                        "type": "menu_item"
                    }, {"id": "1792", "type": "menu_item"}, {"id": "1793", "type": "menu_item"}, {
                        "id": "1794",
                        "type": "menu_item"
                    }, {"id": "1795", "type": "menu_item"}, {"id": "1796", "type": "menu_item"}, {
                        "id": "1797",
                        "type": "menu_item"
                    }, {"id": "1798", "type": "menu_item"}, {"id": "1799", "type": "menu_item"}, {
                        "id": "1800",
                        "type": "menu_item"
                    }, {"id": "1801", "type": "menu_item"}, {"id": "1802", "type": "menu_item"}, {
                        "id": "1803",
                        "type": "menu_item"
                    }, {"id": "1804", "type": "menu_item"}, {"id": "1805", "type": "menu_item"}, {
                        "id": "1806",
                        "type": "menu_item"
                    }, {"id": "1807", "type": "menu_item"}, {"id": "1808", "type": "menu_item"}, {
                        "id": "1809",
                        "type": "menu_item"
                    }, {"id": "1810", "type": "menu_item"}, {"id": "1811", "type": "menu_item"}, {
                        "id": "1812",
                        "type": "menu_item"
                    }, {"id": "1813", "type": "menu_item"}, {"id": "1814", "type": "menu_item"}, {
                        "id": "1815",
                        "type": "menu_item"
                    }, {"id": "1816", "type": "menu_item"}, {"id": "1817", "type": "menu_item"}, {
                        "id": "1818",
                        "type": "menu_item"
                    }, {"id": "1819", "type": "menu_item"}, {"id": "1820", "type": "menu_item"}, {
                        "id": "1821",
                        "type": "menu_item"
                    }, {"id": "1822", "type": "menu_item"}, {"id": "1823", "type": "menu_item"}, {
                        "id": "1824",
                        "type": "menu_item"
                    }, {"id": "1825", "type": "menu_item"}, {"id": "1826", "type": "menu_item"}, {
                        "id": "1827",
                        "type": "menu_item"
                    }, {"id": "1828", "type": "menu_item"}, {"id": "1829", "type": "menu_item"}, {
                        "id": "1830",
                        "type": "menu_item"
                    }, {"id": "1831", "type": "menu_item"}, {"id": "1832", "type": "menu_item"}, {
                        "id": "1833",
                        "type": "menu_item"
                    }, {"id": "1834", "type": "menu_item"}, {"id": "1835", "type": "menu_item"}, {
                        "id": "1836",
                        "type": "menu_item"
                    }, {"id": "1837", "type": "menu_item"}, {"id": "1838", "type": "menu_item"}, {
                        "id": "1839",
                        "type": "menu_item"
                    }, {"id": "1840", "type": "menu_item"}, {"id": "1841", "type": "menu_item"}, {
                        "id": "1842",
                        "type": "menu_item"
                    }, {"id": "1843", "type": "menu_item"}, {"id": "1844", "type": "menu_item"}, {
                        "id": "1845",
                        "type": "menu_item"
                    }, {"id": "1846", "type": "menu_item"}, {"id": "1847", "type": "menu_item"}, {
                        "id": "1848",
                        "type": "menu_item"
                    }, {"id": "1849", "type": "menu_item"}, {"id": "1850", "type": "menu_item"}, {
                        "id": "1851",
                        "type": "menu_item"
                    }, {"id": "1852", "type": "menu_item"}, {"id": "1853", "type": "menu_item"}, {
                        "id": "1854",
                        "type": "menu_item"
                    }, {"id": "1855", "type": "menu_item"}, {"id": "1856", "type": "menu_item"}, {
                        "id": "1857",
                        "type": "menu_item"
                    }, {"id": "1858", "type": "menu_item"}, {"id": "1859", "type": "menu_item"}, {
                        "id": "1860",
                        "type": "menu_item"
                    }, {"id": "1861", "type": "menu_item"}, {"id": "1862", "type": "menu_item"}, {
                        "id": "1863",
                        "type": "menu_item"
                    }, {"id": "1864", "type": "menu_item"}, {"id": "1865", "type": "menu_item"}, {
                        "id": "1866",
                        "type": "menu_item"
                    }, {"id": "1867", "type": "menu_item"}, {"id": "1868", "type": "menu_item"}, {
                        "id": "1869",
                        "type": "menu_item"
                    }, {"id": "1870", "type": "menu_item"}, {"id": "1871", "type": "menu_item"}, {
                        "id": "1872",
                        "type": "menu_item"
                    }, {"id": "1873", "type": "menu_item"}, {"id": "1874", "type": "menu_item"}, {
                        "id": "1875",
                        "type": "menu_item"
                    }, {"id": "1876", "type": "menu_item"}, {"id": "1877", "type": "menu_item"}, {
                        "id": "1878",
                        "type": "menu_item"
                    }, {"id": "1879", "type": "menu_item"}, {"id": "1880", "type": "menu_item"}, {
                        "id": "1881",
                        "type": "menu_item"
                    }, {"id": "1882", "type": "menu_item"}, {"id": "1883", "type": "menu_item"}, {
                        "id": "1884",
                        "type": "menu_item"
                    }, {"id": "1885", "type": "menu_item"}, {"id": "1886", "type": "menu_item"}, {
                        "id": "1887",
                        "type": "menu_item"
                    }, {"id": "1888", "type": "menu_item"}, {"id": "1889", "type": "menu_item"}, {
                        "id": "1890",
                        "type": "menu_item"
                    }, {"id": "1891", "type": "menu_item"}, {"id": "1892", "type": "menu_item"}, {
                        "id": "1893",
                        "type": "menu_item"
                    }, {"id": "1894", "type": "menu_item"}, {"id": "1895", "type": "menu_item"}, {
                        "id": "1896",
                        "type": "menu_item"
                    }, {"id": "1897", "type": "menu_item"}, {"id": "1898", "type": "menu_item"}, {
                        "id": "1899",
                        "type": "menu_item"
                    }, {"id": "1900", "type": "menu_item"}, {"id": "1901", "type": "menu_item"}, {
                        "id": "1902",
                        "type": "menu_item"
                    }, {"id": "1903", "type": "menu_item"}, {"id": "1904", "type": "menu_item"}, {
                        "id": "1905",
                        "type": "menu_item"
                    }, {"id": "1906", "type": "menu_item"}, {"id": "1907", "type": "menu_item"}],
                    "links": {
                        "related": "/api/restaurant/30/menu_items",
                        "self": "/api/restaurant/30/relationships/menu_items"
                    }
                }
            }, "type": "restaurant"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/restaurant/30"}, "meta": {}
    },

    {
        "data": {
            "attributes": {
                "address": "2246 Guadalupe St Austin, TX 78705",
                "created_at": "2019-10-16T06:22:29.397289",
                "cuisines": [],
                "image_url": "",
                "name": "Psycha Delhi",
                "phone": "(512) 350-6713",
                "priceRange": "$",
                "updated_at": "2019-10-16T06:22:29.397289"
            },
            "id": "7",
            "links": {"self": "http://127.0.0.1:5000/api/restaurant/7"},
            "relationships": {
                "menu_items": {
                    "data": [{"id": "24", "type": "menu_item"}, {
                        "id": "25",
                        "type": "menu_item"
                    }, {"id": "26", "type": "menu_item"}, {"id": "27", "type": "menu_item"}, {
                        "id": "28",
                        "type": "menu_item"
                    }, {"id": "29", "type": "menu_item"}, {"id": "30", "type": "menu_item"}, {
                        "id": "31",
                        "type": "menu_item"
                    }, {"id": "32", "type": "menu_item"}, {"id": "33", "type": "menu_item"}, {
                        "id": "34",
                        "type": "menu_item"
                    }, {"id": "35", "type": "menu_item"}, {"id": "36", "type": "menu_item"}],
                    "links": {
                        "related": "/api/restaurant/7/menu_items",
                        "self": "/api/restaurant/7/relationships/menu_items"
                    }
                }
            },
            "type": "restaurant"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/restaurant/7"}, "meta": {}
    },
    {
        "data": {
            "attributes": {
                "address": "411 W 23rd St Austin, TX 78705",
                "created_at": "2019-10-16T23:04:37.023749",
                "cuisines": [],
                "image_url": "",
                "name": "Taqueria Jefe's",
                "phone": "(512) 459-0034",
                "priceRange": "",
                "updated_at": "2019-10-16T23:04:37.023749"
            }, "id": "12", "links": {"self": "http://127.0.0.1:5000/api/restaurant/12"}, "relationships": {
                "menu_items": {
                    "data": [{"id": "201", "type": "menu_item"}, {"id": "202", "type": "menu_item"}, {
                        "id": "191",
                        "type": "menu_item"
                    }, {"id": "192", "type": "menu_item"}, {"id": "193", "type": "menu_item"}, {
                        "id": "194",
                        "type": "menu_item"
                    }, {"id": "195", "type": "menu_item"}, {"id": "196", "type": "menu_item"}, {
                        "id": "197",
                        "type": "menu_item"
                    }, {"id": "198", "type": "menu_item"}, {"id": "199", "type": "menu_item"}, {
                        "id": "200",
                        "type": "menu_item"
                    }, {"id": "203", "type": "menu_item"}, {"id": "204", "type": "menu_item"}, {
                        "id": "205",
                        "type": "menu_item"
                    }, {"id": "206", "type": "menu_item"}, {"id": "207", "type": "menu_item"}, {
                        "id": "208",
                        "type": "menu_item"
                    }, {"id": "209", "type": "menu_item"}, {"id": "210", "type": "menu_item"}, {
                        "id": "211",
                        "type": "menu_item"
                    }, {"id": "212", "type": "menu_item"}, {"id": "213", "type": "menu_item"}, {
                        "id": "214",
                        "type": "menu_item"
                    }, {"id": "215", "type": "menu_item"}, {"id": "216", "type": "menu_item"}, {
                        "id": "217",
                        "type": "menu_item"
                    }, {"id": "218", "type": "menu_item"}, {"id": "219", "type": "menu_item"}, {
                        "id": "220",
                        "type": "menu_item"
                    }, {"id": "221", "type": "menu_item"}, {"id": "222", "type": "menu_item"}, {
                        "id": "223",
                        "type": "menu_item"
                    }, {"id": "224", "type": "menu_item"}, {"id": "225", "type": "menu_item"}, {
                        "id": "226",
                        "type": "menu_item"
                    }, {"id": "227", "type": "menu_item"}, {"id": "228", "type": "menu_item"}, {
                        "id": "229",
                        "type": "menu_item"
                    }, {"id": "230", "type": "menu_item"}, {"id": "231", "type": "menu_item"}, {
                        "id": "232",
                        "type": "menu_item"
                    }, {"id": "233", "type": "menu_item"}],
                    "links": {
                        "related": "/api/restaurant/12/menu_items",
                        "self": "/api/restaurant/12/relationships/menu_items"
                    }
                }
            }, "type": "restaurant"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/restaurant/12"}, "meta": {}
    }

];
export default restaurants;