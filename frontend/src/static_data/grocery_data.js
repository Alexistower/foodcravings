const groceries = [

    {
        "data": {
            "attributes": {
                "brand": "Fresh Beef",
                "category": "Home Page/Food/Fresh Food/Meat, Seafood & Poultry/Beef",
                "created_at": "2019-10-16T17:59:18.026637",
                "image_url": "",
                "name": "Beef Bottom Round Roast 2.0-3.0 lb",
                "price": 0.0,
                "serving_size": 112.0,
                "serving_size_unit": "g",
                "test": null,
                "updated_at": "2019-10-16T17:59:18.026637"
            },
            "id": "253",
            "links": {"self": "http://127.0.0.1:5000/api/groceries/253"},
            "relationships": {
                "ingredient": {
                    "data": {"id": "28899", "type": "ingredients"},
                    "links": {
                        "related": "/api/groceries/253/ingredient",
                        "self": "/api/groceries/253/relationships/ingredient"
                    }
                },
                "label": {
                    "data": {"id": "369", "type": "nutrition_label"},
                    "links": {"related": "/api/groceries/253/label", "self": "/api/groceries/253/relationships/label"}
                }
            },
            "type": "groceries"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/groceries/253"}, "meta": {}
    },

    {
        "data": {
            "attributes": {
                "brand": "Progresso",
                "category": "Home Page/Food/Baking/Baking Mixes",
                "created_at": "2019-10-17T00:54:15.960591",
                "image_url": "",
                "name": "(3 Pack) Progresso Italian Style Bread Crumbs, 40 oz",
                "price": 10.0,
                "serving_size": 230.0,
                "serving_size_unit": "g",
                "test": null,
                "updated_at": "2019-10-17T00:54:15.960591"
            },
            "id": "1299",
            "links": {"self": "http://127.0.0.1:5000/api/groceries/1299"},
            "relationships": {
                "ingredient": {
                    "data": {"id": "14370", "type": "ingredients"},
                    "links": {
                        "related": "/api/groceries/1299/ingredient",
                        "self": "/api/groceries/1299/relationships/ingredient"
                    }
                },
                "label": {
                    "data": {"id": "1415", "type": "nutrition_label"},
                    "links": {"related": "/api/groceries/1299/label", "self": "/api/groceries/1299/relationships/label"}
                }
            },
            "type": "groceries"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/groceries/1299"}, "meta": {}
    },

    {
        "data": {
            "attributes": {
                "brand": "Great Value",
                "category": "Home Page/Food/Baking/Baking Mixes",
                "created_at": "2019-10-16T17:59:18.026637",
                "image_url": "",
                "name": "(5 Pack) Great Value Italian Style Bread Crumbs, 15 oz",
                "price": 5.0,
                "serving_size": 28.0,
                "serving_size_unit": "g",
                "test": null,
                "updated_at": "2019-10-16T17:59:18.026637"
            },
            "id": "302",
            "links": {"self": "http://127.0.0.1:5000/api/groceries/302"},
            "relationships": {
                "ingredient": {
                    "data": {"id": "29051", "type": "ingredients"},
                    "links": {
                        "related": "/api/groceries/302/ingredient",
                        "self": "/api/groceries/302/relationships/ingredient"
                    }
                },
                "label": {
                    "data": {"id": "418", "type": "nutrition_label"},
                    "links": {"related": "/api/groceries/302/label", "self": "/api/groceries/302/relationships/label"}
                }
            },
            "type": "groceries"
        }, "included": [], "jsonapi": {"version": "1.0"}, "links": {"self": "/api/groceries/302"}, "meta": {}
    }

];