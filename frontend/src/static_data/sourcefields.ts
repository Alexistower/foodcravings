export interface sources {
    groceries: Fields;
    restaurant: Fields;
    recipes: Fields;

    [index: string]: any;
}

export interface Fields {
    img: string;
    details: { [key: string]: string };
}

export const fieldTable: sources = {
    groceries: {
        img: 'image_url',
        details: {
            "name": 'name',
            "Brand": 'brand',
            "Price": 'price',
            "Serving Size": 'serving_size',
            "Serving Unit": 'serving_size_unit',
            "Category": 'category'
        }
    },
    restaurant: {
        img: 'image_url',
        details: {
            "name": 'name',
            "Address": 'address',
            "Phone Number": 'phone',
            "Price Range": 'priceRange',
            "Cuisines": 'cuisines'
        }
    },
    recipes: {
        img: 'image_url',
        details: {
            "name": 'name',
            "Calories": 'calories',
            "Instructions": 'instructions',
            "Serving Size": 'servings'
        }
    }
};
